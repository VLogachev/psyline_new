<?php
    $isPsyliner = $this->db->where('id',$this->session->userdata('user_id'))->get('users')->row()->psyliner_id != 0;
?>
<!-- выводим все консультации для определенного пользователя -->
<table style="width: 100%; text-align: center;">
    <tr>
        <th>
            <? if($isPsyliner):?>
                Клиент
            <? else: ?>
                Псилайнер
            <? endif; ?>
        </th>
        <th>Дата начала</th>
        <th>Дата окончания</th>
        <th>Тип консультации</th>
        <? if(!$isPsyliner): ?>
            <th>Статус</th>
        <? endif; ?>
        <th>
            <? if($isPsyliner):?>
                Начать сессию
            <? else: ?>
                Присоединиться к сессии
            <? endif; ?>
        </th>
    </tr>
    <? foreach($consultations as $consultation): ?>
        <tr>
            <td><?= $consultation->username?></td>
            <td><?= date("d.m.Y H:i:s",$consultation->from_time)?></td>
            <td><?= date("d.m.Y H:i:s",$consultation->to_time)?></td>
            <td><?= $consultation->type_name?></td>
            <? if(!$isPsyliner): ?>
                <td><span data-conference-name="conference-<?=$consultation->id?>">offline</span></td>
            <? endif; ?>
            <td>
                <? if($isPsyliner):?>
                    <button
                        data-to-time="<?=$consultation->to_time?>"
                        data-category-by-stream="<?=$consultation->type_id?>"
                        data-conference-name="conference-<?=$consultation->id?>"
                        class="open-session"
                        <?=$this->consultation_model->disableInitButton($consultation->from_time, $consultation->to_time)?>>
                        начать сессию
                    </button>
                <? else: ?>
                    <button data-to-time="<?=$consultation->to_time?>" data-category-by-stream="<?=$consultation->type_id?>" data-conference-name="conference-<?=$consultation->id?>" class="connect-to-session" disabled>присоединиться</button>
                <? endif; ?>
            </td>
        </tr>
    <? endforeach; ?>
</table>

<!-- модальное окно с видео -->
<div data-user-id="<?=$this->session->userdata('user_id')?>" id="webRTCDialog" style="background: #ffffff;">
    <!-- контейнер для кнопок управления сессией -->
    <div id="webrtcControls" class="webrtcdialog__controls">
        <div class="controls__button controls__disable-video"><img src="/images/video-icon.png"></div>
        <div class="controls__button controls__enable-video" style="display: none;"><img src="/images/video-icon-crossed.png"></div>
        <div class="controls__button controls__disable-audio"><img src="/images/sound-icon.png"></div>
        <div class="controls__button controls__enable-audio" style="display: none;"><img src="/images/sound-icon-crossed.png"></div>
        <div class="controls__button controls__fullscreen"><img src="/images/fullscreen-icon.png"></div>
        <div class="controls__button controls__end-call"><img src="/images/end-call-icon.png"></div>
    </div>
    <div id="webRTCVideoContainer" style="float: left; width: 700px; position: relative;">
        <!-- контейнер для видео -->
    </div>
    <div style="float: right; width: 200px;">
        <!-- таймер времени окончания консультации -->
        <div id="consultationTimeLeft">

        </div>
        <!-- чат -->
        <div>
            <h4>Чат</h4>
            <div>Сообщение 1</div>
            <div>Сообщение 2</div>
            <input type="text"/><br/>
            <button>отправить</button>
        </div>
        <!-- слушатели вебинара -->
        <div id="webinarUsers">

        </div>
    </div>
</div>

<script src="/js/webrtc/libs/RTCMultiConnection-v3.0/dist/rmc3.js"></script>
<script src="https://rtcmulticonnection.herokuapp.com/socket.io/socket.io.js"></script>
<script src="/js/webrtc/index.js"></script>
