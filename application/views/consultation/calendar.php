<div class="component">
    <div style="width: 750px;">
    <h1>Резервирование <?=$tip?>консультации</h1>
    
    <p style="text-indent: 0px;">Здесь Вы можете выбрать дату и время проведения
     online консультации, которые проводит <strong><?=$this->user_model->user_name_by_id($pid_uid)?></strong></p>
     <p style="text-indent: 0px; color: #4e6ca1;"><strong>Синим цветом отмечены дни, в которые консультация возможна.</strong></p>
     <br />
    <div class="calendar">
        <?php
        $days = array('Пнд','Втр','Срд','Чтв','Птн','<span style="color:#B78D8D;">Сбт</span>','<span style="color:#B78D8D;">Вск</span>');
        $mes_array = array('Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря');
        $mes_short_array = array('Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек');
        
        $now = time();
        $sutki = 86400;
        
        $denn = date('N',$now);//день недели
        $den = date('j',$now);
        $mes = date('n',$now);
        $god = date('Y',$now);
        $fst = mktime(0,0,1,$mes,$den,$god);//Первая доступная консультация
        
        
        
        $ii = $denn;
        echo '<div style="width:'.((7-(7-$denn+1))*104).'px; height:15px;display: inline-block;"></div>';
        for ($i = $fst; $i <= $fst+($sutki*31); $i=$i+$sutki) {
            
            
            //Количество часов дня
            $this->db->where(array(
            'psyliner_id'=>$pid,
            'time_from >='=> $i,
            'time_from <='=> $i+($sutki-2)
            ));
            
            $sql = $this->db->get('cons_time_for');
            $n = $sql->num_rows();
            if($n>0)
            {
                $add_class = 'cal_day_act';
            }else
            {
                $add_class = '';
            }
            
            echo '<div style="padding: 7px 3px;" class="cal_day_user '.$add_class.'" t="'.$i.'" pid="'.$pid.'" code="'.md5($i.'!'.$pid).'">';
            $denn = date('N',$i);//день недели
            $den = date('j',$i);
            $mes = date('n',$i);
            $god = date('Y',$i);
           
            echo $days[$denn-1].', '.$den.' '.$mes_short_array[$mes-1].'<div title="Количество консультаций в этот день" class="n_cons"></div>';
            echo '</div>';
            if($ii%7==0){echo '<br />';}
            $ii++;
        }
        ?>
</div>

<div id="hours"></div>

<div id="form_order_cons">
    <h1>Данные о консультации</h1>
    <br />
    <form action="consultation/final_check" method="post">
    <table>
    <tr>
        <td>Заказчик</td><td><div>Иванов Иван</div></td>
    </tr>
    <tr>
        <td>Консультант</td><td><div>Иванов Иван</div></td>
    </tr>
    <tr>
        <td>Тип консультации</td><td>Online Видео-консудьтация</td>
    </tr>
    <tr>
        <td>Стоимость</td><td><div>1500 руб</div></td>
    </tr>
    <tr>
        <td>Дата и время</td><td>17 Сентября 2015 (10:00 - 10:50 Мск)</td>
    </tr>
    </table>
    <input class="agree" type="submit" value="&#10004;&nbsp;&nbsp;&nbsp;&nbsp;Зарезервировать консультацию"/>
    </form>
    <br />
     
</div> 
   <br />
    </div>
</div>