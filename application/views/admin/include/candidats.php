<div class="grid_10">
            <?php
            $this->db->order_by( 'status asc, cr_time desc');
            $sql=$this->db->get_where('psyliners',array('status <='=>3,'status >'=>0));
            
            $n_k = $sql->num_rows();
            ?>
            <div class="box round">
                <h2>
                    Кандидаты в псилайнеры</h2>
                <div class="block">
                <?php 
                if($n_k==0){
                    echo '<p><strong>Нет активных кандидатов</strong></p>';
                }
                if($n_k>0){
                echo '<table class="form">';
                    $cnds = $sql->result();
                ?>    
                <tr>
                    <td>&nbsp;</td>
                    <td><strong>Дата подачи заявления</strong></td>
                    <td><strong>Имя</strong></td>
                    <td><strong>Локация</strong></td>
                    <td><strong>Статус</strong></td>
                    <td>...</td>
                    
                </tr>   
                <?php    
                    $i = 0;
                    foreach($cnds as $cnd):
                    $i=$i+1;
                ?>
                <tr style="background-color: #EFEFEF; border-bottom: 1px dotted silver; border-top: 2px solid white;">
                    <td></td>
                    <td>
                    <?php
                    if($cnd->status == 1)
                    {echo '<strong>'; echo $this->data->create_data($cnd->cr_time); echo'</strong>';}
                    else
                    {echo $this->data->create_data($cnd->cr_time);}
                    ?>
                    </td>
                    <td>
                    <?php
                    if($cnd->status == 1)
                    {echo '<strong>'; echo $cnd->name; echo'</strong>';}
                    else
                    {echo $cnd->name;}
                    ?>
                    </td>
                    <td>
                    <?php
                    if($cnd->status == 1)
                    {echo '<strong>'; echo $cnd->strana.' - '.$cnd->region.' - '.$cnd->np; echo'</strong>';}
                    else
                    {echo $cnd->strana.' - '.$cnd->region.' - '.$cnd->np;}
                    ?>
                    <td>
                    <?php if($cnd->status == 1){echo '<strong>Первичное обращение</strong>';}?>
                    <?php if($cnd->status == 2){echo 'Предварительно одобренно';}?>
                    <?php if($cnd->status == 3){echo 'Загрузга документов';}?>
                    </td>
                    <td><div class="more_info" n="<?=$i;?>">Подробно &nabla;</div></td>
                </tr>
                <tr class="more-info-tr-<?=$i?>" style="display: none;">
                    <td colspan="3">
                    <br />
                    <h5>Общая информация</h5>
                    Имя: <strong><?=$cnd->name?></strong><br />
                    E-Mail: <strong><?=$cnd->email?></strong><br />
                    Дата рождения: <strong><?=$this->data->format_data($cnd->dr)?></strong> 
                    (полных лет:<strong> <?=$this->data->f_y($cnd->dr)?></strong>)<br />
                    Страна: <strong><?=$cnd->strana?></strong><br />
                    Регион: <strong><?=$cnd->region?></strong><br />
                    Нас. Пункт: <strong><?=$cnd->np?></strong>
                    
                    <br /><br />
                    
                    <strong>Членство в организациях:</strong><br />
                    <?=nl2br($cnd->orgs)?><br /><br />
                    
                    Желаемый статус: <strong><?=$this->user_model->user_status($cnd->wish_rang)?></strong>
                    <br /><br />
                    
                    <h5>Образование</h5>
                    Высшее образование: <strong><?echo $this->user_model->user_obrazovanie($cnd->obrz)?></strong><br />
                    Вуз: <strong><?=$cnd->vuz?></strong><br />
                    Год окончания: <strong><?=date('Y',$cnd->god)?></strong><br /></strong><br /><br />
                    <strong>Дополнительное образование:</strong><br />
                    <?=nl2br($cnd->dop_obrz)?><br /><br />
                    </td>
                    <td colspan="3" style="width: 65%;">
                    <br />
                    <h5>Профессиональная информация</h5>
                    Начало психологической деятельности: <strong><?=$this->data->format_data_my($cnd->psy_from)?></strong> (полных лет:<strong> <?=$this->data->f_y($cnd->psy_from)?></strong>)<br />
                    Психотерапевтическая модальность:  <strong>
                    <?php
                    echo $this->user_model->user_psymod($cnd->psymod);
                    ?>
                    </strong><br /><br />
                    Формы работы: <br /><strong>
                    <?php
                    $forms = explode('*/*',$cnd->psy_form);
                    foreach($forms as $f):
                    echo '- '.$f.'<br />';
                    endforeach;
                    ?>
                    </strong><br />
                    Возрастные группы: <br /><strong>
                    <?php
                    $forms = explode('*/*',$cnd->psy_vzr);
                    foreach($forms as $f):
                    echo '- '.$f.'<br />';
                    endforeach;
                    ?>
                    </strong><br />
                    Типы проблемм: <br /><strong>
                    <?php
                    $forms = explode('*/*',$cnd->psy_tip);
                    foreach($forms as $f):
                    echo '- '.$f.'<br />';
                    endforeach;
                    ?>
                    </strong><br />
                    <?php if($cnd->status == 1)
                    {
                    ?>
                    <table style="width: 100%; text-align: center;">
                        <tr>
                            <td><a class="red_but" href="<?=base_url()?>admin/dest_cnd/<?=$cnd->id?>">Отказать</a></td>
                            <td><a class="yel_but" href="<?=base_url()?>admin/ref_cnd/<?=$cnd->id?>">Переслать</a></td>
                            <td><a class="green_but" href="<?=base_url()?>admin/conf_cnd/<?=$cnd->id?>">Одобрить</a></td>
                        </tr>
                    </table>
                    <?php    
                    }
                    ?>
                    
                    
                    
                    
                    
                    
                    </td>
                </tr>
                <?
                endforeach;
                echo '</table>';
                }
                ?>    
                    <div class="clear"></div>
                    
                </div>
            </div>
        </div>
        
        