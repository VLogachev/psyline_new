<div class="grid_10">
            <?php
            $sql=$this->db->get_where('psyliners',array('status'=>4));
            $n_k = $sql->num_rows();
            ?>
            <div class="box round">
                <h2>
                    Кандидаты в псилайнеры</h2>
                <div class="block">
                <?php 
                if($n_k==0){
                    echo '<p><strong>Нет активных кандидатов</strong></p>';
                }
                if($n_k>0){
                echo '<table class="form">';
                    $cnds = $sql->result();
                ?>    
                <tr>
                    <td></td>
                    <td><strong>Дата подачи заявления</strong></td>
                    <td><strong>Имя</strong></td>
                    <td><strong>Локация</strong></td>
                    <td><strong>Статус обработки</strong></td>
                    <td>...</td>
                    
                </tr>   
                <?php    
                    $i = 0;
                    foreach($cnds as $cnd):
                    $i=$i+1;
                ?>
                <tr style="background-color: #EFEFEF; border-bottom: 1px dotted silver; border-top: 2px solid white;">
                    <td></td>
                    <td><?=$this->data->create_data($cnd->cr_time)?></td>
                    <td><strong><span style="color: #1B548D;"><?=$cnd->name?></span></strong></td>
                    <td><?=$cnd->strana?> - <?=$cnd->region?> - <?=$cnd->np?></td>
                    <td>Не рассматривалась</td>
                    <td><div class="more_info" n="<?=$i;?>">Подробно &nabla;</div></td>
                </tr>
                <tr class="more-info-tr-<?=$i?>" style="display: none;">
                    <td colspan="3" style="vertical-align: top;">
                    
                    <img src="<?=base_url()?>users_img/<?=$cnd->foto;?>"/> <br /><br />
                    <h5>Общая информация</h5>
                    Имя: <strong><?=$cnd->name?></strong><br />
                    E-Mail: <strong><?=$cnd->email?></strong><br />
                    Дата рождения: <strong><?=$this->data->format_data($cnd->dr)?></strong> 
                    (полных лет:<strong> <?=$this->data->f_y($cnd->dr)?></strong>)<br />
                    Страна: <strong><?=$cnd->strana?></strong><br />
                    Регион: <strong><?=$cnd->region?></strong><br />
                    Нас. Пункт: <strong><?=$cnd->np?></strong>
                    
                    <br /><br />
                    
                    <h5>Членство в организациях:</h5>
                    <?=nl2br($cnd->orgs)?><br /><br />
                    <h5>Образование</h5>
                    Высшее образование: <strong><?echo $this->user_model->user_obrazovanie($cnd->obrz)?></strong><br />
                    Вуз: <strong><?=$cnd->vuz?></strong> Год окончания: <strong><?=date('Y',$cnd->god)?></strong><br /></strong><br /><br />
                    <strong>Дополнительное образование:</strong><br />
                    <?=nl2br($cnd->dop_obrz);?>
                    </td>
                    <td colspan="3" style="width: 65%;">
                    
                    <!--ФОТО ДОКУМЕНТОВ -->
                    <table style="width: 90%;">
                    <tr>
                        <td>
                        <a target="_blank" href="<?=base_url()?>users_img/diplom/<?=$cnd->foto_diplom?>" class="img_box">
                        <div class="d_doc" style="background-image: url('<?=base_url()?>users_img/diplom/<?=$cnd->foto_diplom?>');"></div>
                        </a>
                        </td>
                        
                        <td>
                        <a target="_blank" href="<?=base_url()?>users_img/pasport/<?=$cnd->foto_pasport?>" class="img_box">
                        <div class="d_doc" style="background-image: url('<?=base_url()?>users_img/pasport/<?=$cnd->foto_pasport?>');"></div>
                        </a>
                        </td>
                        
                        <td>
                        <a target="_blank" href="<?=base_url()?>users_img/dogovor/<?=$cnd->foto_dogovor?>" class="img_box">
                        <div class="d_doc" style="background-image: url('<?=base_url()?>users_img/dogovor/<?=$cnd->foto_dogovor?>');"></div>
                        </a>
                        </td>
                    </tr>
                    </table>
                    <!--КОНЕЦ ФОТО ДОКУМЕНТОВ -->
                    
                    <br />
                    <h5>Профессиональная информация</h5>
                    Начало психологической деятельности: <strong><?=$this->data->format_data_my($cnd->psy_from)?></strong> (полных лет:<strong> <?=$this->data->f_y($cnd->psy_from)?></strong>)<br />
                    Психотерапевтическая модальность:  <strong>
                    <?php
                    echo $this->user_model->user_psymod($cnd->psymod);
                    ?>
                    </strong><br /><br />
                    Формы работы: <br /><strong>
                    <?php
                    $forms = explode('*/*',$cnd->psy_form);
                    foreach($forms as $f):
                    echo '- '.$f.'<br />';
                    endforeach;
                    ?>
                    </strong><br /><br />
                    Возрастные группы: <br /><strong>
                    <?php
                    $forms = explode('*/*',$cnd->psy_vzr);
                    foreach($forms as $f):
                    echo '- '.$f.'<br />';
                    endforeach;
                    ?>
                    </strong><br /><br />
                    Типы проблемм: <br /><strong>
                    <?php
                    $forms = explode('*/*',$cnd->psy_tip);
                    foreach($forms as $f):
                    echo '- '.$f.'<br />';
                    endforeach;
                    ?>
                    </strong><br />
                    </td>
                    </tr>
                    <tr class="more-info-tr-<?=$i?>" style="display: none;">
                        <td colspan="6">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="background-color: #DFFFDF; padding: 5px; width: 50%; border: 1px solid silver;">
                                    
                                    <form action="<?=base_url()?>admin/conf_atest/" method="post">
                                    <input type="hidden" name="user" value="<?=$cnd->id;?>"/>
                                    <input type="hidden" name="wish" value="<?=$cnd->wish_rang;?>"/>
                                    Желаемый статус: <strong><?=$this->user_model->user_status($cnd->wish_rang)?></strong>
                                    <br /><br />
                                    Обоснование статуса:<br />
                                    <div style="padding: 10px; border: 1px solid silver; background-color: white;">
                                    <?=nl2br($cnd->wish_rang_info);?>
                                    </div><br />
                                    <strong>Предложить статус: </strong> 
                                    <select name="rang">
                                    <?php
                                    $query = $this->db->get_where('groups',array('status'=>1,'id >'=>3,'id <'=>8));
                                    $mods = $query->result();
                                    foreach($mods as $mod):
                                    ?>
                                    <option value="<?=$mod->id?>"><?=$mod->name?></option>
                                    <?php endforeach; ?>
                                    </select><br /><br />
                                    <strong>Пояснение</strong>:<br />
                                    <textarea style="width: 98.5%; height: 120px;" name="info"></textarea>
                                    <br /><br />
                                    <input type="submit" value="ОДОБРИТЬ" style="width: 70%; margin-left: 15%; padding: 10px 0; text-align: center; border: 3px solid silver; background-color: #008040; border-radius: 7px; color: white; cursor: pointer;"/>
                                    </form>

                                    </td>
                                    
                                    <td style="background-color: #FFDFDF; padding: 5px; width: 50%; border: 1px solid silver;">
                                    <form action="<?=base_url()?>admin/conf_atest/" method="post">
                                    Желаемый статус: <strong><?=$this->user_model->user_status($cnd->wish_rang)?></strong><br />
                                    Обоснование статуса:<br />
                                    <div style="padding: 10px; border: 1px solid silver;">
                                    <?=$cnd->wish_rang_info?>
                                    </div><br />
                                    Предложить статус: 
                                    <select name="status">
                                    <?php
                                    $query = $this->db->get_where('groups',array('status'=>1,'id >'=>3,'id <'=>8));
                                    $mods = $query->result();
                                    foreach($mods as $mod):
                                    ?>
                                    <option value="<?=$mod->id?>"><?=$mod->name?></option>
                                    <?php endforeach; ?>
                                    </select><br /><br />
                                    <input type="submit" value="ОТКАЗАТЬ" style="width: 70%; margin-left: 15%; padding: 10px 0; text-align: center; border: 3px solid silver; background-color: #D70000; border-radius: 7px; color: white; cursor: pointer;"/>
                                    </form>
                                    </td>
                                    
                                </tr>
                        </table>
                            </td>
                        
                    
                    
                </tr>
                <?
                endforeach;
                echo '</table>';
                }
                ?>    
                    <div class="clear"></div>
                    
                </div>
            </div>
        </div>
        
        