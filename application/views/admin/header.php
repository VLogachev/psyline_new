<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?=$title?></title>
    <link href="<?=base_url()?>favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>adm/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>adm/css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>adm/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>adm/css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>adm/css/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="<?=base_url()?>adm/css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?=base_url()?>adm/css/ie.css" media="screen" /><![endif]-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>adm/css/my.css" media="screen" />
    <!-- BEGIN: load jquery -->
    <script src="<?=base_url()?>adm/js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="<?=base_url()?>adm/text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="<?=base_url()?>adm/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>adm/js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>adm/js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>adm/js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <!-- BEGIN: load jqplot -->
    <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
    <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="js/jqPlot/excanvas.min.js"></script><![endif]-->
    <script language="javascript" type="text/javascript" src="<?=base_url()?>adm/js/jqPlot/jquery.jqplot.min.js"></script>
    <script language="javascript" type="text/javascript" src="<?=base_url()?>adm/js/jqPlot/plugins/jqplot.barRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="<?=base_url()?>adm/js/jqPlot/plugins/jqplot.pieRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="<?=base_url()?>adm/js/jqPlot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="<?=base_url()?>adm/js/jqPlot/plugins/jqplot.highlighter.min.js"></script>
    <script language="javascript" type="text/javascript" src="<?=base_url()?>adm/js/jqPlot/plugins/jqplot.pointLabels.min.js"></script>
    <!-- END: load jqplot -->
    <script src="<?=base_url()?>adm/js/setup.js" type="text/javascript"></script>
   <script type="text/javascript">

        $(document).ready(function () {
            /*setupDashboardChart('chart1');*/
            setupLeftMenu();
			setSidebarHeight();


        });
    </script>
    <script src="<?=base_url()?>adm/js/myscript.js" type="text/javascript"></script>
</head>