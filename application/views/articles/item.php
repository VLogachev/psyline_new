<div class="component">
  <div style="width: 750px;"> 
    
    <table style="width: 750px;">
        <tr>
            <td rowspan="2" style="padding-right: 10px;">
            <div style="
            width: 60px; 
            height: 60px; 
            background-size: 60px; 
            background-position: center; 
            background-image: url(<?=base_url();?>/users_img/<?=$this->user_model->psyliner_foto_by_user_id($item->user_id)?>);
            border-radius: 3px;
            "></div>
            </td>
            
            <td><h1><?=$item->title?></h1></td>
            </tr>
            <tr>
            <td>
            
            <div class="article_info">
            <span>Автор:</span> <a href="<?=base_url()?>psyliners/<?=$item->user_id?>"><?=$this->user_model->user_name_by_id($item->user_id)?></a>
            <span style="margin-left: 50px;">Опубликовано:</span> <?=$this->data->create_data($item->create)?>
            &nbsp;&nbsp;&nbsp;<span>Просмотров:</span> <?=$item->hits?>
            </div>
            
            </td>
        </tr>
    </table>
    
    <div class="article_text">
    <?=$item->text?>
    </div>
    <br /><br />
    <?php
    $user_id = $this->session->userdata('user_id');
    
    
    if(!empty($user_id))
    {   $sql = $this->db->get_where('articles_like',array('article_id'=>$item->id));
        $n = $sql->num_rows();
        ?>
        
        <div class="like" style="position: relative;">
            <?php
            if($n>0)
            {
            ?>    
            <div class="arrow_box">
            <div id="close_arrow" style="text-align: right;">Закрыть</div>  
            <span style="line-height: 20px;">Эта статья понравилась:</span><br />
            <?php
            $likers = $sql->result();
            foreach($likers as $liker):
            ?>
            <a href="#" title="<?=$this->user_model->user_name_by_id($liker->user_id)?>"> 
            <div style="
            width: 50px; 
            height: 50px; 
            background-size: 60px; 
            background-position: center; 
            background-image: url(<?=base_url();?>/users_img/<?=$this->user_model->psyliner_foto_by_user_id($liker->user_id)?>);
            border-radius: 3px;
            "></div>
            </a>
               
            <?php
            endforeach;
            echo "</div>";
            }
            ?>
            
            
            <div id="who_like"><?=$n?></div>
            <div id="like_info"></div>
            
            
            
            <div id="article_like" uid="<?=$this->session->userdata('user_id')?>" aid="<?=$item->id?>" autid="<?=$item->user_id?>">
            <img src="<?=base_url()?>icos/ico-like.png"/>Нравится
            </div>
        </div>
        <?php
    }
    ?>
    
    <table style="display: block; float: left; padding: 0px 10px; border: 1px solid silver;">
    <tr>
    <td>Поделитесь статьей: </td>
    <td><script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
    <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="facebook,vkontakte,twitter,odnoklassniki,gplus" data-yashareTheme="counter"></div>
    </td>
    </tr>
    </table>
    
    <br />
    <br /><br />
    
    <?php
    // ----------------- Другие статьи
    $this->db->where(array('status'=>1,'id !='=>$item->id,'user_id'=>$item->user_id));
    $this->db->order_by('create','asc');
    $sql = $this->db->get('articles');
    if($sql->num_rows()==0)
    {
        
    }
    else
    {
    $n = $sql->num_rows();
    ?>
    <div class="hide_block_wrap" style="width: 730px !important;">
                        <div class="hide_block_head" par="9" act="sd">Другие статьи автора (<?=$n?>)<div>Показать ▼</div></div>
                        <div class="hide_block hide_block9">
                        <br/>
                        <?php
                            echo "<table>";
                            $items = $sql->result();
                            foreach($items as $a):
                        {
                        ?>
                        <tr>
                        <td style="padding-right: 15px;">
                        <span style="font-size: 0.85em;"> <?=$this->data->create_data($a->create)?></span>
                        </td>
                        <td style="width: 535px; padding-right: 15px;">
                        <a style="color: #333; font-weight: bold;" href="<?=base_url()?>articles/item/<?=$a->id?>"><?=$a->title?></a><br />
                        </td>
                        <td>
                        <span style="font-size: 0.85em;">Просмотров: <span style="font-size: 0.85em;"><?=$a->hits?></span>
                        </td></tr>
                        <?php    
                        }   
                            endforeach;
                            echo "</table></div></div><br />";
    }
    // ------------------- Конец других статей
    ?>    
    
    
    
    <?php // ----------------- Коментарии
    $ses_user_id = $this->session->userdata('user_id');
    
    $this->db->where(array('status'=>1,'article_id'=>$item->id));
    $this->db->order_by('create','asc');
    $sql = $this->db->get('articles_coments');
    $nk = $sql->num_rows();
    ?>
    
    <div class="hide_block_wrap" style="width: 730px !important;">
    <div class="hide_block_head" par="2" act="sd">Комментарии (<?=$nk?>)<div>Показать ▼</div></div>
    <div class="hide_block hide_block2">
    <br/>
    
    <?php
    
    
    if($nk==0)
    {
       echo "Ваш комментарий может быть первым";
    }
    else
    {
    $n = $sql->num_rows();
    ?>
    
                        <?php
                            
                            $items = $sql->result();
                            $n = 1;
                            foreach($items as $c):
                            if($n%2==0)
                            { $bg = '#FFF;'; } else {$bg = '#FFE;';}          
                            
                        {
                        ?>
                        <div class="comment" style="background-color: <?=$bg?>">
                        
                        <div class="comment_img" style="
                        float: left;
                        width: 60px;  
                        height: 60px; 
                        background-size: 60px; 
                        background-position: center; 
                        background-image: url(<?=base_url();?>/users_img/<?=$this->user_model->psyliner_foto_by_user_id($c->user_id)?>);
                        border-radius: 3px;
                        "></div>
                        
                        <div class="comment_text">
                        <div class="comment_name">
                        <a href="<?=base_url()?>psyliners/profile/<?=$c->user_id?>"><?=$this->user_model->user_name_by_id($c->user_id)?></a>
                        </div>
                        <span style="font-size: 0.85em;">#<?=$n?> <img src="<?=base_url()?>icos/data.png" height="10px" /> <?=$this->data->create_data($c->create)?></span><br /><br />
                            <div class="comment_msg" uname="<?=$this->user_model->user_name_by_id($c->user_id)?>">
                                <?=$c->text?>
                                <div class="quot"><div id="quot_img"></div>Цитировать выделенное</div>
                            </div>
                        </div>
                        
                        </div>
                        <?php    
                        }   
                            $n++;
                            endforeach;                        
    }
    ?> 
    <?php
    if(!empty($ses_user_id))
    { 
    ?>
    
    <form action="<?=base_url()?>articles/add_comment" method="post">
    <br />
    <input type="hidden" name="a_id" value="<?=$item->id?>"/>
    <input type="hidden" name="u_id" value="<?=$ses_user_id?>"/>
    <input type="hidden" name="md" value="<?=md5($item->id.'boops'.$ses_user_id)?>"/>
    <strong>Ваш комментарий:</strong><br /><br />
    <script type="text/javascript">
	bkLib.onDomLoaded(function() {
        new nicEditor({
        	buttonList : 
        	['bold','italic','underline','strikeThrough','html','left','center','right',
            'image','upload','link','unlink']
        	
        	}
        	).panelInstance('textedit'); 
        }
        );
    </script>
    <textarea name="msg" id="textedit" style="width: 725px; height: 100px;"></textarea>
    <br />
    <input type="submit" value="Добавить комментарий" />
    </form>
    
    <?php
    }
    else
    {
        echo "<strong><p align='center' style='font-size:0.85em;'>
        Вы не можете добавить коментарий. Войдите или <a href='".base_url()."registration'>зарегистрируйтесь</a>
        </p></strong>";
    }
    ?>   
    </div></div>
    
<!--    
    <div id="hypercomments_widget"></div>
<script type="text/javascript">
_hcwp = window._hcwp || [];
_hcwp.push({widget:"Stream", widget_id: 63026});
(function() {
if("HC_LOAD_INIT" in window)return;
HC_LOAD_INIT = true;
var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/63026/"+lang+"/widget.js";
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(hcc, s.nextSibling);
})();
</script>
<a href="http://hypercomments.com" class="hc-link" title="comments widget">comments powered by HyperComments</a>
-->    

  </div>   
</div>

