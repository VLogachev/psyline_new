<div class="component">
    
    
    <div id="articles_category">
    <h3>Категории публикаций</h3>
        <div>
        <a href="<?=base_url()?>articles/">Все категории</a>
        <?php foreach($categorys as $cat):?>
        <a href="<?=base_url()?>articles/category/<?=$cat->id?>"><?=$cat->name?></a> 
        <?php endforeach; ?>
    </div>
    
    </div>
    <br /><br />
    <h1><?=$category_name?></h1>
    <br />
    
    <div id="articles_list">
    <table>
    <?php foreach($articles as $item):
    $query = $this->db->get_where('users',array('id'=>$item->user_id));
    $user_info = $query->row();
    
    $query = $this->db->get_where('articles_category',array('id'=>$item->category_id));
    $category = $query->row();
    
    $query = $this->db->get_where('articles_like',array('article_id'=>$item->id,'status'=>1));
    $likes = $query->num_rows();
    
    $query = $this->db->get_where('articles_coments',array('article_id'=>$item->id,'status'=>1));
    $comments = $query->num_rows();
    ?>
    <tr>
        <td style="width: 100px;">
        <div style="
        width: 60px; 
        height: 60px; 
        background-size: 60px; 
        background-position: center; 
        background-image: url(<?=base_url();?>/users_img/<?=$this->user_model->psyliner_foto_by_user_id($item->user_id)?>);
        border-radius: 3px;
        "></div>
        </td>
        
        <td style="width: 500px;">
        <a href="<?=base_url()?>articles/item/<?=$item->id?>"><h2><?=$item->title?></h2></a>
        Автор: <a href="#"><?=$user_info->username?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <br /><br />
        <span style="font-size: 0.8em; font-weight: bold;"> <?php $this->data->create_data($item->create);?></span>
        </td>
        
        <td style="width: 150px; vertical-align: top; text-align: right;">
        <br />
        <?php
        if($item->hits > 0)
        {
        ?>
        <img title="Количество просмотров" style="height: 10px; margin-top: -3px;" src="<?=base_url()?>icos/ico-view.png"/>&nbsp;<?=$item->hits?>&nbsp;&nbsp;    
        <?php
        }
        ?>
        <?php
        if($likes > 0)
        {
        ?>
        <img title="Отметок нравиться" style="height: 11px; margin-top: -3px;" src="<?=base_url()?>icos/ico-like.png"/>&nbsp;<?=$likes?>&nbsp;&nbsp;
        <?php
        }
        ?>
         <?php
        if($comments > 0)
        {
        ?>
        <img title="Количество коментариев" style="height: 11px; margin-top: -3px;" src="<?=base_url()?>icos/ico-comment.png"/>&nbsp;<?=$comments?>&nbsp;&nbsp;
        <?php
        }
        ?>
        </td>
    </tr>
    
    <?php endforeach; ?>
    </table>  
    <?php echo $this->pagination->create_links(); ?>
    </div>
</div>