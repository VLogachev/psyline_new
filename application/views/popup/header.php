<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>PSY-LINE.ORG</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<?=base_url()?>css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>css/style_popup.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>favicon.ico" rel="shortcut icon" type="image/x-icon" />

<script type="text/javascript" src="<?=base_url()?>javascript/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>javascript/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>javascript/cycle.js"></script>
<script type="text/javascript" src="<?=base_url()?>javascript/mask.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>javascript/myscript.js"></script>

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

</head>
<body >
<div id="popup">
