<?php
function CleanText($content) //ooieoc? iadrainec nndricou 
{ 
$text = $content; 
$text = preg_replace('/-\s{2,}/s', '', $text); 
$text = preg_replace("/<title>\s*(.*?)\s*<\/title>/is"," ",$text); 
$text = preg_replace("/<!--.*?-->/s"," ",$text); 
$text = preg_replace("/<[Ss][Cc][Rr][Ii][Pp][Tt].*?<\/[Ss][Cc][Rr][Ii][Pp][Tt]>/s"," ",$text); 
$text = preg_replace("/<[Ss][Tt][Yy][Ll][Ee].*?<\/[Ss][Tt][Yy][Ll][Ee]>/s"," ",$text); 
$text = preg_replace("/<[^>]*>/s"," ",$text); 
  
$style='/\<style[\w\W]*?\<\/style\>/i'; 
$script = '/\<script[\w\W]*?\<\/script\>/i'; 
$doc = '/\<!doctype[\w\W]*?\>/i'; 
     
$text = preg_replace($doc, ' ', $text); 
$text = preg_replace($style, ' ', $text); 
$text = preg_replace('~style="[^"]*"~i', '', $text);
$text = strip_tags($text,'<strong><b>'); 
$text = preg_replace($script, ' ', $text); 
$text = str_replace("&nbsp;", ' ', $text); 
$text = preg_replace ("/[\s,]+/", ' ', $text); 

$text = str_replace("...", ".", $text); 
$text = str_replace("..", ".", $text); 
$text = str_replace("!!!", "!", $text); 
$text = str_replace("!!", "!", $text); 
$text = str_replace("???", "?", $text); 
$text = str_replace("??", "?", $text); 
$text = str_replace('»', '"', $text); 
$text = str_replace('«', '"', $text); 

$text = str_replace(".", ".\r\n", $text); 
$text = str_replace("!", ".\r\n", $text); 
$text = str_replace("?", ".\r\n", $text); 
$text = str_replace("|", ".\r\n", $text); 

$text = str_replace(".\r\n.\r\n", ".\r\n", $text); 
$text = str_replace(".\r\n.\r\n", ".\r\n", $text); 
$text = str_replace(". \r\n", ".\r\n", $text); 
$text = str_replace("\r\n\r\n", "\r\n", $text); 
$text = str_replace("\r\n\r\n", "\r\n", $text); 

$text = str_replace("\t\t", " ", $text); 
$text = str_replace("\t", " ", $text); 
$text = str_replace("   ", " ", $text); 
$text = str_replace("  ", " ", $text); 
$text = str_replace(" .", ".", $text); 
$text = str_replace(" ,", ",", $text); 
$text = str_replace("- - - ", "- ", $text); 
$text = str_replace("- - ", "- ", $text); 

$text = str_replace("---", "-", $text); 
$text = str_replace("--", "-", $text); 
$text = str_replace("--", "-", $text); 
$text = str_replace("_", " ", $text); 

$text = str_replace("   ", " ", $text); 
$text = str_replace("  ", " ", $text); 
$text = str_replace("--", "-", $text); 
$text = str_replace("--", "-", $text); 
$text = str_replace("***", "*", $text); 
$text = str_replace("**", "*", $text); 

$text = str_replace("\r\n?", "\r\n", $text); 
$text = str_replace("\r\n(", "\r\n", $text); 
$text = str_replace("\r\n)", "\r\n", $text); 
$text = str_replace("\r\n'", "\r\n", $text); 
$text = str_replace("\r\n-", "\r\n", $text); 
$text = str_replace("\r\n*", "\r\n", $text); 
$text = str_replace("\r\n?", "\r\n", $text); 
$text = str_replace("\r\n-", "\r\n", $text); 
$text = str_replace("\r\n ", "\r\n", $text); 
$text = str_replace("<", "", $text); 
$text = str_replace(">", "", $text); 

return $text; 
}   

?>


<div class="component">
    <div style="width: 750px;">
    <h1>Специалисты Пси-профиля</h1>
    
    <div id="forum-filter"></div>
    <div id="forum-items-header"></div>
    <br />
    <table id="psy_list_tbl" cellspacing="0" width="750px">
    
    <?php
    $this->db->order_by("id", "asc");
    $query = $this->db->get_where('psyliners',array('status'=>6));
    foreach ($query->result() as $item):
    
    $query = $this->db->get_where('users',array('email'=>$item->email));
    $user = $query->row();
    
    $params = $this->user_model->psyliner_params_by_psyliner_id($item->id);
    
    $cons_block = $this->cons->cons_block($item->id,$item->rang);
    
    ?>
    
    <tr>
        <td style="width: 170px; vertical-align: top;">
        
        <div style="border: 1px solid silver; padding: 3px; width: 150px;">
        
        <a href="<?= base_url() ?>psyliners/profile/<?=$item->id?>">
        <div class="user_img" style="background: url('<?=base_url().'users_img/'.$item->foto;?>');"></div>
        </a>
        
        </div>
        
        <div class="online">
        <div class="onl<?=$user->online?>"><?php if($user->online == 0){echo 'OFFLINE';}else{echo 'ONLINE';}?></div>
        </div>
        
        
        </td>
        
        <td style="vertical-align: top;">
        
        <p class="info_h">
            <a href="<?= base_url() ?>psyliners/profile/<?=$item->id?>"><?=$user->username?></a>
        </p>
        
        <p><img style="width: 100px;" src="<?=base_url().'users_img/r'.$item->rang.'.png';?>" />&nbsp;&nbsp;
        
        <div style="margin-top: -20px; position: absolute; margin-left: 130px;">
        <a href="<?=base_url()?>psyline/rangs#<?=$item->rang?>" class="info iframe"><?=$this->user_model->user_status($item->rang)?></a> 
        </div>
        </p>
        
        <p><span><?=$item->np?> (<?=$item->strana?>)</span></p>
        
        <p><span>Пси-Профиль:</span>  <?=$this->user_model->user_obrazovanie($item->obrz)?></p>
        <p><span>Модальность:</span> <a href="<?=base_url()?>psyliners/psy_mod_info/<?=$item->psymod?>" class="info iframe"><?=$this->user_model->user_psymod($item->psymod)?></a> </p>
        <br />
        
        
        
        
        <!--
        <?php //Личное обращение
        if($params->pers_msg != ''){
        ?>
        <div style="margin-left: 15px; margin-top: 10px; font-style: italic; font-family: helvetica, arial, sans-serif; font-size: 15px;">
        <?=CleanText($params->pers_msg)?>
        </div>
        <?php } ?>
        -->
        
        <?php
        if($params->cons_now == 1){
        ?>
        <br />
        <div class="prcs" style="background-color: #BDFCB1;">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="<?=base_url()?>icos/check.png"/></td>
                <td class="nо_img"> Специалист готов проконсультировать Вас <strong>сейчас</strong></td>
                <td class="img2_td" style="width: 89px;">
                <a class="btn_with_img" href="#">
                <img style="height: 25px;" title="Получить консультацию сейча" src="<?=base_url()?>icos/comm.png"/>
                Получить</a>
                </td>
            </tr>
            </table>
        </div>
        
        
        <br />
                    
        <?php
        }
        if($params->free_cons == 1){
        ?>
        <div class="prcs" style="background-color: #BDFCB1;">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="<?=base_url()?>icos/check.png"/></td>
                <td class="nо_img"> Специалист готов провести <strong>бесплатную консультацию</strong> (15 минут)</td>
                <td class="img2_td" style="width: 89px;">
                <a class="btn_with_img" href="#">
                <img style="height: 25px;" title="Получить консультацию сейча" src="<?=base_url()?>icos/comm.png"/>
                Получить</a>
                </td>
            </tr>
            </table>
        </div>
        
        <br />
        <?php
        }
        ?>
        <br />
        
        <p><span><strong>Доступные виды консультаций:</strong></span></p>
        
        <?=$cons_block['v']?>
        <?=$cons_block['a']?>
        <?=$cons_block['t']?>
        <?=$cons_block['per']?>
        <br />
        
        </td>
    </tr>
    <?php endforeach ?>
    </table>
    
    
    </div>
</div>