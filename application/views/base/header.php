<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title><?=$title;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<?=base_url()?>css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>css/style.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>favicon.ico" rel="shortcut icon" type="image/x-icon" />

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/cycle.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.cycle2.tile.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/mask.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/colorbox.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/myscript.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/nicEdit.js"></script>

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

</head>
<body>

<div class="results"></div>
