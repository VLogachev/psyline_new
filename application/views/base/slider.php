<table style="width: 900px; margin-left: 25px;" id="slider">
<tr>
    <td style="width: 750px; vertical-align: top; padding-top: 20px;">
    <div class="cycle-slideshow composite-example" 
    data-cycle-fx=tileBlind
    data-cycle-speed=500
    data-cycle-timeout=6000 
    data-cycle-slides="> div">
        <div>
            <img src="<?=base_url();?>/images/slide1.jpg" />
            <div class="cycle-overlay"><h1>Консультация психолога онлайн</h1>
            <span>Вы можете получить квалифицированную психологическую помощь не выходя из дома</span></div>
        </div>
        <div>
            <img src="<?=base_url();?>/images/slide2.jpg" />
            <div class="cycle-overlay"><h1>Очная встреча с психологом</h1>
            <span>Вы можете выбрать квалифицированного психолога для личной встречи</span></div>
        </div>
        <div>
            <img src="<?=base_url();?>/images/slide3.jpg" />
            <div class="cycle-overlay"><h1>PSYLINE стирает границы пространства</h1>
            <span>Вы можете получить квалифицированную психологическую помощь не выходя из дома</span></div>
        </div>
        <div>
            <img src="<?=base_url();?>/images/slide4.jpg" />
            <div class="cycle-overlay"><h1>Обучение и супервизии</h1>
            <span>PSYLINE расширяет возможности профессионального роста</span></div>
        </div>
    </div>
    </td>
    
    <td style="vertical-align: top; padding-left: 10px; padding-top: 21px;">
    <div class="module">
    <!--<h3>Что такое ПСИЛАЙН?</h3>-->
    
    <p style="font-size:0.9em;"><img style="float: left; margin: 5px;" src="<?=base_url()?>images/ico-psyliner.png" width="35px"/> 
    PSYLINE (ПСИЛАЙН) это международна организация, 
    объединяющая <strong>375</strong> квалифицированных специалистов-психологов.</p><br />
    <a class="btn" href="<?=base_url();?>psyliners">Специалисты-псилайнеры</a>
    <br />
    
    <p style="font-size:0.9em;"><img style="float: left; margin: 5px;" src="<?=base_url()?>images/ico-user.png" width="35px"/> 
    Нашими услугами воспользовались уже <strong>1085</strong> человек. 
    PSYLINE это качественная психологическая помощь, которую можно получить в режиме on-line. </p><br />
    <a class="btn" href="<?=base_url();?>psilyners">Зарегистрироваться</a><br />
    
    <p style="font-size:0.9em;"><img style="float: left; margin: 5px;" src="<?=base_url()?>images/ico-cons.png" width="35px"/> 
    Псилайн-Форум это возможность задать вопрос специалисту и получить квалифицированный ответ на него.  
    <a href="<?=base_url();?>forum">Перейти на форум</a>. </p>
    </div>
    </td></tr></table> 
    <br />
