<?php
        $query = $this->db->get_where('articles',array('status'=>1,'category_id'=>3),2);
        $news = $query->result();
        
        $this->db->order_by('id','desc');
        $query = $this->db->get_where('articles',array('status'=>1,'category_id <>'=>3),4);
        $articles = $query->result();
?>

    <div class="component">
    
        <div class="pg1 w33 cright">
        <h3>Последние на форуме</h3>
        </div> 
        
        <div class="pg1 w33 cmiddle">
        <a class="link_to_all" href="#">Все статьи</a>
        <h3>Последнии статьи</h3>
        <?php foreach($articles as $item):?>
                <div class="item">
                    <div class="item_img" style="width:40px; height: 40px; background-image: url(<?=base_url();?>/users_img/<?=$this->user_model->psyliner_foto_by_user_id($item->user_id)?>);"></div>
                    <div class="it_title"><a href="#"><?=$item->title?></a></div>
                    <div class="item_data">
                    <a href="#"><?php echo $this->user_model->user_name_by_id($item->user_id);?></a> <?php echo $this->data->create_data($item->create);?></div>
                    
                    </div>
                <?php endforeach; ?>
        </div> 
        
        <div class="pg1 w33 cleft">
            <a class="link_to_all" href="#">Все новости</a>
            <h3>Новости PsyLine</h3>
                <?php foreach($news as $item):?>
                <div class="item">
                    <div class="item_img" style="background-image: url(<?=base_url();?>/images/news/<?=$item->img;?>);"></div>
                    <div class="item_data"><?php echo $this->data->create_data($item->create);?></div>
                    <div class="it_title"><a href="#"><?=$item->title?></a></div>
                    <div class="item_text">
                    <?php echo mb_substr(strip_tags($item->text), 0, 90, 'UTF-8'); ?> ...
                    </div>    
                    
                    </div>
                <?php endforeach; ?>
        
        </div> 
        
        <div class="pg1 w100">
            <h3>Псилайнеры</h3>
        </div> 
    
    </div>
