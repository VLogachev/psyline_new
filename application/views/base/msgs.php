<div id="top_msgs">
<h2>Новые сообщения</h2>
<?php
$this->db->where(array('to'=>$this->session->userdata('user_id'),'new'=>1));
$this->db->order_by('cr_time desc');
$sql = $this->db->get('msgs',5,0);

$msgs = $sql->result();
foreach($msgs as $msg):

//ФОТО И ИМЯ АДМИНИСТРАЦИИ
if($msg->from == 0)
{
    $img = base_url().'images/psyline.jpg';
    $name = 'Администрация PSY-LINE.ORG';
}
else
{
    //ПСИЛАЙНЕР?
    $is = $this->user_model->is_psyliner($msg->from);
    //ДА!
    if($is == true)
    {
        $user_from = $this->user_model->psyliner_info($msg->from);
        
        $img = base_url().'users_img/'.$user_from->foto;
        $name = $user_from->name.' (Псилайнер)';
    }
    
}
?>
<div class="item_msg">
    <table style="width: 100%;">
        <tr>
            <td style="width: 70px; text-align: left;">
                <div class="img" style="background-image: url('<?=$img?>');"></div>
            </td>
            <td>
                <div class="msg_content">
                <strong><?=$name;?></strong><br />
                <?=$msg->text;?><br />
                <span style="font-size: 0.85em; color: gray;"><?php $this->data->create_data($msg->cr_time);?></span>
                </div>
            </td>
        </tr>
    </table>
</div>
<?php
endforeach;
?>
<br />
<a href="<?=base_url()?>user/profile#tabs-2" style="color: #3b5998; text-align: center; font-weight: bold;">Перейти к сообщениям</a>
<a class="u_p_close" href="#" style="color: #3b5998; text-align: center; font-weight: bold; float: right;">Закрыть</a>
<br />
</div>