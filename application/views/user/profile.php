<div class="component" >
    
    <h1>Личный кабинет</h1>
    <br /><br />
    <?php
    $info = $this->user_model->psyliner_info($this->session->userdata['user_id']);
    ?> 
    <?php
    if($info->status == 2)//Предворительно одобрен
    {
    ?>
    <div class="r_msg">
    <strong>Внимание!</strong><br /><br />
    <strong>Ваш аккаунт еще не активирован!</strong><br />
    Для активации вашего аккаунта необходимо загрузить:<br />
    - Фотогрфию профиля
    <br />
    А так же отсканированные копии следующих документов:<br />
    - паспорт;<br />
    - диплом о высшем образовании пси-профиля;<br />
    - подписанный договор с порталом PSYLINE
    </div><br /><br /> 
    <?php
    }
    if($info->status == 3)//Подготавлил все документы
    {
    ?>
    <div class="r_msg">
    <strong>Вы загрузили все необходимые документы!</strong><br /><br />
    <strong>Внимание!</strong><br />
    <strong>Все готово для начала процедуры аттестации!</strong><br />
    Убедитесь что все докумнты загружены корректно. После аттестации их нельзя будет заменить.<br />
    </div><br /><br /> 
    <div class="r_msg">
    <strong>Что дальше?</strong><br /><br />
    1. Еще раз ознакомтесь с положением о статусе псилайнера с тем что бы объективно оценить свой возможный статус.<br />
    2. Укажите и обоснуйте статус на который вы претендуете.<br />
    3. Нажмите на кнопку "Отправить на аттестацию".<br /> 
    </div><br /><br /> 
    <div class="grey_msg">
    <form method="post" action="<?=base_url()?>user/send_profile">
    Желаемый статус: 
    <select name="status">
        <?php
        $query = $this->db->get_where('groups',array('status'=>1,'id >'=>3,'id <'=>8));
        $mods = $query->result();
        foreach($mods as $mod):
        ?>
        <option value="<?=$mod->id?>"><?=$mod->name?></option>
        <?php endforeach; ?>
        </select>
    <br /><br />
    Обоснование статуса:<br />
    <script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
    </script>
    <textarea style="width: 100%; height: 120px; background-color: white !important;" name="info"></textarea>
    <br />
    <p style="text-align: right;"><input type="submit" class="orang_b" value="Отправить на аттестацию" /></p>
    </form>
    </div>
    <br /><br />
    <?php
    }
    ?>
    <?php
    if($info->status == 4)
    {
    ?>
    <div class="r_msg">
    <strong>Внимание!</strong><br /><br />
    <strong>Ваша анкета находится в процессе аттестации!</strong><br /><br />
    О решении аттестационной комисси Вам будет сообщено по электронной почте.
    </div><br /><br /> 
    <?php
    }
    ?>
    <div id="tabs">
      <ul>
        <li><a href="#tabs-1">Общая информация</a></li>
        <li><a href="#tabs-2">Сообщения</a></li>
        <li><a href="#tabs-3">Консультации</a></li>
        <li><a href="#tabs-4">Финансы</a></li>
        <li><a href="#tabs-5">Публикации</a></li>
        <li><a href="#tabs-6">Настройки</a></li>
      </ul>
      <div id="tabs-1">
      <table style="width: 1100px;">      
                <tr class="more-info-tr">
                    <td style="width: 40%; vertical-align: top;">
                    <h2>Общая информация</h2><br />
                    <?php
                    if($info->foto == ''){
                    ?> 
                    <img src="<?=base_url()?>images/nofoto.jpg"/> <br />
                    <?php
                    $atts = array(
                        'width'       => 800,
                        'height'      => 400,
                        'scrollbars'  => 'yes',
                        'status'      => 'no',
                        'resizable'   => 'no',
                        'screenx'     => 0,
                        'screeny'     => 0,
                        'window_name' => '_self'
                    );
                    
                    echo anchor_popup('user/upload/foto', 'Загрузить фото', $atts);
                    }
                    else
                    {
                    ?>
                    <img src="<?=base_url()?>users_img/<?=$info->foto;?>"/> <br />
                    <?php
                        if($info->status == 2 OR $info->status == 3)
                        {
                        $atts = array(
                            'width'       => 800,
                            'height'      => 400,
                            'scrollbars'  => 'yes',
                            'status'      => 'no',
                            'resizable'   => 'no',
                            'screenx'     => 0,
                            'screeny'     => 0,
                            'window_name' => '_self'
                        );
                        
                        echo anchor_popup('user/upload/foto', 'Измнить фото профиля', $atts);
                        }
                    }
                    ?>
                    <br /><br />
                    Имя: <strong><?=$info->name?></strong><br />
                    E-Mail: <strong><?=$info->email?></strong><br />
                    Дата рождения: <strong><?=$this->data->format_data($info->dr)?></strong> 
                    (полных лет:<strong> <?=$this->data->f_y($info->dr)?></strong>)<br />
                    Страна: <strong><?=$info->strana?></strong><br />
                    Регион: <strong><?=$info->region?></strong><br />
                    Нас. Пункт: <strong><?=$info->np?></strong>
                    <br /><br />
                    <?php
                    if($info->status < 6)
                    {?>
                    Желаемый статус: <strong><?=$this->user_model->user_status($info->wish_rang)?></strong>
                    <br /><br />
                    <?php }else {?>
                    Статус: <strong><?=$this->user_model->user_status($info->rang)?></strong><br /><br />
                    <?php } ?>
                    
                    
                    <strong>Членство в организациях:</strong><br />
                    <?=nl2br($info->orgs)?><br /><br />
                    </td>
                    
                    <td style="width: 60%; vertical-align: top; padding-top: 40px;">
                    <div class="hide_block_wrap">
                    <div class="hide_block_head" par="<?=$info->id?>" act="sd">Образование<div>Показать &#9660;</div></div>
                    <div class="hide_block hide_block<?=$info->id?>">
                        <br />
                        Высшее образование: <strong><?echo $this->user_model->user_obrazovanie($info->obrz)?></strong><br />
                        Вуз: <strong><?=$info->vuz?></strong> Год окончания: <strong><?=date('Y',$info->god)?></strong><br /><br />
                        <strong>Дополнительное образование:</strong><br />
                        <?=nl2br($info->dop_obrz)?>
                    </div>
                    </div>
                    <br /><br />
                    
                    <?php $nnn = rand(10,19);?>
                    <div class="hide_block_wrap">
                    <div class="hide_block_head" par="<?=$nnn?>" act="sd">Контактная информация<div>Показать &#9660;</div></div>
                    <div class="hide_block hide_block<?=$nnn?>">
                        <br />
                        Высшее образование: <strong><?echo $this->user_model->user_obrazovanie($info->obrz)?></strong><br />
                        Вуз: <strong><?=$info->vuz?></strong> Год окончания: <strong><?=date('Y',$info->god)?></strong><br /><br />
                        <strong>Дополнительное образование:</strong><br />
                        <?=nl2br($info->dop_obrz)?>
                    </div>
                    </div>
                    <br /><br />
                    
                    <table style="width: 90%;">
                    <tr>
                        <td>
                        <?php
                        if($info->foto_diplom == ''){
                        ?> 
                        <img src="<?=base_url()?>images/nodoc.jpg"/> <br />
                        <?php
                        $atts = array(
                            'width'       => 800,
                            'height'      => 400,
                            'scrollbars'  => 'yes',
                            'status'      => 'no',
                            'resizable'   => 'no',
                            'screenx'     => 0,
                            'screeny'     => 0,
                            'window_name' => '_self'
                        );
                        
                        echo anchor_popup('user/upload/diplom', 'Загрузить скан диплома', $atts);
                        }
                        else
                        {
                        ?>
                        <a href="<?=base_url()?>users_img/diplom/<?=$info->foto_diplom?>" class="box">
                        <div class="d_doc" style="background-image: url('<?=base_url()?>users_img/diplom/<?=$info->foto_diplom?>');"></div>
                        </a>
                        <br />
                        <?php
                         if($info->status == 2 OR $info->status == 3)
                         {
                            $atts = array(
                                'width'       => 800,
                                'height'      => 400,
                                'scrollbars'  => 'yes',
                                'status'      => 'no',
                                'resizable'   => 'no',
                                'screenx'     => 0,
                                'screeny'     => 0,
                                'window_name' => '_self'
                            );
                            
                            echo anchor_popup('user/upload/diplom', 'Изменить скан диплома', $atts);
                            }
                        }
                        ?>
                        </td>
                        <td>
                        <?php
                        if($info->foto_pasport == ''){
                         ?> 
                        <img src="<?=base_url()?>images/nodoc.jpg"/> <br />
                        <?php
                        $atts = array(
                            'width'       => 800,
                            'height'      => 400,
                            'scrollbars'  => 'yes',
                            'status'      => 'no',
                            'resizable'   => 'no',
                            'screenx'     => 0,
                            'screeny'     => 0,
                            'window_name' => '_self'
                        );
                        
                        echo anchor_popup('user/upload/pasport', 'Загрузить скан паспорта', $atts);
                        }
                        else
                        {
                        ?>
                        <a href="<?=base_url()?>users_img/pasport/<?=$info->foto_pasport?>" class="box">
                        <div class="d_doc" style="background-image: url('<?=base_url()?>users_img/pasport/<?=$info->foto_pasport?>');"></div>
                        </a>
                        <br />
                        <?php
                        if($info->status == 2 OR $info->status == 3)
                        {
                            $atts = array(
                                'width'       => 800,
                                'height'      => 400,
                                'scrollbars'  => 'yes',
                                'status'      => 'no',
                                'resizable'   => 'no',
                                'screenx'     => 0,
                                'screeny'     => 0,
                                'window_name' => '_self'
                            );
                            
                            echo anchor_popup('user/upload/pasport', 'Изменить скан паспорта', $atts);
                        }
                        }
                        ?>
                        </td>
                        <td>
                        <?php
                        if($info->foto_dogovor == ''){
                         ?> 
                        <img src="<?=base_url()?>images/nodoc.jpg"/> <br />
                        <?php
                        $atts = array(
                            'width'       => 800,
                            'height'      => 400,
                            'scrollbars'  => 'yes',
                            'status'      => 'no',
                            'resizable'   => 'no',
                            'screenx'     => 0,
                            'screeny'     => 0,
                            'window_name' => '_self'
                        );
                        
                        echo anchor_popup('user/upload/dogovor', 'Загрузить скан договора', $atts);
                        }
                        else
                        {
                        ?>
                        <a href="<?=base_url()?>users_img/dogovor/<?=$info->foto_dogovor?>" class="box">
                        <div class="d_doc" style="background-image: url('<?=base_url()?>users_img/dogovor/<?=$info->foto_dogovor?>');"></div>
                        </a>
                        <br />
                        <?php
                        if($info->status == 2 OR $info->status == 3)
                        {
                            $atts = array(
                                'width'       => 800,
                                'height'      => 400,
                                'scrollbars'  => 'yes',
                                'status'      => 'no',
                                'resizable'   => 'no',
                                'screenx'     => 0,
                                'screeny'     => 0,
                                'window_name' => '_self'
                            );
                            
                            echo anchor_popup('user/upload/dogovor', 'Изменить скан договора', $atts);
                        }
                        }
                        ?>
                        </td>
                    </tr>
                    </table>
                    
                    <br /><br /><br />
                    <h5>Пси информация</h5>
                    Начало психологической деятельности: <strong><?=$this->data->format_data_my($info->psy_from)?></strong> (полных лет:<strong> <?=$this->data->f_y($info->psy_from)?></strong>)<br />
                    Психотерапевтическая модальность:  <strong>
                    <?php
                    echo $this->user_model->user_psymod($info->psymod);
                    ?>
                    </strong><br /><br />
                    Формы работы: <br /><strong>
                    <?php
                    $forms = explode('*/*',$info->psy_form);
                    foreach($forms as $f):
                    echo '- '.$f.'<br />';
                    endforeach;
                    ?>
                    </strong><br /><br />
                    Возрастные группы: <br /><strong>
                    <?php
                    $forms = explode('*/*',$info->psy_vzr);
                    foreach($forms as $f):
                    echo '- '.$f.'<br />';
                    endforeach;
                    ?>
                    </strong><br /><br />
                    Типы проблемм: <br /><strong>
                    <?php
                    $forms = explode('*/*',$info->psy_tip);
                    foreach($forms as $f):
                    echo '- '.$f.'<br />';
                    endforeach;
                    ?>
                    </strong><br />
                    
                    </td>
                </tr>
            </table>          
        
        </div>
      <div id="tabs-2">
      <?php
      if($info->status<5)
      {
      ?>
      Этот раздел для Вас пока недоступен
      <?php  
      }
      ?>
      </div>
      <div id="tabs-3">
      <?php
      if($info->status<5)
      {
      ?>
      Этот раздел для Вас пока недоступен
      <?php  
      }
      ?>
      </div>
      <div id="tabs-4">
      <?php
      if($info->status<5)
      {
      ?>
      Этот раздел для Вас пока недоступен
      <?php  
      }
      ?>
      </div>
      <div id="tabs-5">
      <?php
      if($info->status<5)
      {
      ?>
      Этот раздел для Вас пока недоступен
      <?php  
      }
      ?>
      </div>
    </div>       
                 
</div>
<div style="clear: both;"></div>