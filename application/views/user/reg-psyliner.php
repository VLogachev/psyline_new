<div class="component">
    <h1>Регистрация псилайнера</h1>
    <br /><br />
    
    <div class="block-form-orange">
    <p><strong>Важная информация!</strong></p>
    <p>Регистрация в качестве псилайнера на сайте PSY-LINE.ORG эквивалентна вступительному заявлению в 
    Международную Организацию Псилайна (МОП).</p><br />
    <p>Стать членом МОП может специалист одного из следующих профилей:</p>
    - Психолог<br />
    - Психотерапевт<br />
    - Психиатр<br />
    - Нарколог<br />
    - Дефектолог<br />
    - Логопед<br /><br />
    <p>Процедура вступления в МОП включает в себя несколько этапов:</p><br />
    1. <strong>Заполнение анкеты на сайте PSY-LINE.ORG.</strong> После заполнения анкеты будет проверен адрес e-mail, 
    который в дальнейшем станет основным индентефикатором для входа в систему.<br /><br />
    2. <strong>Подтверждение своей квалификации.</strong> После подтверждения e-mail, <a href="#">комитетом по сертификации 
    Международной Организации Псилайна (КС МОП)</a> будет принято предварительное решение о возможности вступления кандидата в МОП. Как положительное, так 
    и отрицательное предварительное решение будет сообщено кандидату в течении 3 рабочих дней. В случае положительного 
    предварительного решения кандидату будет предложено отправить в КС МОП отсканированные копии паспорта и диплома(ов),
    подтверждающих личность и квалификацию специалиста. По итогам проверки этих документов будет вынесено 
    окончательное решение о вступлении в МОП. На усмотрение КС МОП может производится дополнительное онлайн-собеседование.<br /><br />
    3. <strong>Подписание договора.</strong> После принятия окончательного положительного решения псилайнеру будет выслан 
    текст договора о правилах использования ресурса портала PSY-LNE.ORG при проведении консультативной онлайн-деятельности. 
    По подписанию этого договара и передачи отсканированной подписанной копии в СК МОП, кандидат становится членом МОП и 
    получает право использовать ресурс PSY-LINE.ORG 
    для проведения коммерческой консультативной деятельности.<br />
    </div>
    
    
    <p style="text-indent: 0 !important;">
    <input type="checkbox" id="agree" /><strong>&nbsp;Я подтверждаю, что:</strong><br />
    - Ознакомился с информацией по процедуре регистрации;<br />
    - Ознакомился с <a href="#">этическим кодексом МОП</a> и готов следовать его положениям;<br />
    - Обладаю необходимым образованием и квалификацией для встпупления в МОП.</p>
    <br /><br />
    
    
    <div class="block-form" id="ifagree" style="display: <?=$display_form;?>;">
    
    <h2>Регистрационная анкета</h2>
    
    <?
    echo form_open('registration/reg_psyliner');
    ?>
        
    <table>
    <tr>
        <td style="width: 250px;">Ваш E-Mail<span>*</span></td>
        <td><input  type="text" name="email" value="<?php echo set_value('email'); ?>"/>
        <?php echo form_error('email'); ?>
        </td>
    </tr>
     <tr>
        <td>Дата рождения<span>*</span></td>
        <td><input class="dr" type="text" style="width: 100px;" name="dr" value="<?php echo set_value('dr'); ?>" />
        <?php echo form_error('dr'); ?>
        </td>
    </tr>
    <tr>
        <td>Фамилия<span>*</span></td>
        <td>
        <input type="text" name="n1" value="<?php echo set_value('n1'); ?>"/>
        <?php echo form_error('n1'); ?>
        </td>
    </tr>
    <tr>
        <td>Имя<span>*</span></td>
        <td>
        <input type="text" name="n2" value="<?php echo set_value('n2'); ?>" />
        <?php echo form_error('n2'); ?>
        </td>
    </tr>
    <tr >
        <td>Отчество</td>
        <td><input  type="text" name="n3" value="<?php echo set_value('n3'); ?>"/>
        <?php echo form_error('n3'); ?>
        </td>
    </tr>
    <tr>
        <td>Телефон</td>
        <td>
            <table>
            <tr>
                <td>+ <input class="tel1" type="text" style="width: 32px;" name="tel1" value="<?php echo set_value('tel1'); ?>"/>
            <br /><span style="font-size:0.7em; color: grey;">Код страны</span></td>
                <td><input class="tel2" type="text" style="width: 150px;" name="tel2" value="<?php echo set_value('tel2'); ?>"/>
                <br /><span style="font-size:0.7em; color: grey;">Номер телефона</span></td>
            </tr>
            </table>
        
        </td>
    </tr>
    <tr>
        <td>Страна<span>*</span></td>
        <td><input type="text" name="strana" value="<?php echo set_value('strana'); ?>"/>
        <?php echo form_error('strana'); ?>
        
        </td>
    </tr>
    <tr>
        <td>Регион<span>*</span></td>
        <td><input type="text" name="region" value="<?php echo set_value('region'); ?>"/>
        <?php echo form_error('region'); ?>
        </td>
    </tr>
    <tr>
        <td>Город/Населенный пункт<span>*</span></td>
        <td><input type="text" name="np" value="<?php echo set_value('np'); ?>"/>
        <?php echo form_error('np'); ?>
        </td>
    </tr>
   
    <tr>
        <td>Высшее образование<span>*</span></td>
        <td><select name="obrz">
            <option value="1">Высшее Психологическое (бакалавр)</option>
            <option value="2">Высшее Психологическое (специалист)</option>
            <option value="3">Высшее Психологическое (магистр)</option>
            <option value="4">Высшее Медицинское (психиатр)</option>
            <option value="5">Высшее Медицинское (психотерапевт)</option>
            <option value="6">Высшее Медицинское (нарколог)</option>
            <option value="7">Логопед</option>
            <option value="8">Дефектолог</option>
            <option value="9">Другое</option>
        </select></td>
    </tr>
    <tr>
        <td>ВУЗ<span>*</span></td>
        <td><input type="text" name="vuz" value="<?php echo set_value('vuz'); ?>"/>
        <?php echo form_error('vuz'); ?>
        </td>
    </tr>
    <tr>
        <td>Год окончания ВУЗа<span>*</span></td>
        <td><input class="god" type="text" style="width: 75px;" name="god" value="<?php echo set_value('god'); ?>"/>
        <?php echo form_error('god'); ?>
        </td>
    </tr>
    <tr>
        <td>Дополнительное образование<br />
        <span style="font-size: 0.7em; color: gray;">
        Укажите здесь сертификаты и свидетельства о дополнительном образовании с учебным объемом от 36 часов
        </span>
        </td>
        <td>
        <textarea name="dop_obrz" style="width: 350px; height: 200px; padding: 5px;"><?php echo set_value('dop_obrz'); ?></textarea></td>
        </td>
    </tr>
    
    <tr>
        <td>Дата начала профильной практики (Месяц/Год)<span>*</span></td>
        <td><input type="text" class="m_g" style="width: 75px;" name="psy-from" value="<?php echo set_value('psy-from'); ?>"/>
        <?php echo form_error('psy-from'); ?></td>
    </tr>
    <tr>
        <td>Базовая психотерапевтическая модальность<span>*</span></td>
        <td>
        <select name="psymod">
        <?php
        $query = $this->db->get_where('psy_mod',array('status'=>1));
        $mods = $query->result();
        foreach($mods as $mod):
        ?>
        <option value="<?=$mod->id?>"><?=$mod->name?></option>
        <?php endforeach; ?>
        <option value="0">Другое</option>
        </select></td>
        </td>
    </tr>
     <tr>
        <td>Формы работы<span>*</span></td>
        <td>
        <input type="checkbox" name="psy-form[]" value="Индивидуальное консультирование"/> <span style="color: #252525; font-size: 0.75em;">Индивидуальное консультирование</span><br />
        <input type="checkbox" name="psy-form[]" value="Супружеское консультирование"/> <span style="color: #252525; font-size: 0.75em;">Супружеское консультирование</span><br />
        <input type="checkbox" name="psy-form[]" value="Семейное консультирование"/> <span style="color: #252525; font-size: 0.75em;">Семейное консультирование</span><br />
        <input type="checkbox" name="psy-form[]" value="Груповое консультирование"/> <span style="color: #252525; font-size: 0.75em;">Груповое консультирование</span>
        <?php echo form_error('psy-form'); ?></td>
    </tr>
    <tr>
        <td>Возрастные клиентские группы<span>*</span></td>
        <td>
        <input type="checkbox" name="psy-vzr[]" value="Дети до 6 лет"/> <span style="color: #252525; font-size: 0.75em;">Дети до 6 лет</span>
        <input type="checkbox" name="psy-vzr[]" value="Дети от 6 до 12 лет"/> <span style="color: #252525; font-size: 0.75em;">Дети от 6 до 12</span><br />
        <input type="checkbox" name="psy-vzr[]" value="Подростки"/> <span style="color: #252525; font-size: 0.75em;">Подростки</span><br />
        <input type="checkbox" name="psy-vzr[]" value="Взрослые"/> <span style="color: #252525; font-size: 0.75em;">Взрослые</span><br />
        <input type="checkbox" name="psy-vzr[]" value="Пожилые"/> <span style="color: #252525; font-size: 0.75em;">Пожилые</span>
        <?php echo form_error('psy-vzr'); ?></td>
    </tr>
    <tr>
        <td>Тип проблематики<span>*</span></td>
        <td>
        <input type="checkbox" name="psy-tip[]" value="Кризисы"/> <span style="color: #252525; font-size: 0.75em;">Кризисы</span><br />
        <input type="checkbox" name="psy-tip[]" value="Неврозы"/> <span style="color: #252525; font-size: 0.75em;">Неврозы</span><br />
        <input type="checkbox" name="psy-tip[]" value="Пограничная проблематика"/> <span style="color: #252525; font-size: 0.75em;">Пограничная проблематика</span><br />
        <input type="checkbox" name="psy-tip[]" value="Психозы"/> <span style="color: #252525; font-size: 0.75em;">Психозы</span><br />
        <input type="checkbox" name="psy-tip[]" value="Зависимости"/> <span style="color: #252525; font-size: 0.75em;">Зависимости</span><br />
        <input type="checkbox" name="psy-tip[]" value="Дефекты развития"/> <span style="color: #252525; font-size: 0.75em;">Дефекты развития</span>
        <?php echo form_error('psy-tip'); ?></td>
    </tr>
    <tr>
        <td>Членство и статус в признаных психологических или медицинских сообществах<br />
        </td>
        <td>
        <textarea name="orgs" style="width: 350px; height: 100px; padding: 5px;"></textarea></td>
        </td>
    </tr>
    
    <tr>
        <td>Желаемый статус МОП<span>*</span><br />
        <span style="font-size: 0.7em; color: gray;">
        Ознакомиться с <a href="#">Положением о статусе членов МОП</a> 
        </span>
        </td>
        <td>
        <select name="status">
        <?php
        $query = $this->db->get_where('groups',array('status'=>1,'id >'=>3,'id <'=>8));
        $mods = $query->result();
        foreach($mods as $mod):
        ?>
        <option value="<?=$mod->id?>"><?=$mod->name?></option>
        <?php endforeach; ?>
        </select>
        </td>
        
    </tr>
  
    <tr>
        <td>Защитный код<span>*</span></td>
        <td>
        <?php
            $this->load->helper('captcha');
            $this->load->helper('string');
            $string = random_string('numeric', 10); // создаем произвольную строку из семи цифр
            
            $vals = array(
            'word' => $string,
            'img_path'	=> './captcha/',
            'img_url'	=> base_url().'captcha/',
            'img_width' => 150, // ширина капчи
            'img_height' => 50 // высота капчи
            );
        
            $cap = create_captcha($vals);
            
            $data = array(
                'captcha_time'	=> $cap['time'],
                'ip_address'	=> $this->input->ip_address(),
                'word'	=> $cap['word']
                );
            
            $query = $this->db->insert_string('captcha', $data);
            $this->db->query($query);
            
            echo '<input style="width:75px;" type="text" name="captcha" value="" /><br />
            <span style="font-size:0.7em; color: grey;">Введите 10 цифр с картинки</span><br />';
            echo $cap['image'];
            ?>
        </td>
        </tr>
        <tr>
        <td colspan="2"><input type="submit" value="Подать заявку на регистрацию"/></td>
    </tr>
    
    
    </table>
    </form>
    </div>
    
    
</div>