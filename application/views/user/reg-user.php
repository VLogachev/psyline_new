<div class="component">
    <h1>Регистрация пользователя</h1>
    <br /><br />
    
    <div class="block-form" id="ifagree" >
    
    <?
        echo form_open('registration/reg_user');
    ?>
        
    <table>
    <tr>
        <td style="width: 250px;">Ваш E-Mail<span>*</span></td>
        <td><input  type="text" name="email" value="<?php echo set_value('email'); ?>"/>
        <?php echo form_error('email'); ?>
        </td>
    </tr>
     <tr>
        <td>Дата рождения<span>*</span></td>
        <td><input class="dr" type="text" style="width: 100px;" name="dr" value="<?php echo set_value('dr'); ?>" />
        <?php echo form_error('dr'); ?>
        </td>
    </tr>
    <tr>
        <td>Фамилия<span>*</span></td>
        <td>
        <input type="text" name="n1" value="<?php echo set_value('n1'); ?>"/>
        <?php echo form_error('n1'); ?>
        </td>
    </tr>
    <tr>
        <td>Имя<span>*</span></td>
        <td>
        <input type="text" name="n2" value="<?php echo set_value('n2'); ?>" />
        <?php echo form_error('n2'); ?>
        </td>
    </tr>
    <tr >
        <td>Отчество</td>
        <td><input  type="text" name="n3" value="<?php echo set_value('n3'); ?>"/>
        <?php echo form_error('n3'); ?>
        </td>
    </tr>
    
    <tr>
        <td>Страна<span>*</span></td>
        <td><input type="text" name="strana" value="<?php echo set_value('strana'); ?>"/>
        <?php echo form_error('strana'); ?>
        
        </td>
    </tr>
    <tr>
        <td>Регион<span>*</span></td>
        <td><input type="text" name="region" value="<?php echo set_value('region'); ?>"/>
        <?php echo form_error('region'); ?>
        </td>
    </tr>
    <tr>
        <td>Город/Населенный пункт<span>*</span></td>
        <td><input type="text" name="np" value="<?php echo set_value('np'); ?>"/>
        <?php echo form_error('np'); ?>
        </td>
    </tr>
  
    <tr>
        <td>Защитный код<span>*</span></td>
        <td>
        <?php
            $this->load->helper('captcha');
            $this->load->helper('string');
            $string = random_string('numeric', 10); // создаем произвольную строку из семи цифр
            
            $vals = array(
            'word' => $string,
            'img_path'	=> './captcha/',
            'img_url'	=> base_url().'captcha/',
            'img_width' => 150, // ширина капчи
            'img_height' => 50 // высота капчи
            );
        
            $cap = create_captcha($vals);
            
            $data = array(
                'captcha_time'	=> time(),
                'ip_address'	=> $this->input->ip_address(),
                'word'	=> 123
                );
            
            $query = $this->db->insert_string('captcha', $data);
            $this->db->query($query);
            
            echo '<input style="width:75px;" type="text" name="captcha" value="" /><br />
            <span style="font-size:0.7em; color: grey;">Введите 10 цифр с картинки</span><br />';
            echo $cap['image'];
            ?>
        </td>
        </tr>
        <tr>
        <td colspan="2"><input type="submit" value="Подать заявку на регистрацию"/></td>
    </tr>
    
    
    </table>
    </form>
    </div>
    
    
</div>