<div class="component" >
    
    <h1>Личный кабинет (<?=$info->username?>)</h1>
    <br /><br />
    
    
    <div id="tabs">
      <ul>
          <li><a href="#tabs-1">Общая информация</a></li>
          <li><a href="#tabs-2">Консультации</a></li>
          <li><a href="#tabs-3">Сообщения</a></li>
        
      </ul>
      <div id="tabs-1">
      <table style="width: 990px;">      
                <tr class="more-info-tr">
                    
                    <td colspan="2" style="position: relative;">
                    
                    <table style="width: 990px;">
                        <tr>
                        <td style="width: 250px; vertical-align: top;">
                        <h2><?=$info->username?></h2><br />
                            
                            <?php
                            if($info->foto == '')
                            {
                            ?> 
                            <img style="border: 1px solid silver;" src="<?=base_url()?>images/nofoto.jpg"/> <br />
                            <?php
                            }
                            else
                            {
                            ?>
                            <img src="<?=base_url()?>users_img/<?=$info->foto;?>"/> <br />
                            <?php
                            }
                            ?>
                            <br />
                            
                               
                        </td>
                        
                            
                        <td style="vertical-align: top;">
                        
                        <div class="profile_set_buts">
                            <div rel="info1"><img src="<?=base_url()?>icos/info.png"/>Общая информация</div>
                            <div rel="foto1"><img src="<?=base_url()?>icos/foto.png"/>Загрузить аватар</div>
                            <div rel="pass1"><img src="<?=base_url()?>icos/lock.png"/>Смена пароля</div>
                        </div>
                        <br />
                        
                        <div class="hide_form_1" id="info1" style="display: block;">
                        <h3>Общая информация</h3><br />
                        Дата рождения: <strong><?=$this->data->format_data($info->dr)?></strong> 
                            (полных лет:<strong> <?=$this->data->f_y($info->dr)?></strong>)<br /><br />
                        Страна: <strong><?=$info->strana?></strong><br /><br />
                        Регион: <strong><?=$info->region?></strong><br /><br />
                        Нас. Пункт: <strong><?=$info->np?></strong>   
                        </div>
                        
                        <div class="hide_form_1" id="pass1">
                        <h3>Изменить пароль</h3>
                        Новый пароль должен содержать не менее 6 символов
                        <br /><br />
                        <form action="<?=base_url()?>user/ch_pass" method="post">
                        <input type="password" name="old" /> <p>Текущий пароль:</p><br /><br />
                        <input type="password" name="new" /> <p>Новый пароль:</p><br /><br />
                        <input type="password" name="new1" /> <p>Новый пароль еще раз:</p><br /><br />
                        <input type="submit" style="margin-left: 220px; " value="Изменить пароль" />
                        </form>
                        </div>
                        
                        <div class="hide_form_1" id="foto1">
                        <h3>Загрузить аватар профиля</h3><br /><br />
                        
                        <?php echo form_open_multipart('user/do_upload_user_avatar');?>
                    
                        <input type="file" name="userfile" size="20" />
                        <input type="hidden" name="user_id" value="<?=$info->id?>"/>
                        <br /><br />
                        
                        <input type="submit" value="Загрузить" />
                        
                        </form>
                        </div>
                    
                        </td>
                        </tr>
                    </table>
                    </td>
                   
                   </tr>
            </table>          
        
        </div>

        <div id="tabs-2">
            <h2>Консультации</h2>
            <?php $this->load->view('consultation/_cons_grid.php',['consultations' => $consultations]); ?>
        </div>

      <div id="tabs-3">
      <h2>Сообщения</h2>
      <?php $this->load->view('user/profile/inc/msgs');?>
      </div>
      
    </div>       
                 
</div>
<div style="clear: both;"></div>