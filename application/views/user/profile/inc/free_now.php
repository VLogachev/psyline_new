<?php
        $pid = $this->user_model->psyliner_id($this->session->userdata('user_id'));
        $this->db->select('cons_now, free_cons');
        $this->db->where(array('psyliner_id'=>$pid));
        $sql = $this->db->get('psyliners_params');
        $res = $sql->row();
        
        if($res->cons_now == 1)
        {
            $ch1 = 'checked'; $z1 = 1;
        }
        else
        {
            $ch1 = ''; $z1 = 0;
        }
        if($res->free_cons == 1)
        {
            $ch2 = 'checked'; $z2 = 1;
        }
        else
        {
            $ch2 = ''; $z2 = 0;
        }
?>
<div id="free_now">
    <table>
    <tr>
    <td>Консультация прямо сейчас?&nbsp;</td>
    <td>
        <div class="onoffswitch_now" uid="<?=$pid?>" znch="<?=$z1?>">
        <input type="checkbox" name="onoffswitch_now" class="onoffswitch_now-checkbox" id="myonoffswitch_now" <?=$ch1?> />
        <label class="onoffswitch_now-label" for="myonoffswitch_now">
            <span class="onoffswitch_now-inner"></span>
            <span class="onoffswitch_now-switch"></span>
        </label>
        </div>
        
    </td>
   <td>&nbsp;&nbsp;&nbsp;&nbsp;Бесплатная консультация?&nbsp;</td>
    <td>
        <div class="onoffswitch_free" uid="<?=$pid?>" znch="<?=$z2?>">
        <input type="checkbox" name="onoffswitch_free" class="onoffswitch_free-checkbox" id="myonoffswitch_free" <?=$ch2?> />
        <label class="onoffswitch_free-label" for="myonoffswitch_free">
            <span class="onoffswitch_free-inner"></span>
            <span class="onoffswitch_free-switch"></span>
        </label>
        </div>
    </td>
    </tr>
    </table>
    </div>