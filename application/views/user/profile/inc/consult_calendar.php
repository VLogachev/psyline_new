

<p style="text-indent: 0px;">Здесь Вы можете указать даты и время проведения online консультаций на ближайший месяц</p>
<br />
<table style="width: 990px;">
<tr>

<td style="vertical-align: top; width: 68%;">
<h3>Выберите дату</h3>
<div class="calendar">
<?php
$days = array('Пнд','Втр','Срд','Чтв','Птн','<span style="color:#c00;">Сбт</span>','<span style="color:#c00;">Вск</span>');
$mes_array = array('Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря');
$mes_short_array = array('Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек');

$now = time();
$sutki = 86400;

$denn = date('N',$now);//день недели
$den = date('j',$now);
$mes = date('n',$now);
$god = date('Y',$now);
$fst = mktime(0,0,1,$mes,$den+1,$god);//Первая секунда завтрашнего дня


$ii = $denn;
echo '<div style="width:'.((7-(7-$denn))*92).'px; height:15px;display: inline-block;"></div>';
for ($i = $fst; $i <= $fst+($sutki*31); $i=$i+$sutki) {
    $ii++;
    
    //Количество часов дня
    $this->db->where(array(
    'psyliner_id'=>$this->user_model->psyliner_id($this->session->userdata('user_id')),
    'time_from >='=> $i,
    'time_from <='=> $i+($sutki-2)
    ));
    
    $sql = $this->db->get('cons_time_for');
    $n = $sql->num_rows();
    if($n>0){$s = 'style="background-color: #D6FFD2;"';}else{$s = '';}
    
    echo '<div '.$s.' class="cal_day" t="'.$i.'" uid="'.$this->session->userdata('user_id').'">';
    $denn = date('N',$i);//день недели
    $den = date('j',$i);
    $mes = date('n',$i);
    $god = date('Y',$i);
    echo $days[$denn-1].', '.$den.' '.$mes_short_array[$mes-1].'<div title="Количество консультаций в этот день" class="n_cons">'.$n.'</div>';
    echo '</div>';
    if($ii%7==0){echo '<br />';}
}
?>
</div>

<div id="cal_time"></div>

</td>
<td style="vertical-align: top;" >
    <div id="cal_cons_block">
    <h3>Список изменений</h3>
    <div id="cal_cons"></div>
    <div style="text-align: center;">
    <br />Изменения отразятся в календаре после<br /><span id="rel_l">обновления страницы</span>
                                                                        
    </div>
    </div>
</td>

</tr>
</table>