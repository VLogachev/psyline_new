<?php
$uid = $this->session->userdata('user_id');

$all = $this->articles_model->num_user_articles_all($uid);

//Опубликованные
$pub = $this->articles_model->num_user_articles($uid);


if($all > 0)
{
    $articles = $this->articles_model->get_user_articles_all($uid);
}
?>
<table style="width: 990px;">
<tr>
    <td style="width: 200px; vertical-align: top; padding-right: 35px;">
    
    <div class="msg_nav">
    <div>Ваши публикации (<?=$all?>)</div>
    <span><?=$all?></span><p>Все</p>
    <span><?=$pub?></span><p>Опубликованные</p>
    <span><?=$all-$pub?></span><p>Не опубликованные</p>
    </div>
    
    </td>
    <td>

    <?php $nnn = rand(0,9);?>
    <div class="hide_block_wrap" style="width: 738px;">
    <div class="hide_block_head" par="<?=$nnn?>" act="sd">Создать публикацию<div>Открыть &#9660;</div></div>
    <div class="hide_block hide_block<?=$nnn?>">
    <br />
    <form action="<?=base_url()?>articles/add_article" method="post">
    <br />
    <span style="font-size: 1.2em;">Раздел: </span>
    <select name="cat_id">
    <?php 
    $categorys = $this->articles_model->user_categorys();
    foreach($categorys as $cat):?>
        <option value="<?=$cat->id?>"><?=$cat->name?></option> 
        <?php endforeach; ?>
        </select>
    <br /><br />
    <span style="font-size: 1.2em;">Заголовок: </span><br />
    <input type="text" style="width: 712px; padding: 5px; font-family: Trebuchet MS,Tahoma,Verdana,Arial,sans-serif;font-size: 1.2em;" name="header"/><br /><br />
    
    <script type="text/javascript">
	bkLib.onDomLoaded(function() {
        new nicEditor({
        	buttonList : 
        	['bold','italic','underline','strikeThrough','html','left','center','right',
            'image','upload','link','unlink']
        	
        	}
        	).panelInstance('articleadd'); 
        }
        );
    </script>
    <textarea name="text" id="articleadd" style="width: 725px; height: 300px;"></textarea>
    <br />
    <input type="submit" value="Добавить статью" />
    </form>
    </div>
    </div>
    <br />

<div id="articles_block">
<?php
foreach($articles as $item):

//ФОТО И ИМЯ АДМИНИСТРАЦИИ
if($msg->from == 0)
{
    $img = base_url().'images/psyline.jpg';
    $name = 'Администрация PSY-LINE.ORG';
}
else
{
    //ПСИЛАЙНЕР?
    $is = $this->user_model->is_psyliner($msg->from);
    //ДА!
    if($is == true)
    {
        $user_from = $this->user_model->psyliner_info($msg->from);
        
        $img = base_url().'users_img/'.$user_from->foto;
        $name = $user_from->name.' (Псилайнер)';
    }
    
}
?>
<div class="item_msg">
    <table style="width: 100%;">
        <tr id="<?='msg'.$msg->id;?>" <?php if($msg->new == 1){echo "class='new_msg'";}?>>
            <td style="width: 65px; text-align: left; vertical-align: top;">
                <div class="img" style="background-image: url('<?=$img?>');"></div>
            </td>
            <td style="padding-left: 55px;">
                <div class="msg_content">
                
                <span style="font-size: 0.85em; color: gray;"><?php $this->data->create_data($msg->cr_time);?></span>
                <br />
                
                От: <strong><?=$name;?></strong> 
                &nbsp;&nbsp;&nbsp;&nbsp;
                Кому: <strong><?=$this->user_model->user_name_by_id($msg->to);?></strong>
                <br /><br />
                <em class="msg<?=$msg->id?>pretext" style="color: gray;"><?=mb_substr($msg->text,0,100).'...'?></em>
                
                <div class="show_msg" msgid="<?=$msg->id;?>"></div>
                
                <div class="msg<?=$msg->id?>text" style="display: none;">
                <?=nl2br($msg->text);?><br />
                    <div msgid="<?=$msg->id;?>" class="msg_hide">Свернуть сообщение</div>
                </div>
                
                </div>
            </td>
        </tr>
    </table>
</div>
<?php
endforeach;
?>
</div>
</td>
</tr>
</table>