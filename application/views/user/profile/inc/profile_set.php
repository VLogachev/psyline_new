<?php
$params = $this->user_model->psyliner_params($this->session->userdata('user_id'));
$psyliner_id = $this->user_model->psyliner_id($this->session->userdata('user_id'));
$rang = $this->user_model->psyliner_rang($psyliner_id);
$price = $this->user_model->psyliner_prices($rang);
$foto = $this->user_model->psyliner_foto($psyliner_id);
if($params->video_url != ''){
    $in = strpos($params->video_url,'v=');
    $vind = substr($params->video_url,$in+2);
}
?>
<br /><br />
<table style="width: 990px;">
<tr>
    <td class="set_links" style="vertical-align: top;" >
    <a href="#" rel="foto">Изменить фото профиля</a>
    <a href="#" rel="pass">Смена пароля</a>
    <a href="#" rel="msg">Личное обращение</a>
    <a href="#" rel="cons">On-Line Консультации</a>
    <a href="#" rel="personal">Личная встреча</a>
    <a href="#" rel="videourl">Презентационное видео</a>
    <a href="#" rel="url">Ваша страничка в интернете</a>
    </td>
    
    <td style="vertical-align: top;">
    &nbsp;
    <div class="hide_form" id="foto">
    <?php
    if($foto == ''){
    ?> 
    <img style="border: 1px solid silver;" src="<?=base_url()?>images/nofoto.jpg"/> <br />
    <?php
    $atts = array(
    'width'       => 800,
    'height'      => 400,
    'scrollbars'  => 'yes',
    'status'      => 'no',
    'resizable'   => 'no',
    'screenx'     => 0,
    'screeny'     => 0,
    'window_name' => '_self'
    );
                            
    echo anchor_popup('user/upload/foto', 'Загрузить новое фото', $atts, 'blue_link');
    }
    else
    {
    ?>
    <img src="<?=base_url()?>users_img/<?=$foto;?>"/> <br />
    <?php
        
        $atts = array(
        'width'       => 800,
        'height'      => 400,
        'scrollbars'  => 'yes',
        'status'      => 'no',
        'resizable'   => 'no',
        'screenx'     => 0,
        'screeny'     => 0,
        'window_name' => '_self'
        );
                                    
        echo anchor_popup('user/upload/foto', 'Измнить фото профиля', $atts,'blue_link');
        
    }
    ?>
    </div>
    
    <div class="hide_form" id="pass">
    <h3>Изменить пароль</h3>
    Новый пароль должен содержать не менее 6 символов
    <br /><br />
    <form action="<?=base_url()?>user/ch_pass" method="post">
    <input type="password" name="old" /> <p>Текущий пароль:</p><br /><br />
    <input type="password" name="new" /> <p>Новый пароль:</p><br /><br />
    <input type="password" name="new1" /> <p>Новый пароль еще раз:</p><br /><br />
    <input type="submit" style="margin-left: 220px; " value="Изменить пароль" />
    </form>
    </div>
    
    <div class="hide_form" id="msg">
    <h3>Личное обращение</h3>
    Личное обращение это текст, которы будут видеть пользователи на вашей страничке.
    <br /><br />
    <form action="<?=base_url()?>user/personal_msg" method="post">
    <script type="text/javascript">
	bkLib.onDomLoaded(function() {
        new nicEditor({
        	buttonList : 
        	['bold','italic','underline','strikeThrough','html','left','center','right',
            'image','upload','link','unlink']
        	
        	}
        	).panelInstance('textedit'); 
        }
        );
    </script>
    <textarea name="msg" id="textedit" style="width: 600px; height: 200px;"><?=$params->pers_msg?></textarea>
    <br />
    <input type="submit" style="margin-left: 450px; " value="Cохранить" />
    </form>
    </div>
    
    <div class="hide_form" id="cons">
    <h3>On-Line Консультации</h3>
    <br /><br />
    <table class="cons_tbl">
    <tr>
        <td style="width: 200px;"><strong>Видеоконсультация на сайте</strong></td>
        <?php if($rang<7)
        {?>
        <td style="width: 250px;">Стоимость: <strong>&#8381; <?=$price->video_price?>.00</strong> (50 мин.)</td>    
        <?php }
        else
        {?>
        <td style="width: 250px;">50 мин.: <input id="v_price_zn" ps_id="<?=$psyliner_id?>" value="<?=$params->video_price?>" type="text" style="width: 35px; margin-left: 0;"/><strong>  &#8381;</strong> 
        <div class="blue_block" id="video_price">Установить</div>
        </td>      
        <?php } ?>
         
        
        <td>
            <div class="onoffswitch">
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" 
                userid="<?=$psyliner_id?>"
                tip="video"
                znch="<?=$params->video?>"
                id="myonoffswitch" <?php if($params->video==1){echo 'checked';}?>/>
                
                
                <label class="onoffswitch-label" for="myonoffswitch">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                </label>
            </div>
        </td>
    </tr>
    <tr>
        <td style="width: 200px;"><strong>Аудиоконсультация на сайте</strong></td>
        <?php if($rang<7)
        {?>
        <td style="width: 250px;">Стоимость: <strong>&#8381; <?=$price->audio_price?>.00</strong> (45 мин.)</td>    
        <?php }
        else
        {?>
        <td style="width: 250px;">45 мин.: <input id="a_price_zn" ps_id="<?=$psyliner_id?>" value="<?=$params->audio_price?>" type="text" style="width: 35px; margin-left: 0;"/><strong>  &#8381;</strong> 
        <div class="blue_block" id="audio_price">Установить</div>
        </td>      
        <?php } ?>
        <td>
            <div class="onoffswitch">
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                userid="<?=$psyliner_id?>"
                tip="audio"
                znch="<?=$params->audio?>"
                id="myonoffswitch1" <?php if($params->audio==1){echo 'checked';}?>/>
                
                <label class="onoffswitch-label" for="myonoffswitch1">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                </label>
            </div>
        </td>
    </tr>
    <tr>
        <td style="width: 200px;"><strong>Приватный текст-чат на сайте</strong></td>
        <?php if($rang<7)
        {?>
        <td style="width: 250px;">Стоимость: <strong>&#8381; <?=$price->text_price?>.00</strong> (60 мин.)</td>    
        <?php }
        else
        {?>
        <td style="width: 250px;">60 мин.: <input id="t_price_zn" ps_id="<?=$psyliner_id?>" value="<?=$params->text_price?>" type="text" style="width: 35px; margin-left: 0;"/><strong>  &#8381;</strong> 
        <div class="blue_block" id="text_price">Установить</div>
        </td>      
        <?php } ?>
        <td>
            <div class="onoffswitch">
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" 
                userid="<?=$psyliner_id?>"
                tip="text"
                znch="<?=$params->text?>"
                id="myonoffswitch2" <?php if($params->text==1){echo 'checked';}?>/>
                
                
                <label class="onoffswitch-label" for="myonoffswitch2">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                </label>
            </div>
        </td>
    </tr>
    </table>
    </div>
    
    <div class="hide_form" id="personal">
    <h3>Личная встреча</h3>
    Условия и параметры личной встречи с клиентом
    <br /><br />
    <form action="<?=base_url()?>user/personal" method="post">
    <select name="person">
    <option value="0" <?php if($params->person==0){echo 'selected';}?>>Нет</option>
    <option value="1" <?php if($params->person==1){echo 'selected';}?>>Да</option>
    </select>
    <p>Личная встреча:</p><br /><br />
    <input type="text" name="person_city" value="<?=$params->person_city?>"/> <p>Город:</p><br /><br />
    <input type="text" name="person_adres" value="<?=$params->person_adres?>"/> <p>Адрес:</p><br /><br />
    <input style="width: 30px;" type="text" name="person_price" value="<?=$params->person_price?>"/> &#8381;<p>Стоимость:</p><br /><br />
    
    <textarea class="t" name="description"><?=$params->person_desc?></textarea><p>Условия и описание:</p><br /><br />
    <input type="submit" style="margin-left: 220px; " value="Сохранить" />
    </form>
    </div>
    
    <div class="hide_form" id="videourl">
    <h3>Ссылка на ваше видео в YouTube</h3>
    <br />
    Скопируйте ссылку вашего видео на YouTube из адресной строки вашего браузера и всавьте в поле "Ссылка".
    <br /><br />
    <form action="<?=base_url()?>user/set_videourl" method="post">
    <input type="text" name="video_url" value="<?=$params->video_url?>"/> <p>Ссылка:</p><br /><br />
    <?php if($params->video_url != ''){?>
    <a style="margin-left: 220px;" class="iframe cboxElement" href="https://www.youtube.com/embed/<?=$vind?>">Смотреть видео</a><br /><br />
    <?php } ?>
    <input type="submit" style="margin-left: 220px; " value="Сохранить" />
    </form>
    </div>
    
    <div class="hide_form" id="url">
    <h3>Ваша страничка в интернете</h3>
    <br /><br />
    <form action="<?=base_url()?>user/set_url" method="post">
    <input type="text" name="site_url" value="<?=$params->site_url?>"/> <p>Ссылка:</p><br /><br />
    <input type="submit" style="margin-left: 220px; " value="Сохранить" />
    </form>
    </div>
    
    </td>
</tr>
</table>
