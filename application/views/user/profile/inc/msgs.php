<?php
//Все сообщения
$this->db->where(array('to'=>$this->session->userdata('user_id')));
$this->db->or_where('from',$this->session->userdata('user_id'));
$sql = $this->db->get('msgs');
$n_msgs_all = $sql->num_rows();
//Входящие
$this->db->where(array('to'=>$this->session->userdata('user_id')));
$sql = $this->db->get('msgs');
$n_msgs_in = $sql->num_rows();
//Исходящие
$n_msgs_out = $n_msgs_all - $n_msgs_in;


$this->db->where(array('to'=>$this->session->userdata('user_id')));
$this->db->or_where('from',$this->session->userdata('user_id'));
$this->db->order_by('cr_time desc');
$sql = $this->db->get('msgs',5,0);

?>
<table style="width: 990px;">
<tr>
    <td style="width: 200px; vertical-align: top; padding-right: 35px;">
    
    <div class="msg_nav">
    <div>Ваши сообщения (<?=$n_msgs_all?>)</div>
    <span><?=$n_msgs_all?></span><p>Все</p>
    <span><?=$n_msgs_in?></span><p>Входящие</p>
    <span><?=$n_msgs_out?></span><p>Исходящие</p>
    </div>
    
    </td>
    <td>
<div id="msg_block">

<?php
$msgs = $sql->result();
foreach($msgs as $msg):

//ФОТО И ИМЯ АДМИНИСТРАЦИИ
if($msg->from == 0)
{
    $img = base_url().'images/psyline.jpg';
    $name = 'Администрация PSY-LINE.ORG';
}
else
{
    //ПСИЛАЙНЕР?
    $is = $this->user_model->is_psyliner($msg->from);
    //ДА!
    if($is == true)
    {
        $user_from = $this->user_model->psyliner_info($msg->from);
        
        $img = base_url().'users_img/'.$user_from->foto;
        $name = $user_from->name.' (Псилайнер)';
    }
    
}
?>
<div class="item_msg">
    <table style="width: 100%;">
        <tr id="<?='msg'.$msg->id;?>" <?php if($msg->new == 1){echo "class='new_msg'";}?>>
            <td style="width: 65px; text-align: left; vertical-align: top;">
                <div class="img" style="background-image: url('<?=$img?>');"></div>
            </td>
            <td style="padding-left: 55px;">
                <div class="msg_content">
                
                <span style="font-size: 0.85em; color: gray;"><?php $this->data->create_data($msg->cr_time);?></span>
                <br />
                
                От: <strong><?=$name;?></strong> 
                &nbsp;&nbsp;&nbsp;&nbsp;
                Кому: <strong><?=$this->user_model->user_name_by_id($msg->to);?></strong>
                <br /><br />
                <em class="msg<?=$msg->id?>pretext" style="color: gray;"><?=mb_substr($msg->text,0,100).'...'?></em>
                
                <div class="show_msg" msgid="<?=$msg->id;?>"></div>
                
                <div class="msg<?=$msg->id?>text" style="display: none;">
                <?=nl2br($msg->text);?><br />
                    <div msgid="<?=$msg->id;?>" class="msg_hide">Свернуть сообщение</div>
                </div>
                
                </div>
            </td>
        </tr>
    </table>
</div>
<?php
endforeach;
?>
</div>
</td>
</tr>
</table>