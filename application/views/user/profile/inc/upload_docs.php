  <?php
    $info = $this->user_model->psyliner_info($this->session->userdata['user_id']);
  ?> 
  <table style="width: 90%;">
                    <tr>
                        <td>
                        <?php
                        if($info->foto_diplom == ''){
                        ?> 
                        <img src="<?=base_url()?>images/nodoc.jpg"/> <br />
                        <?php
                        $atts = array(
                            'width'       => 800,
                            'height'      => 400,
                            'scrollbars'  => 'yes',
                            'status'      => 'no',
                            'resizable'   => 'no',
                            'screenx'     => 0,
                            'screeny'     => 0,
                            'window_name' => '_self'
                        );
                        
                        echo anchor_popup('user/upload/diplom', 'Загрузить скан диплома', $atts, 'upl_url w153');
                        }
                        else
                        {
                        ?>
                        <a href="<?=base_url()?>users_img/diplom/<?=$info->foto_diplom?>" class="box">
                        <div class="d_doc" style="background-image: url('<?=base_url()?>users_img/diplom/<?=$info->foto_diplom?>');"></div>
                        </a>
                        <br />
                        <?php
                         if($info->status == 2 OR $info->status == 3)
                         {
                            $atts = array(
                                'width'       => 800,
                                'height'      => 400,
                                'scrollbars'  => 'yes',
                                'status'      => 'no',
                                'resizable'   => 'no',
                                'screenx'     => 0,
                                'screeny'     => 0,
                                'window_name' => '_self'
                            );
                            
                            echo anchor_popup('user/upload/diplom', 'Изменить скан диплома', $atts, 'upl_url w153');
                            }
                        }
                        ?>
                        </td>
                        <td>
                        <?php
                        if($info->foto_pasport == ''){
                         ?> 
                        <img src="<?=base_url()?>images/nodoc.jpg"/> <br />
                        <?php
                        $atts = array(
                            'width'       => 800,
                            'height'      => 400,
                            'scrollbars'  => 'yes',
                            'status'      => 'no',
                            'resizable'   => 'no',
                            'screenx'     => 0,
                            'screeny'     => 0,
                            'window_name' => '_self'
                        );
                        
                        echo anchor_popup('user/upload/pasport', 'Загрузить скан паспорта', $atts, 'upl_url w153');
                        }
                        else
                        {
                        ?>
                        <a href="<?=base_url()?>users_img/pasport/<?=$info->foto_pasport?>" class="box">
                        <div class="d_doc" style="background-image: url('<?=base_url()?>users_img/pasport/<?=$info->foto_pasport?>');"></div>
                        </a>
                        <br />
                        <?php
                        if($info->status == 2 OR $info->status == 3)
                        {
                            $atts = array(
                                'width'       => 800,
                                'height'      => 400,
                                'scrollbars'  => 'yes',
                                'status'      => 'no',
                                'resizable'   => 'no',
                                'screenx'     => 0,
                                'screeny'     => 0,
                                'window_name' => '_self'
                            );
                            
                            echo anchor_popup('user/upload/pasport', 'Изменить скан паспорта', $atts, 'upl_url w153');
                        }
                        }
                        ?>
                        </td>
                        <td>
                        <?php
                        if($info->foto_dogovor == ''){
                         ?> 
                        <img src="<?=base_url()?>images/nodoc.jpg"/> <br />
                        <?php
                        $atts = array(
                            'width'       => 800,
                            'height'      => 400,
                            'scrollbars'  => 'yes',
                            'status'      => 'no',
                            'resizable'   => 'no',
                            'screenx'     => 0,
                            'screeny'     => 0,
                            'window_name' => '_self'
                        );
                        
                        echo anchor_popup('user/upload/dogovor', 'Загрузить скан договора', $atts, 'upl_url w153');
                        }
                        else
                        {
                        ?>
                        <a href="<?=base_url()?>users_img/dogovor/<?=$info->foto_dogovor?>" class="box">
                        <div class="d_doc" style="background-image: url('<?=base_url()?>users_img/dogovor/<?=$info->foto_dogovor?>');"></div>
                        </a>
                        <br />
                        <?php
                        if($info->status == 2 OR $info->status == 3)
                        {
                            $atts = array(
                                'width'       => 800,
                                'height'      => 400,
                                'scrollbars'  => 'yes',
                                'status'      => 'no',
                                'resizable'   => 'no',
                                'screenx'     => 0,
                                'screeny'     => 0,
                                'window_name' => '_self'
                            );
                            
                            echo anchor_popup('user/upload/dogovor', 'Изменить скан договора', $atts, 'upl_url w153');
                        }
                        }
                        ?>
                        </td>
                    </tr>
                    </table>