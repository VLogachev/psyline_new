<div class="component" >
    
    <h1>Личный кабинет</h1>
    <br /><br />
    <?php
    $info = $this->user_model->psyliner_info($this->session->userdata['user_id']);
    ?> 
    
    <div id="tabs">
      <ul>
        <li><a href="#tabs-1">Общая информация</a></li>
        <li><a href="#tabs-2">Сообщения</a></li>
        
      </ul>
      <div id="tabs-1">
      <table style="width: 990px;">      
                <tr class="more-info-tr">
                    
                    <td colspan="2" style="position: relative;">
                    
                    <div class="g_msg">
                    <strong>Внимание!</strong><br /><br />
                    <strong>Ваша анкета находится в процессе аттестации!</strong><br /><br />
                    О решении аттестационной комисси Вам будет сообщено по электронной почте.
                    </div><br /> 
                    
                    <table style="width: 990px;">
                        <tr>
                        <td style="width: 400px; vertical-align: top;">
                        <h2><?=$info->name?></h2><br />
                            
                            <img src="<?=base_url()?>users_img/<?=$info->foto;?>"/> <br />
                            <br /><br />
                            Образование: <strong><?echo $this->user_model->user_obrazovanie($info->obrz)?></strong><br /><br />
                            Статус: <strong><?=$this->user_model->user_status($info->rang)?></strong><br /><br />
                            Желаемый статус: <strong><?=$this->user_model->user_status($info->wish_rang)?></strong>
                            
                           
                        </td>
                        
                            
                        <td style="vertical-align: top;">
                        
                      
                    <h2>Информационные блоки</h2>
                    <br />
                        
                        
                        <?php $nnn = rand(0,9);?>
                        <div class="hide_block_wrap">
                        <div class="hide_block_head" par="<?=$nnn?>" act="sd">Основная информация<div>Показать &#9660;</div></div>
                        <div class="hide_block hide_block<?=$nnn?>">
                        <br />
                            Имя: <strong><?=$info->name?></strong><br />
                            Дата рождения: <strong><?=$this->data->format_data($info->dr)?></strong> 
                            (полных лет:<strong> <?=$this->data->f_y($info->dr)?></strong>)<br />
                            Страна: <strong><?=$info->strana?></strong><br />
                            Регион: <strong><?=$info->region?></strong><br />
                            Нас. Пункт: <strong><?=$info->np?></strong>    
                        </div>
                        </div>
                        <br />
                        
                        <?php $nnn = rand(10,19);?>
                        <div class="hide_block_wrap">
                        <div class="hide_block_head" par="<?=$info->id?>" act="sd">Образование<div>Показать &#9660;</div></div>
                        <div class="hide_block hide_block<?=$info->id?>">
                            <br />
                            Высшее образование: <strong><?echo $this->user_model->user_obrazovanie($info->obrz)?></strong><br />
                            Вуз: <strong><?=$info->vuz?></strong> Год окончания: <strong><?=date('Y',$info->god)?></strong><br /><br />
                            <strong>Дополнительное образование:</strong><br />
                            <?=nl2br($info->dop_obrz)?>
                        </div>
                        </div>
                        <br />
                        
                        <?php $nnn = rand(20,29);?>
                        <div class="hide_block_wrap">
                        <div class="hide_block_head" par="<?=$nnn?>" act="sd">Профессиональная информация<div>Показать &#9660;</div></div>
                        <div class="hide_block hide_block<?=$nnn?>">
                        <br />
                        
                        <strong>Членство в организациях:</strong><br />
                        <?=nl2br($info->orgs)?><br /><br />   
                        
                        Начало психологической деятельности: <strong><?=$this->data->format_data_my($info->psy_from)?></strong> (полных лет:<strong> <?=$this->data->f_y($info->psy_from)?></strong>)<br />
                        Психотерапевтическая модальность:  <strong>
                        <?php
                        echo $this->user_model->user_psymod($info->psymod);
                        ?>
                        </strong><br /><br />
                            
                        Формы работы: <br /><strong>
                        <?php
                        $forms = explode('*/*',$info->psy_form);
                        foreach($forms as $f):
                        echo '- '.$f.'<br />';
                        endforeach;
                        ?>
                        </strong><br /><br />    
                        
                        Возрастные группы: <br /><strong>
                        <?php
                        $forms = explode('*/*',$info->psy_vzr);
                        foreach($forms as $f):
                        echo '- '.$f.'<br />';
                        endforeach;
                        ?>
                        </strong><br /><br />
                            
                        Типы проблемм: <br /><strong>
                        <?php
                        $forms = explode('*/*',$info->psy_tip);
                        foreach($forms as $f):
                        echo '- '.$f.'<br />';
                        endforeach;
                        ?>
                        </strong><br />   
                         
                        </div>
                        </div>
                        <br />
                        
                        <?php $nnn = rand(30,39);?>
                        <div class="hide_block_wrap">
                        <div class="hide_block_head" par="<?=$nnn?>" act="sd">Контактная информация<div>Показать &#9660;</div></div>
                        <div class="hide_block hide_block<?=$nnn?>">
                            <br />
                            
                            E-Mail: <strong><?=$info->email?></strong><br />
                            Телефон: <strong><?=$info->telefon?></strong><br />
                            
                        </div>
                        </div>
                        <br /><br />
                        <?php $nnn = rand(40,49);?>
                        <div class="hide_block_wrap">
                        <div class="hide_block_head" par="<?=$nnn?>" act="sd">Загруженные документы<div>Показать &#9660;</div></div>
                        <div class="hide_block hide_block<?=$nnn?>">
                        <br />
                           
                           <div style="width: 470px; margin-left: 42px;" class="g_msg">
                           <h4>Обязательные документы</h4>
                           <br /><br />
                           <?$this->load->view('user/profile/inc/upload_docs');?>
                           </div>
                           <br />
                           <div style="width: 470px; margin-left: 42px;" class="g_msg">
                           <h4>Дополнительные документы</h4>
                           <br /><br />
                           <?$this->load->view('user/profile/inc/upload_docs2');?>
                           </div>
                            
                           
                              
                        </div>
                        </div>
                        </td>
                        </tr>
                    </table>
                    </td>
                   
                   </tr>
            </table>          
        
        </div>
      <div id="tabs-2">
      <h2>Сообщения</h2>
      <?php $this->load->view('user/profile/inc/msgs');?>
      </div>
      
    </div>       
                 
</div>
<div style="clear: both;"></div>