<div class="component" >
    
    <h1>Личный кабинет</h1>
    <br /><br />
    <?php
    $info = $this->user_model->psyliner_info($this->session->userdata['user_id']);
    ?> 
    
    <div id="tabs">
      <ul>
        <li><a href="#tabs-1">Общая информация</a></li>
        <li><a href="#tabs-2">Сообщения</a></li>
        
      </ul>
      <div id="tabs-1">
      <table style="width: 990px;">      
                <tr class="more-info-tr">
                    
                    <td colspan="2" style="position: relative;">
                    
                    <div class="g_msg">
                    <strong>Внимание!</strong><br />
                    <strong>Все готово для начала процедуры аттестации!</strong><br />
                    Убедитесь что все докумнты загружены корректно. После аттестации их нельзя будет заменить.<br />
                    </div><br /> 
                    <div class="r_msg">
                    <strong>Что дальше?</strong><br /><br />
                    1. Еще раз ознакомтесь с <a href="<?=base_url()?>psyline/rangs" target="_blank">положением о статусе псилайнера</a> для того чтобы объективно оценить свой возможный статус.<br />
                    2. Укажите и обоснуйте статус на который вы претендуете.<br />
                    3. Нажмите на кнопку "Отправить на аттестацию".<br /> 
                    </div><br /> 
                    <div class="g_msg">
                    <form method="post" action="<?=base_url()?>user/send_profile">
                    <strong>Желаемый статус</strong>:&nbsp;&nbsp;&nbsp;
                        <?php
                        $query = $this->db->get_where('groups',array('status'=>1,'id >'=>3,'id <'=>8));
                        $mods = $query->result();
                        foreach($mods as $mod):
                        ?>
                        <input <?php if($mod->id == $info->wish_rang){echo 'checked="checked"';}?> type="radio" name="status" value="<?=$mod->id?>"/> <?=$mod->name?>&nbsp;&nbsp;&nbsp;
                        <?php endforeach; ?>
                    <br /><br />
                    Обоснование статуса:<br />
                    <script type="text/javascript">
                	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
                    </script>
                    <textarea style="width: 967px; height: 70px; background-color: white !important;" name="info"></textarea>
                    
                    <p style="text-align: right;"><input type="submit" class="orang_b" value="Отправить на аттестацию" /></p>
                    </form>
                    </div>
    <br /><br />
                    
                    <div style="width: 470px; float: right;" class="g_msg">
                    <h4>Форма для дополнительных документов</h4>
                    <span><strong>По желанию</strong> загрузите сканы дополнительных дипломов и сертификатов.</span>
                    <br /><br />
                    <?$this->load->view('user/profile/inc/upload_docs2');?>
                    </div>
                    
                    <div style="width: 470px;" class="g_msg">
                    <h4>Форма для документов, обязательных к загрузке</h4>
                    <span>Подготовьте и загрузите сканы паспорта, основного диплома и подписанного договора</span>
                    <br /><br />
                    <?$this->load->view('user/profile/inc/upload_docs');?>
                    </div>
                    
                    <table style="width: 990px;">
                        <tr>
                        <td style="width: 400px; vertical-align: top;">
                        <h2><?=$info->name?></h2><br />
                            <?php
                            if($info->foto == ''){
                            ?> 
                            <img style="border: 1px solid silver;" src="<?=base_url()?>images/nofoto.jpg"/> <br />
                            <?php
                            $atts = array(
                                'width'       => 800,
                                'height'      => 400,
                                'scrollbars'  => 'yes',
                                'status'      => 'no',
                                'resizable'   => 'no',
                                'screenx'     => 0,
                                'screeny'     => 0,
                                'window_name' => '_self'
                            );
                            
                            echo anchor_popup('user/upload/foto', 'Загрузить фото', $atts, 'upl_url w184');
                            }
                            else
                            {
                            ?>
                            <img src="<?=base_url()?>users_img/<?=$info->foto;?>"/> <br />
                            <?php
                                if($info->status == 2 OR $info->status == 3)
                                {
                                $atts = array(
                                    'width'       => 800,
                                    'height'      => 400,
                                    'scrollbars'  => 'yes',
                                    'status'      => 'no',
                                    'resizable'   => 'no',
                                    'screenx'     => 0,
                                    'screeny'     => 0,
                                    'window_name' => '_self'
                                );
                                
                                echo anchor_popup('user/upload/foto', 'Измнить фото профиля', $atts, 'upl_url w184');
                                }
                            }
                            ?>
                        
                        <br /><br />
                            Образование: <strong><?echo $this->user_model->user_obrazovanie($info->obrz)?></strong><br /><br />
                            Статус: <strong><?=$this->user_model->user_status($info->rang)?></strong><br /><br />
                            Желаемый статус: <strong><?=$this->user_model->user_status($info->wish_rang)?></strong>
                            
                           
                        </td>
                        
                            
                        <td style="vertical-align: top;">
                        
                      
                    <h2>Информационные блоки</h2>
                    <br />
                        
                        
                        <?php $nnn = rand(0,9);?>
                        <div class="hide_block_wrap">
                        <div class="hide_block_head" par="<?=$nnn?>" act="sd">Основная информация<div>Показать &#9660;</div></div>
                        <div class="hide_block hide_block<?=$nnn?>">
                        <br />
                            Имя: <strong><?=$info->name?></strong><br />
                            Дата рождения: <strong><?=$this->data->format_data($info->dr)?></strong> 
                            (полных лет:<strong> <?=$this->data->f_y($info->dr)?></strong>)<br />
                            Страна: <strong><?=$info->strana?></strong><br />
                            Регион: <strong><?=$info->region?></strong><br />
                            Нас. Пункт: <strong><?=$info->np?></strong>    
                        </div>
                        </div>
                        <br />
                        
                        <?php $nnn = rand(10,19);?>
                        <div class="hide_block_wrap">
                        <div class="hide_block_head" par="<?=$info->id?>" act="sd">Образование<div>Показать &#9660;</div></div>
                        <div class="hide_block hide_block<?=$info->id?>">
                            <br />
                            Высшее образование: <strong><?echo $this->user_model->user_obrazovanie($info->obrz)?></strong><br />
                            Вуз: <strong><?=$info->vuz?></strong> Год окончания: <strong><?=date('Y',$info->god)?></strong><br /><br />
                            <strong>Дополнительное образование:</strong><br />
                            <?=nl2br($info->dop_obrz)?>
                        </div>
                        </div>
                        <br />
                        
                        <?php $nnn = rand(20,29);?>
                        <div class="hide_block_wrap">
                        <div class="hide_block_head" par="<?=$nnn?>" act="sd">Профессиональная информация<div>Показать &#9660;</div></div>
                        <div class="hide_block hide_block<?=$nnn?>">
                        <br />
                        
                        <strong>Членство в организациях:</strong><br />
                        <?=nl2br($info->orgs)?><br /><br />   
                        
                        Начало психологической деятельности: <strong><?=$this->data->format_data_my($info->psy_from)?></strong> (полных лет:<strong> <?=$this->data->f_y($info->psy_from)?></strong>)<br />
                        Психотерапевтическая модальность:  <strong>
                        <?php
                        echo $this->user_model->user_psymod($info->psymod);
                        ?>
                        </strong><br /><br />
                            
                        Формы работы: <br /><strong>
                        <?php
                        $forms = explode('*/*',$info->psy_form);
                        foreach($forms as $f):
                        echo '- '.$f.'<br />';
                        endforeach;
                        ?>
                        </strong><br /><br />    
                        
                        Возрастные группы: <br /><strong>
                        <?php
                        $forms = explode('*/*',$info->psy_vzr);
                        foreach($forms as $f):
                        echo '- '.$f.'<br />';
                        endforeach;
                        ?>
                        </strong><br /><br />
                            
                        Типы проблемм: <br /><strong>
                        <?php
                        $forms = explode('*/*',$info->psy_tip);
                        foreach($forms as $f):
                        echo '- '.$f.'<br />';
                        endforeach;
                        ?>
                        </strong><br />   
                         
                        </div>
                        </div>
                        <br />
                        
                        <?php $nnn = rand(30,39);?>
                        <div class="hide_block_wrap">
                        <div class="hide_block_head" par="<?=$nnn?>" act="sd">Контактная информация<div>Показать &#9660;</div></div>
                        <div class="hide_block hide_block<?=$nnn?>">
                            <br />
                            
                            E-Mail: <strong><?=$info->email?></strong><br />
                            Телефон: <strong><?=$info->telefon?></strong><br />
                            
                        </div>
                        </div>
                        <br /><br />
                        </td>
                        </tr>
                    </table>
                    </td>
                   
                   </tr>
            </table>          
        
        </div>
      <div id="tabs-2">
      <h2>Сообщения</h2>
      <?php $this->load->view('user/profile/inc/msgs');?>
      </div>
      
    </div>       
                 
</div>
<div style="clear: both;"></div>