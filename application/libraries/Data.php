<?php if (!defined('BASEPATH')) exit('Нет доступа к скрипту'); 

class Data {

    function create_data($create)
    {
        $now = time();//Сейчас
        //этот день начился
        $date_time_array = getdate($now);
        
        // используйте mktime для обновления UNIX времени
        $timestamp = mktime(
            $date_time_array['hours'],
            $date_time_array['minutes'],
            $date_time_array['seconds'],
            $date_time_array['mon'],
            $date_time_array['mday'],
            $date_time_array['year']
            );
        $in_day = 24*60*60;
        $from_today_start = ($date_time_array['hours']*60*60)+($date_time_array['hours']*60)+$date_time_array['seconds'];
        
        if(time()-$create <= $from_today_start)
        {
            echo 'Сегодня в '.date('H:m',$create);
        }
        elseif(time()-$create >= $from_today_start AND time()-$create < $from_today_start+$in_day)
        {
            echo 'Вчера в '.date('H:m',$create);
        }
        else
        {
            echo date('d.m.Y H:m',$create);
        }
    }
    
    function y_ago($data){
        
    }
    
    function f_y($data){
        $fy = round((time()-$data)/(365.25*24*60*60),0,PHP_ROUND_HALF_DOWN);
        echo $fy;
    }
    
    function format_data($data){
        $ms = array();
        $ms[1] = 'Января';
        $ms[2] = 'Февраля';
        $ms[3] = 'Марта';
        $ms[4] = 'Апреля';
        $ms[5] = 'Мая';
        $ms[6] = 'Июня';
        $ms[7] = 'Июля';
        $ms[8] = 'Августа';
        $ms[9] = 'Сентября';
        $ms[10] = 'Октября';
        $ms[11] = 'Ноября';
        $ms[12] = 'Декабря';
        
        $date_time_array = getdate($data);
        echo date('d',$data).' '.$ms[$date_time_array['mon']].' '.date('Y',$data).' г.';
        
        
    }
    
    function format_data_my($data){
        $ms = array();
        $ms[1] = 'Январь';
        $ms[2] = 'Февраль';
        $ms[3] = 'Март';
        $ms[4] = 'Апрель';
        $ms[5] = 'Май';
        $ms[6] = 'Июнь';
        $ms[7] = 'Июль';
        $ms[8] = 'Август';
        $ms[9] = 'Сентябрь';
        $ms[10] = 'Октябрь';
        $ms[11] = 'Ноябрь';
        $ms[12] = 'Декабрь';
        
        $date_time_array = getdate($data);
        echo $ms[$date_time_array['mon']].' '.date('Y',$data).' г.';
        
        
    }
}

?>