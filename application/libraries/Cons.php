<?php if (!defined('BASEPATH')) exit('Нет доступа к скрипту'); 



class Cons {

    function cons_block($pid, $rang)
    {
    $CI =& get_instance();
    
    $CI->load->model('user_model');
    $params = $CI->user_model->psyliner_params_by_psyliner_id($pid);
    
    if($rang <=6)//Параметры нестатусных псилайнеров
    {
        if($params->person == 1)//Личная встреча
        {
            $per = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/customers.png"/></td>
                <td class="no_img">Личная встреча в <strong>г.'.$params->person_city.' '.number_format($params->person_price, 2, '.', ' ').' &#8381;</strong>.
                <div id="psyliner'.$pid.'_phone"></div>
                </td>
                <td class="img2_td" style="width: 145px;">
                <a class="btn_with_img view_phone" pid="'.$pid.'" uid="'.$CI->session->userdata('user_id').'" href="#">
                <img style="height: 16px; top:14px;" title="Показать контакты" src="'.base_url().'icos/phone.png"/>
                Показать контакты</a>
                </td>
            </tr>
            </table>
            </div>
            ';
            
        }
        else
        {
            $per = '';
            
        }
        if($params->video == 1)//Видео
        {
            
            $v = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/video.png"/></td>
                <td class="nо_img"> Online Видео-консультация на 
                сайте Psy-Line.org: <strong>'.$CI->user_model->price('video_price',$rang).'.00 &#8381;
                </strong> за 50 мин</td>
                <td class="img2_td">
                <a href="'.base_url().'consultation/order/v/'.$pid.'/'.md5('!'.$pid.'v').'">
                <img title="Резервирование консультации" src="'.base_url().'icos/table.png"/>
                </a>
                </td>
            </tr>
            </table>
            </div>
            '; 
        }
        else
        {
            $v = '';
            
        }
        if($params->audio == 1)//Аудио
        {
            $a = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/audio.png"/></td>
                <td class="nо_img"> Online Аудио-консультация на 
                сайте Psy-Line.org: <strong>'.$CI->user_model->price('audio_price',$item->rang).'.00 &#8381;</strong> за 30 мин</td>
                <td class="img2_td">
                <a href="'.base_url().'consultation/order/a/'.$pid.'/'.md5('!'.$pid.'a').'">
                <img title="Резервирование консультации" src="'.base_url().'icos/table.png"/>
                </a>
                </td>
            </tr>
            </table>
            </div>
            '; 
            
        }
        else
        {
            $a = '';
            
        }
        if($params->text == 1)//Чат
        {
            $t = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/chat.png"/></td>
                <td class="nо_img"> Приватный чат на 
                сайте Psy-Line.org: <strong>'.$CI->user_model->price('text_price',$rang).'.00 &#8381;</strong> за 60 мин.</td>
                <td class="img2_td">
                <a href="'.base_url().'consultation/order/t/'.$pid.'/'.md5('!'.$pid.'t').'">
                <img title="Резервирование консультации" src="'.base_url().'icos/table.png"/>
                </a>
                </td>
            </tr>
            </table>
            </div>
            '; 
            
        }
        else
        {
            $t = '';
            
        }
    }
    if($rang >= 7) //Ранговитый псилайнер
    {
        if($params->person == 1)//Личная встреча
        { 
            $per = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/customers.png"/></td>
                <td class="no_img">Личная встреча в <strong>г.'.$params->person_city.' '.number_format($params->person_price, 2, '.', ' ').' &#8381;</strong>.
                <div id="psyliner'.$pid.'_phone"></div>
                </td>
                <td class="img2_td" style="width: 145px;">
                <a class="btn_with_img view_phone" pid="'.$pid.'" uid="'.$CI->session->userdata('user_id').'" href="#">
                <img style="height: 16px; top:14px;" title="Показать контакты" src="'.base_url().'icos/phone.png"/>
                Показать контакты</a>
                </td>
            </tr>
            </table>
            </div>
            ';
            
        }
        else
        {
            $per = '';
            
        }
        if($params->video == 1)//Видео
        {
            
            $v = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/video.png"/></td>
                <td class="nо_img"> Online Видео-консультация на 
                сайте Psy-Line.org: <strong>'.number_format($params->video_price, 2, '.', ' ').' &#8381;
                </strong> за 50 мин</td>
                <td class="img2_td">
                <a href="'.base_url().'consultation/order/v/'.$pid.'/'.md5('!'.$pid.'v').'">
                <img title="Резервирование консультации" src="'.base_url().'icos/table.png"/>
                </a>
                </td>
            </tr>
            </table>
            </div>
            
            '; 
        }
        else
        {
            $v = '';
            
        }
        if($params->audio == 1)//Аудио
        {
            $a = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/audio.png"/></td>
                <td class="nо_img"> Online Аудио-консультация на 
                сайте Psy-Line.org: <strong>'.number_format($params->audio_price, 2, '.', ' ').' &#8381;</strong> за 30 мин</td>
                <td class="img2_td">
                <a href="'.base_url().'consultation/order/a/'.$pid.'/'.md5('!'.$pid.'a').'">
                <img title="Резервирование консультации" src="'.base_url().'icos/table.png"/>
                </a>
                </td>
            </tr>
            </table>
            </div>
            
            '; 
            
        }
        else
        {
            $a = '';
            
        }
        if($params->text == 1)//Чат
        {
            $t = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/chat.png"/></td>
                <td class="nо_img"> Приватный чат на 
                сайте Psy-Line.org: <strong>'.number_format($params->text_price, 2, '.', ' ').' &#8381;</strong> за 60 мин.</td>
                <td class="img2_td">
                <a href="'.base_url().'consultation/order/t/'.$pid.'/'.md5('!'.$pid.'t').'">
                <img title="Резервирование консультации" src="'.base_url().'icos/table.png"/>
                </a>
                </td>
            </tr>
            </table>
            </div>
            
            '; 
            
        }
        else
        {
            $t = '';
            
        }
    }
    
    $res = array(
    'per'=>$per,
    'v'=>$v,
    'a'=>$a,
    't'=>$t
    );
    
    return $res;
    
    }
}

?>