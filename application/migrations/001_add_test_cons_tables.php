<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_test_cons_tables extends CI_Migration {

    public function up()
    {
        //вставляем тестовые данные
        //1 на 1 видео
        $data = [
            [
                'type' => 3, //видео
                'from_time' => time(),
                'to_time' => time() + 3600,
                'price' => 1500
            ]
        ];
        $this->db->insert_batch('cons_calendar', $data);
        $data = [
            [
                'cons_id' => 1
            ]
        ];
        $this->db->insert_batch('cons_sessions', $data);
        $data = [
            [
                'cons_id' => 1,
                'user_id' => 4,
                'role_id' => 1 //псилайнер
            ],
            [
                'cons_id' => 1,
                'user_id' => 14,
                'role_id' => 2 //клиент
            ],
        ];
        $this->db->insert_batch('cons_users', $data);

        //1 на 1 аудио
        $data = [
            [
                'type' => 2, //аудио
                'from_time' => time(),
                'to_time' => time() + 3600,
                'price' => 1000
            ]
        ];
        $this->db->insert_batch('cons_calendar', $data);
        $data = [
            [
                'cons_id' => 2
            ]
        ];
        $this->db->insert_batch('cons_sessions', $data);
        $data = [
            [
                'cons_id' => 2,
                'user_id' => 4,
                'role_id' => 1 //псилайнер
            ],
            [
                'cons_id' => 2,
                'user_id' => 14,
                'role_id' => 2 //клиент
            ],
        ];
        $this->db->insert_batch('cons_users', $data);

        //вебинар
        $data = [
            [
                'type' => 4, //видео
                'from_time' => time(),
                'to_time' => time() + 3600,
                'price' => 2000
            ]
        ];
        $this->db->insert_batch('cons_calendar', $data);
        $data = [
            [
                'cons_id' => 3
            ]
        ];
        $this->db->insert_batch('cons_sessions', $data);
        $data = [
            [
                'cons_id' => 3,
                'user_id' => 4,
                'role_id' => 3 //ведущий
            ],
            [
                'cons_id' => 3,
                'user_id' => 14,
                'role_id' => 4 //слушатель
            ],
            [
                'cons_id' => 3,
                'user_id' => 15,
                'role_id' => 4 //слушатель
            ],
        ];
        $this->db->insert_batch('cons_users', $data);

        //конференция(обсуждение)
        $data = [
            [
                'type' => 5, //видео
                'from_time' => time(),
                'to_time' => time() + 3600,
                'price' => 2000
            ]
        ];
        $this->db->insert_batch('cons_calendar', $data);
        $data = [
            [
                'cons_id' => 4
            ]
        ];
        $this->db->insert_batch('cons_sessions', $data);
        $data = [
            [
                'cons_id' => 4,
                'user_id' => 4,
                'role_id' => 3 //ведущий
            ],
            [
                'cons_id' => 4,
                'user_id' => 14,
                'role_id' => 5 //участник
            ],
            [
                'cons_id' => 4,
                'user_id' => 15,
                'role_id' => 5 //участник
            ],
        ];
        $this->db->insert_batch('cons_users', $data);

    }

    public function down()
    {
        $this->db->truncate("cons_users");
        $this->db->truncate("cons_sessions");
        $this->db->truncate("cons_calendar");
    }
}