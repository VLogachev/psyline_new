<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    function check_user()
    {
        if($this->session->userdata('loged_in')==1 and $this->session->userdata('user_code')==md5('boops'.$this->session->userdata('user_id')))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function is_psyliner($user_id){
        $query = $this->db->get_where('users',array('id'=>$user_id));
        $mod = $query->row();
        
        if($mod->psyliner_id == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public function is_admin($user_id){
        $query = $this->db->get_where('users_groups',array('user_id'=>$user_id,'group_id'=>1));
        $n = $query->num_rows();
        
        if($n == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public function psyliner_id($id)
    {
        $query = $this->db->get_where('users',array('id'=>$id));
        $mod = $query->row();
        return $mod->psyliner_id;
    }
    
    
    
    public function user_name_by_id($user_id)
    {
        if($user_id == 0)
        {
            $item = "Администрация";
        }
        else
        {
            $query = $this->db->get_where('users',array('id'=>$user_id));
            $array = $query->row();
            $item = $array->username;
            
        }
        return $item;
    }
    
    public function psyliner_info($user_id)
    {
        $this->db->select('psyliner_id');
        $this->db->where('id',$user_id);
        $query = $this->db->get('users');
        $user = $query->row();
        
        $query = $this->db->get_where('psyliners',array('id'=>$user->psyliner_id));
        $info = $query->row();
        
        return $info;
    }
    
    public function psyliner_params($user_id)
    {
        $this->db->select('psyliner_id');
        $this->db->where('id',$user_id);
        $query = $this->db->get('users');
        $user = $query->row();
        
        $query = $this->db->get_where('psyliners_params',array('psyliner_id'=>$user->psyliner_id));
        $info = $query->row();
        
        return $info;
    }
    
    public function psyliner_params_by_psyliner_id($psyliner_id)
    {
        
        $query = $this->db->get_where('psyliners_params',array('psyliner_id'=>$psyliner_id));
        $info = $query->row();
        
        return $info;
    }
    
    public function user_data($user_id)
    {
        $query = $this->db->get_where('users',array('id'=>$user_id));
        $info = $query->row();//Информация о пользователе
        
        return $info;
    }
    
    
    public function user_info($psyliner_id)
    {
        $query = $this->db->get_where('psyliners',array('id'=>$psyliner_id));
        $info = $query->row();//Информация о псилайнере
        
        $query = $this->db->get_where('users',array('psyliner_id'=>$info->id));
        $info = $query->row();
        
        return $info;
    }
    
    public function user_id_by_psyliner_id($psyliner_id)
    {
        $this->db->select('id');
        $query = $this->db->get_where('users',array('psyliner_id'=>$psyliner_id));
        $info = $query->row();
        
        return $info->id;
    }
    
    public function user_psymod($id)
    {
        if($id==0)
        {
            return 'Другое';
        }
        else
        {
        $query = $this->db->get_where('psy_mod',array('status'=>1,'id'=>$id));
        $mod = $query->row();
        return $mod->name;
        }
    }
    
    public function user_status($id)
    {
        $query = $this->db->get_where('groups',array('id'=>$id));
        $mod = $query->row();
        if($query->num_rows() == 0)
        {
            return 'Кандидат';
        }
        else
        {
            return $mod->name;
        }
        
    }
    
    public function user_obrazovanie($id)
    {
        $query = $this->db->get_where('obrazovanie',array('id'=>$id));
        $mod = $query->row();
        return $mod->name;
    }
    
    public function del_foto($id){
        $this->db->select('foto');
        $sql = $this->db->get_where('users',array('id'=>$id));
        $foto = $sql->row();
        
        $img_name = $_SERVER['DOCUMENT_ROOT'].'/toppsy/users_img/'.$foto->foto;
        unlink($img_name);
    }
    
    public function del_psyliner_foto($img,$folder,$user_id)
    {
        if($img == 'foto')
        {
            $tbl_row = 'foto';
        }
        else
        {
            $tbl_row = 'foto_'.$img;
        }
        
        $pl_info = $this->psyliner_info($user_id);
        $u_info = $this->user_info($pl_info->id);
        
        $this->db->where(array('id'=>$u_info->psyliner_id));
        $this->db->select($tbl_row);
        $sql = $this->db->get('psyliners');
        
        $result = $sql->row();//строка псилайнера
        if($result->$tbl_row != '')
        {
            $this->db->where(array('id'=>$pl_info->id));
            $data = array($tbl_row=>'');
            $this->db->update('psyliners',$data);
            
            $img_name = $_SERVER['DOCUMENT_ROOT'].'/toppsy/'.$folder.$result->$tbl_row;
            unlink($img_name);
            
        }
    }
    
    public function psyliner_rang($psyliner_rang){
        
        $this->db->where('id',$psyliner_rang);
        $this->db->select('rang');
        $sql = $this->db->get('psyliners');
        $rang_array = $sql->row();
        $rang = $rang_array->rang;
        return $rang;
    }
    
    public function psyliner_foto($psyliner_id){
        
        $this->db->where('id',$psyliner_id);
        $this->db->select('foto');
        $sql = $this->db->get('psyliners');
        $foto_array = $sql->row();
        $foto = $foto_array->foto;
        return $foto;
    }
    
    public function psyliner_foto_by_user_id($user_id){
        
        $query = $this->db->get_where('users',array('id'=>$user_id));
        $mod = $query->row();
        $psyliner_id = $mod->psyliner_id;
        
        $this->db->where('id',$psyliner_id);
        $this->db->select('foto');
        $sql = $this->db->get('psyliners');
        $foto_array = $sql->row();
        $foto = $foto_array->foto;
        return $foto;
    }
    
    public function psyliner_prices($psyliner_rang){
        
        $this->db->where('id',$psyliner_rang);
        $this->db->select('video_price, audio_price, text_price');
        $sql = $this->db->get('groups');
        $prc = $sql->row();
        
        return $prc;
    }
    public function price($price,$rang){
        $this->db->where('id',$rang);
        $this->db->select($price);
        $sql = $this->db->get('groups');
        $res = $sql->row();
        $prc = number_format($res->$price, 0, '. ', '`');
        return $prc;
    }
}