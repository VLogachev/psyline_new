<?php

class Consultation_model extends CI_Model
{
    //за 15 минут до начала консультации ее инициализация становится доступной
    const MINUTES_BEFORE_INITIALIZE = 15;
    //после окончания консультации ее инициализация доступна еще 5 минут
    const MINUTES_AFTER_INITIALIZE = 5;

    /**
     * Возращает все консультации для определенного клиента
     *
     * @param int $customer_id id клиента
     * @return mixed
     */
    public function get_consultations_by_customer_id($customer_id){

        //находим все консультации для определенного клиента из таблицы cons_users
        $this->db->select('cons_id');
        $this->db->from('cons_users');
        $this->db->where(['user_id' => $customer_id]);
        $cons_id_arrays = $this->db->get()->result_array();

        $cons_ids = [];
        foreach($cons_id_arrays as $cons_id_array){
            $cons_ids[] = $cons_id_array['cons_id'];
        };

        $this->db->select('cc.id as id, u.username as username, cc.from_time as from_time, cc.to_time as to_time,
            cc.type as type_id, ct.type_name as type_name');
        $this->db->from('cons_calendar cc');
        $this->db->join('cons_types ct', 'cc.type = ct.id');
        $this->db->join('cons_users cu', 'cc.id = cu.cons_id');
        $this->db->join('users u', 'cu.user_id = u.id');
        if(empty($cons_ids)){
            $this->db->where(['cu.user_id' => $customer_id]);
        } else {
            $this->db->where_in('cu.cons_id', $cons_ids);
            $this->db->where(['cu.role_id' => 1]);//псилайнер
            $this->db->or_where(['cu.role_id' => 3]);//ведущий
        }
        return $this->db->get()->result();

    }

    /**
     * Возвращает все консультации для определенного псилайнер
     *
     * @param $psyliner_id
     * @return mixed
     */
    public function get_consultations_by_psyliner_id($psyliner_id){

        //находим все консультации для определенного псилайнера из таблицы cons_users
        $this->db->select('cons_id');
        $this->db->from('cons_users');
        $this->db->where(['user_id' => $psyliner_id]);
        $cons_id_arrays = $this->db->get()->result_array();

        $cons_ids = [];
        foreach($cons_id_arrays as $cons_id_array){
            $cons_ids[] = $cons_id_array['cons_id'];
        };

        $this->db->select("cc.id as id, IF(role_id = 4 or role_id = 5, NULL, u.username) as username, cc.from_time as from_time, cc.to_time as to_time,
            cc.type as type_id, ct.type_name as type_name", FALSE);
        $this->db->from('cons_calendar cc');
        $this->db->join('cons_types ct', 'cc.type = ct.id');
        $this->db->join('cons_users cu', 'cc.id = cu.cons_id');
        $this->db->join('users u', 'cu.user_id = u.id');
        if(empty($cons_ids)){
            $this->db->where(['cu.user_id' => $psyliner_id]);
        } else {
            $this->db->where_in('cu.cons_id', $cons_ids);
            $this->db->where(['cu.role_id' => 2]); //клиент
            $this->db->or_where(['cu.role_id' => 4]); //слушатель
            $this->db->or_where(['cu.role_id' => 5]); //участник
            $this->db->group_by('cc.id');
        }
        return $this->db->get()->result();

    }

    /**
     * Склеивает все аудио и видео фрагменты сессий в 1 файл или для определенной консультации
     *
     * @param string $videosFolder папка со всеми видео консультаций
     * @param string $consultationID
     */
    public function merge_media($videosFolder, $consultationID = NULL){

        //выбираем уже законченную консультацию
        $this->db->where("to_time <", time());
        if($consultationID){
            $this->db->where("id", $consultationID);
        }
        $consultationsEnded = $this->db->get("cons_calendar")->result();
        foreach($consultationsEnded as $consEnded){

            //если для данной конференции уже склеены медиа файлы
            $pathsToMedia = $this->db->get_where("cons_sessions", ['cons_id' => $consEnded->id])->row()->files;
            if(!empty($pathsToMedia)) continue;

            //если консультация была запланирована, но не проведена
            if(!file_exists($videosFolder . "/conference-{$consEnded->id}")) continue;

            //перебираем папки всех пользователей конференции
            $userFolders = scandir($videosFolder . "/conference-{$consEnded->id}");
            //для каждого пользователя, который принимал участие в конслультации
            foreach($userFolders as $userFolder){

                if($userFolder == "." || $userFolder == "..") continue;

                $contentFolder = $videosFolder . "/conference-{$consEnded->id}/$userFolder";

                //склеиваем видео фрагменты
                $videoFragments = glob("$contentFolder/*.webm");
                usort($videoFragments, "strnatcmp");
                $this->create_ffmpeg_input_file(
                    $videoFragments,
                    $contentFolder,
                    "input_video.txt"
                );
                exec("ffmpeg -f concat -i $contentFolder/input_video.txt -codec copy $contentFolder/output_video.webm");

                //склеиваем аудио фрагменты
                $audioFragments = glob("$contentFolder/*.wav");
                usort($audioFragments, "strnatcmp");
                $this->create_ffmpeg_input_file(
                    $audioFragments,
                    $contentFolder,
                    "input_audio.txt"
                );
                exec("ffmpeg -f concat -i $contentFolder/input_audio.txt -codec copy $contentFolder/output_audio.wav");

                //склеиваем видео и аудио дорожки
                exec("ffmpeg -i $contentFolder/output_video.webm -i $contentFolder/output_audio.wav -map 0:0 -map 1:0 $contentFolder/output.webm");

                //удаляем все файлы, кроме output.webm
                $filesInFolder = scandir($contentFolder);
                foreach($filesInFolder as $fileInFolder){
                    if($fileInFolder == "." || $fileInFolder == ".." || $fileInFolder == "output.webm"){
                        continue;
                    } else {
                        unlink($contentFolder."/".$fileInFolder);
                    }
                }

                //записываем в БД путь к получившийся файл
                $pathsToMedia = $this->db->get_where("cons_sessions", ['cons_id' => $consEnded->id])->row()->files;
                $data = [
                    'files' => empty($pathsToMedia) ? $contentFolder . "/output.webm" : $pathsToMedia . "," . $contentFolder . "/output.webm",
                ];
                $this->db->where('cons_id', $consEnded->id);
                $this->db->update('cons_sessions', $data);


            }
        }

    }

    /**
     * Создаем текстовый с путями ко всем медиафрагментам. Нужен для ffmpeg.
     *
     * @param $files
     * @param $saveFolder
     * @param $outputFileName
     */
    public function create_ffmpeg_input_file($files,$saveFolder,$outputFileName){
        $i = 0;
        foreach($files as $file){
            $data = "file $file\n";
            if($i == 0){
                file_put_contents($saveFolder."/".$outputFileName, $data);
            } else {
                file_put_contents($saveFolder."/".$outputFileName, $data, FILE_APPEND);
            }
            $i++;
        }
    }

    /**
     * Возвращает "disabled", если время инициализации сессии не в рамках переменных MINUTES_BEFORE_INITIALIZE
     * и MINUTES_AFTER_INITIALIZE
     *
     * @param $fromTime
     * @param $toTime
     * @return string
     */
    public function disableInitButton($fromTime, $toTime){
        //если текущее время в нужном диапазоне
        $now = time();
        return ( ($now > ($fromTime - self::MINUTES_BEFORE_INITIALIZE * 60)) && ($now < ($toTime + self::MINUTES_AFTER_INITIALIZE * 60)) ) ? "" : "disabled";
    }

}