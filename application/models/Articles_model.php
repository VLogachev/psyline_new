<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles_model extends CI_Model
{
    function get_articles($num, $offset)
        {
            $this->db->where(array('status'=>1));
            $this->db->order_by('id','desc');
            $query = $this->db->get('articles',$num, $offset);
           
            return $query->result();
        }
    
    function get_user_articles_all($user_id)
        {
            $this->db->where(array('user_id'=>$user_id));
            $this->db->order_by('id','desc');
            $query = $this->db->get('articles');
            
            return $query->result();
        }
    
    function get_user_articles($user_id)
        {
            $this->db->where(array('user_id'=>$user_id,'status'=>1));
            $this->db->order_by('id','desc');
            $query = $this->db->get('articles');
           
            return $query->result();
        }
        
    function num_user_articles_all($user_id)
        {
            $this->db->where(array('user_id'=>$user_id));
            $this->db->order_by('id','desc');
            $query = $this->db->get('articles');
            
            return $query->num_rows();
        }
    
    function num_user_articles($user_id)
        {
            $this->db->where(array('user_id'=>$user_id,'status'=>1));
            $this->db->order_by('id','desc');
            $query = $this->db->get('articles');
           
            return $query->num_rows();
        }
    
    function get_articles_category($num, $offset, $category)
        {
            $this->db->where(array('category_id'=>$category,'status'=>1));
            $this->db->order_by('id','desc');
            $query = $this->db->get('articles',$num, $offset);
            
            return $query->result();
        }
    function category_name($category)
        {
            $this->db->where(array('id'=>$category));
            $query = $this->db->get('articles_category');
            $res = $query->row();
            
            return $res->name;
        }
    function user_categorys()
    {
            $query = $this->db->get_where('articles_category',array('parent_id'=>0,'status'=>1,'spec'=>0));
            $categorys = $query->result();
            return $categorys;
    }
    function spec_category($category_id)
    {
            $query = $this->db->get_where('articles_category',array('id'=>$category_id,'spec'=>1));
            $r = $query->num_rows();
            if($r == 0){return false;}
            else{return true;}
    }
}