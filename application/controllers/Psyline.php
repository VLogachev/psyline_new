<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Psyline extends CI_Controller {
    
	public function whatis()
	{
        $sql = $this->db->get_where('articles',array('id'=>1));
        $item = $sql->row();
        
        $this->db->where('id',1);
        $this->db->set('hits',$item->hits + 1);
        $this->db->update('articles');
        
        
        $data = array();
        $data['title'] = "Что такое Псилайн (PSYLINE)";
        $data['item'] = $item;
 
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        $this->load->view('psyline/whatis');
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
    
    public function rangs()
	{
        $sql = $this->db->get_where('articles',array('id'=>2));
        $item = $sql->row();
        
        $this->db->where('id',2);
        $this->db->set('hits',$item->hits + 1);
        $this->db->update('articles');
        
        
        $data = array();
        $data['title'] = $item->title;
        $data['item'] = $item;
 
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        $this->load->view('psyline/whatis');
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
}