<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	public function _user_name($id)
    {
        $this->db->select('first_name, last_name');
        $this->db->from('users');
        $this->db->where('id',$id);
        $query = $this->db->get();
        
        $row = $query->row();
        $name = $row->first_name.' '.$row->last_name;
        return $name;
    }
    
	public function index()
	{
        $data = array();
        $data['reg_user'] = 0;
        
        $data['title'] = 'Регистрация пользователя';
        
        
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        
        $this->load->view('user/reg1');
        
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
    
    public function psyliner($display_form = 'none')
	{
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $data = array();
        $data['display_form'] = $display_form;
        $data['title'] = 'Регистрация псилайнера';
        
        
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        
        $this->load->view('user/reg-psyliner');
        
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
    
    
    public function user()
	{
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $data = array();
        $data['title'] = 'Регистрация пользователя';
        
        
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        
        $this->load->view('user/reg-user');
        
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
    
    
    public function reg_psyliner($display_form = 'none'){
    

    // Во-первых, удалите старую капчу
    $expiration = time()-7200; // Двухчасовое ограничение
    $this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);	
    
    // Потом проверим существование капчи:
    $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
    $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
    $query = $this->db->query($sql, $binds);
    $row = $query->row();
    
    if ($row->count == 0) //Ошибка каптчи
    {
        $ses = array(
        'ses_msg' => 1,
        'ses_msg_type' => 'er',
        'ses_msg_text' => '<strong>Произошла ошибка:</strong><br />
        Введен неверный проверочный код.'
        );
        $this->session->set_userdata($ses);
        redirect('registration/psyliner');
    }
    else
    {    
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');
        
        
        $this->form_validation->set_rules('n1', 'Фамилия', 'required');
        $this->form_validation->set_rules('n2', 'Имя', 'required');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        $this->form_validation->set_rules('dr', 'Дата рождения', 'required');
        $this->form_validation->set_rules('strana', 'Страна', 'required');
        $this->form_validation->set_rules('region', 'Регион', 'required');
        $this->form_validation->set_rules('np', 'Город/Населенный пункт', 'required');
        $this->form_validation->set_rules('vuz', 'ВУЗ', 'required');
        $this->form_validation->set_rules('god', 'Год окончания ВУЗа', 'required');
        $this->form_validation->set_rules('psy-from', 'Дата начала профильной практики', 'required');
        
        
    	if ($this->form_validation->run() == FALSE)
    	{
                $data = array();
                $data['title'] = 'Регистрация псилайнера';
                $data['display_form'] = 'block';
                
                $this->load->view('base/header',$data);
                $this->load->view('base/top');
                
                $this->load->view('right/right-b');
                $this->load->view('right/module1');
                $this->load->view('right/right-e');
                
                $this->load->view('user/reg-psyliner');
                
                $this->load->view('base/clear');
                $this->load->view('base/footer');
    	}
    	else
    	{
                //Проверка на новизну
                $sql = $this->db->get_where('psyliners', array('email' => $_POST['email']));
                $sql1 = $this->db->get_where('users', array('email' => $_POST['email']));
                
                if($sql->num_rows()==0 AND $sql1->num_rows()==0)
                {
                $dr_array = explode('.',$_POST['dr']);
                $dr = mktime(12,00,00,$dr_array[1],$dr_array[0],$dr_array[2]);
                
                $y1_array = explode('.',$_POST['psy-from']);
                $psy_from = mktime(12,00,00,$y1_array[0],1,$y1_array[1]);
                
                $god = mktime(12,00,00,6,15,$_POST['god']);
                
                $psy_form = implode('*/*',$_POST['psy-form']);
                $psy_vzr = implode('*/*',$_POST['psy-vzr']);
                $psy_tip = implode('*/*',$_POST['psy-tip']);
                
                $data = array(
                        'email' => $_POST['email'],
                        'cr_time' => time(),
                        'act_code' => md5('boops'.time()),
                        'name'=> $_POST['n1'].' '.$_POST['n2'].' '.$_POST['n3'],
                        'name1' => $_POST['n1'],
                        'name2' => $_POST['n2'],
                        'name3' => $_POST['n3'],
                        'telefon' => $_POST['tel1'].' '.$_POST['tel2'],
                        'strana'=> $_POST['strana'],
                        'region'=> $_POST['region'],
                        'np'=> $_POST['np'],
                        'obrz'=>$_POST['obrz'],
                        'vuz'=>$_POST['vuz'],
                        'god'=>$god,
                        'dop_obrz'=>$_POST['dop_obrz'],
                        'psy_from'=>$psy_from,
                        'orgs'=>$_POST['orgs'],
                        'wish_rang'=>$_POST['status'],
                        'psy_form'=>$psy_form,
                        'psy_vzr'=>$psy_vzr,
                        'psy_tip'=>$psy_tip,
                        'psymod'=>$_POST['psymod'],
                        'dr'=>$dr     
                );
                
                $this->db->insert('psyliners', $data);
                
                //ОТПРАВКА ПИСЬМА
                $this->load->library('email');
                
                $config['mailtype']='html';
                $this->email->initialize($config);
                
                $this->email->from('info@psy-line.org', 'PSY-LINE.ORG');
                $this->email->to($_POST['email']); 
                $this->email->subject('IPO|Регистрация на сайте PSY-LINE.ORG');
                
                $this->email->message('
                Добрый день!<br />
                Кто-то (возможно Вы) хочет привязать данный e-mail адрес к учетной записи на сайте PSY-LINE.ORG.<br />
                Если Вы этого не делали, то просто УДАЛИТЕ это письмо, и мы никогда не будем использовать этот адрес.<br /><br />

                Чтобы привязать данный e-mail, необходимо подтвердить Ваш почтовый адрес. Для этого перейдите по ссылке :
                <a href="'.base_url().'registration/emailconferm/'.$data['act_code'].'">'.base_url().'registration/emailconferm/'.$data['act_code'].'</a><br />
                нажмите на неё или скопируйте её в адресное окно браузера). 
                <br /><br />
                Это письмо написано роботом. Отвечать на него не надо.
                ');	
                $this->email->send();
                
                $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'ok',
                'ses_msg_text' => '<strong>Ваше заявление успешно зарегистрировано!</strong><br />
                На Ваш почтовый ящик отправлено письмо с кодом подтверждения E-Mail вашего аккаунта.'
                );
                $this->session->set_userdata($ses);
                redirect();
                
                }
                else
                {
                $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'er',
                'ses_msg_text' => '<strong>Произошла ошибка:</strong><br />
                Пользователь с таким почтовым ящиком уже зарегистрирован.'
                );
                $this->session->set_userdata($ses);
                redirect('registration/psyliner/block');    
                }
                
    	}
        
     }    
    }
    
    
    
    
    public function reg_user(){
    

    // Во-первых, удалите старую капчу
    $expiration = time()-7200; // Двухчасовое ограничение
    $this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);	
    
    // Потом проверим существование капчи:
    $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
    $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
    $query = $this->db->query($sql, $binds);
    $row = $query->row();
    
    if ($row->count == 0) //Ошибка каптчи
    {
        $ses = array(
        'ses_msg' => 1,
        'ses_msg_type' => 'er',
        'ses_msg_text' => '<strong>Произошла ошибка:</strong><br />
        Введен неверный проверочный код.'
        );
        $this->session->set_userdata($ses);
        redirect('registration/user');
    }
    else
    {    
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');
        
        
        $this->form_validation->set_rules('n1', 'Фамилия', 'required');
        $this->form_validation->set_rules('n2', 'Имя', 'required');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        $this->form_validation->set_rules('dr', 'Дата рождения', 'required');
        $this->form_validation->set_rules('strana', 'Страна', 'required');
        $this->form_validation->set_rules('region', 'Регион', 'required');
        $this->form_validation->set_rules('np', 'Город/Населенный пункт', 'required');
        
        
    	if ($this->form_validation->run() == FALSE)
    	{
                $data = array();
                $data['title'] = 'Регистрация пользователя';
                
                $this->load->view('base/header',$data);
                $this->load->view('base/top');
                
                $this->load->view('right/right-b');
                $this->load->view('right/module1');
                $this->load->view('right/right-e');
                
                $this->load->view('user/reg-user');
                
                $this->load->view('base/clear');
                $this->load->view('base/footer');
    	}
    	else
    	{
                //Проверка на новизну
                $sql = $this->db->get_where('psyliners', array('email' => $_POST['email']));
                $sql1 = $this->db->get_where('users', array('email' => $_POST['email']));
                
                if($sql->num_rows()==0 AND $sql1->num_rows()==0)
                {
                $dr_array = explode('.',$_POST['dr']);
                $dr = mktime(12,00,00,$dr_array[1],$dr_array[0],$dr_array[2]);
                
                $data = array(
                        'email' => $_POST['email'],
                        'cr_time' => time(),
                        'psyliner_id'=>0,
                        'activation_code' => md5('boops'.time()),
                        'username'=> $_POST['n1'].' '.$_POST['n2'].' '.$_POST['n3'],
                        'n1' => $_POST['n1'],
                        'n2' => $_POST['n2'],
                        'n3' => $_POST['n3'],
                        'strana'=> $_POST['strana'],
                        'region'=> $_POST['region'],
                        'np'=> $_POST['np'],
                        'dr'=>$dr ,
                        'active'=>0  
                );
                
                $this->db->insert('users', $data);
                
                $data = array(
                    'group_id' => 3,
                    'user_id' => $this->db->insert_id()
                );
                $this->db->insert('users_groups', $data);
                
                //ОТПРАВКА ПИСЬМА
                $this->load->library('email');
                
                $config['mailtype']='html';
                $this->email->initialize($config);
                
                $this->email->from('info@psy-line.org', 'PSY-LINE.ORG');
                $this->email->to($_POST['email']); 
                $this->email->subject('IPO|Регистрация на сайте PSY-LINE.ORG');
                
                $this->email->message('
                Добрый день!<br />
                Кто-то (возможно Вы) хочет привязать данный e-mail адрес к учетной записи на сайте PSY-LINE.ORG.<br />
                Если Вы этого не делали, то просто УДАЛИТЕ это письмо, и мы никогда не будем использовать этот адрес.<br /><br />

                Чтобы привязать данный e-mail, необходимо подтвердить Ваш почтовый адрес. Для этого перейдите по ссылке :
                <a href="'.base_url().'registration/emailconferm/'.$data['activation_code'].'">'.base_url().'registration/useremailconferm/'.$data['activation_code'].'</a><br />
                нажмите на неё или скопируйте её в адресную строку браузера). 
                <br /><br />
                Это письмо написано роботом. Отвечать на него не надо.
                ');	
                $this->email->send();
                
                $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'ok',
                'ses_msg_text' => '<strong>Ваша анкета успешно обработана!</strong><br />
                На Ваш почтовый ящик отправлено письмо с кодом подтверждения E-Mail вашего аккаунта.'
                );
                $this->session->set_userdata($ses);
                redirect();
                
                }
                else
                {
                $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'er',
                'ses_msg_text' => '<strong>Произошла ошибка:</strong><br />
                Пользователь с таким почтовым ящиком уже зарегистрирован.'
                );
                $this->session->set_userdata($ses);
                redirect('registration/user');    
                }      
             }
         }    
    }
    
    
    
    public function emailconferm($code)
    {
        $sql = $this->db->get_where('psyliners', array('act_code' => $code));
        
        if($sql->num_rows()==1)
        {
            $sql = $this->db->get_where('psyliners', array('act_code' => $code));
            $row = $sql->row();
            
            $data = array(
                        'status' => 1,
                        'conferm_email_time' => time(),
                        'act_code' => ''
                );
                $this->db->where('id',$row->id);    
                $this->db->update('psyliners', $data);
                
                
            //Устанавливаем сообщения и редиректим
            $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'ok',
                'ses_msg_text' => '<strong>Ваш почтовый ящик успешно подтвержден!</strong><br /><br />
                Ваша анкета поступила администрации. Результаты рассмотрения вашей анкеты будут сообщены 
                Вам по электронной почте.<br />'
                );
                $this->session->set_userdata($ses);
                redirect();
            
        }
        else
        {
           $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'er',
                'ses_msg_text' => '<strong>Произошла ошибка:</strong><br />
                Пользователя с таким почтовым ящиком не существует.'
                );
                $this->session->set_userdata($ses);
                redirect();  
        }  
    }
    
     public function useremailconferm($code)
    {
        $sql = $this->db->get_where('users', array('activation_code' => $code));
        
        if($sql->num_rows()==1)
        {
            $sql = $this->db->get_where('users', array('activation_code' => $code));
            $row = $sql->row();
            
            $this->load->helper('string');
            $pass = random_string('alnum', 8);
            
            $data = array(
                        'active' => 1,
                        'activation_code' => '',
                        'password' => md5($pass)
                );
                $this->db->where('id',$row->id);    
                $this->db->update('users', $data);
                
            
            $config['mailtype']='html';
            $this->email->initialize($config);
            
            $this->email->from('info@psy-line.org', 'PSY-LINE.ORG');
            $this->email->to($row->email); 
            $this->email->subject('IPO|Ваш пароль на сайте PSY-LINE.ORG');
            $this->email->attach('dogovor.doc');
            $this->email->message('
                
                Добро пожаловать на портал PSY-LINE.ORG, '.$row->username.'!.<br /><br />
                Ваш пароль для входа в личный кабинет: <b>'.$pass.'</b>.Вы можете изменить пароль в 
                своем личном кабинете в любое время.<br /><br />
                
                Ждем Вас на нашем портале!
                <br /><br />
                Это письмо написано роботом. Отвечать на него не надо.
                ');	
                $this->email->send();
                
            //Устанавливаем сообщения и редиректим
            $ses = array(
            'ses_msg' => 1,
            'ses_msg_type' => 'ok',
            'ses_msg_text' => '<strong>'.$row->username.', Ваш почтовый ящик успешно подтвержден!</strong><br />
            Ваша учетная запись активирована. На почтовый ящик отправлено письмо с паролем.<br />'
            );
            $this->session->set_userdata($ses);
                
            //Отправляем внутреннее сообщение
            $data=array(
            'from'=>0,
            'to'=>$row->id,
            'type'=>'admin',
            'cr_time'=>time(),
            'text'=> $row->n2.', добро пожаловать на портал PSY-LINE.ORG!<br /><br />
            Теперь Вы можете получить On-Line консультацию специалиста пси-профиля, 
            участвовать в обсуждениях на форуме, комментировать материалы и многое другое.<br /><br />
            <strong><em>P.S. Не забудьте загрузить фото вашего профиля в личном кабинете</em></strong>'
            );
            $this->db->insert('msgs',$data);
                
            redirect();
            
        }
        else
        {
           $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'er',
                'ses_msg_text' => '<strong>Произошла ошибка:</strong><br />
                Пользователя с таким почтовым ящиком не существует.'
                );
                $this->session->set_userdata($ses);
                redirect();  
        }  
    }
}