<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    
	public function _adminonline()
    {
        if($this->session->userdata('user_code')==md5('boops'.$this->session->userdata('user_id')) AND $this->session->userdata('adminonline') == 'yes')
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    
    
    public function index()
	{
		//Проверка админ прав
        if($this->session->userdata('loged_in') == 1)
        {
            $roles_str = $this->session->userdata('user_roles');
            $n_roles = substr_count($roles_str,'/');
            
            $adm = 0;
            
            if($n_roles>1) //несколько ролей
            {   
                $rols = explode('/',$roles_str);
                for($i= $n_roles-$n_roles;$i <= $n_roles-1; $i++)
                {
                    if ($rols[$i] == 1){$adm=1;}
                }
            }
            if($n_roles==1) //роль одна
            {
                if($roles_str == '1'){$adm=1;}
            }
            
            if($adm == 1 AND $this->session->userdata('user_code')==md5('boops'.$this->session->userdata('user_id')))
            {
                $ses = array('adminonline' => 'yes');
                $this->session->set_userdata($ses);
                redirect('admin/panel/');
            }
            else //какой-то косяк
            {
                redirect();
            }
            
        }
        else //незалогиненый юзер
        {
            redirect('user');
        }
	}

    public function panel()
	{
       if($this->_adminonline() == 1)
       {
            $sql=$this->db->get_where('psyliners',array('status'=>0));
            $n_k = $sql->num_rows();
            
            $data = array(
            'title'=>'Административный раздел',
            'nk'=>$n_k
            );
            
            $this->load->view('admin/header',$data);
            $this->load->view('admin/top');
            $this->load->view('admin/menu');
            $this->load->view('admin/main');
            $this->load->view('admin/footer');
       }
       else
       {
            redirect();
       }
    }
    
    public function candidats()
	{
       if($this->_adminonline() == 1)
       {
            $this->load->library('Data');
            $this->load->model('user_model');
            
            
            
            $data = array(
            'title'=>'Кандитаты в псилайнеры'
            
            );
            
            $this->load->view('admin/header',$data);
            $this->load->view('admin/top');
            $this->load->view('admin/menu');
            $this->load->view('admin/include/candidats');
            $this->load->view('admin/footer');
       }
       else
       {
            redirect();
       }
    }
    
    public function attestat()
	{
       if($this->_adminonline() == 1)
       {
            $this->load->library('Data');
            $this->load->model('user_model');
            
            
            
            $data = array(
            'title'=>'Административный раздел'
            
            );
            
            $this->load->view('admin/header',$data);
            $this->load->view('admin/top');
            $this->load->view('admin/menu');
            $this->load->view('admin/include/attestat');
            $this->load->view('admin/footer');
       }
       else
       {
            redirect();
       }
    }
    
    public function conf_cnd($id)
    {
    if($this->_adminonline() == 1)
    {
        $this->load->helper('string');
        
        $sql = $this->db->get_where('psyliners', array('id' => $id));
        
        if($sql->num_rows()==1)//есть такой
        {
            $sql = $this->db->get_where('psyliners', array('id' => $id));
            $row = $sql->row();
            
            
            //Формируем нужные переменные
            $f_name = $row->name;
            $pass = random_string('alnum', 8);
            
            //Обновляем данные в БД ПСИЛАЙНЕРОВ
            $data = array(
                        'status' => 2
                );
                $this->db->where('id',$row->id);    
                $this->db->update('psyliners', $data);
             
                //СОЗДАЕМ УЧЕТКУ  
                $data = array(
                            'username' => $f_name,
                            'password' => md5($pass),
                            'email' => $row->email,
                            'created_on' => time(),
                            'psyliner_id' => $row->id
                    );    
                $this->db->insert('users', $data);
                
                //ПРИСВАЕВАЕМ РОЛЬ
                $ins_id = $this->db->insert_id();
                $data = array(
                            'user_id' => $ins_id,
                            'group_id' => 4
                    );    
                $this->db->insert('users_groups', $data);
                
                //Отправляем письмо с паролем и договором
                $this->load->library('email');
                    
                $config['mailtype']='html';
                $this->email->initialize($config);
            
                $this->email->from('info@psy-line.org', 'PSY-LINE.ORG');
                $this->email->to($row->email); 
                $this->email->subject('IPO|Ваш пароль на сайте PSY-LINE.ORG');
                $this->email->attach('dogovor.doc');
                $this->email->message('
                Добрый день, '.$f_name.'!<br /><br />
                Ваша анкета расмотренна и предварительно одобренна администрацией сайта PSY-LINE.ORG.<br />
                Ваш пароль для входа в личный кабинет: <b>'.$pass.'</b>.Вы сможете изменить пароль в 
                своем личном кабинете после прохождения процедуры аттестации.<br /><br />
                
                Во вложении к этому письму находится форма договора, с которым Вы должны ознакомиться, распечатать, 
                заполнить и отсканировать. <br />
                Сканированная копия заполненного договора понадобиться Вам для прохождения процедуры аттестации.
                <br /><br />
                Ждем Вас на нашем портале!
                <br /><br />
                Это письмо написано роботом. Отвечать на него не надо.
                ');	
                $this->email->send();
                
                $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'ok',
                'ses_msg_text' => '<strong>Кандидат одобрен.</strong><br /><br />
                Письмо с паролем и договором отправлено пользователю.'
                );
                $this->session->set_userdata($ses);
                
                //Отправляем внутреннее сообщение
                $data=array(
                'from'=>0,
                'to'=>$ins_id,
                'type'=>'admin',
                'cr_time'=>time(),
                'text'=> $row->name2.', добро пожаловать на портал PSY-LINE.ORG!'
                );
                $this->db->insert('msgs',$data);
                 
                redirect('admin/candidats');
        }
        else
        {
           $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'er',
                'ses_msg_text' => '<strong>Произошла ошибка!</strong><br /><br />
                Что-то пошло не так ...'
                );
                $this->session->set_userdata($ses);
                redirect('admin/candidats');  
        }
        }
        else
        {
            redirect();
        } 
    }
    
    public function conf_atest()
    {
      if($this->_adminonline() == 1)
       {
           $this->load->model('user_model');
           $user = $this->user_model->user_info($_POST['user']);   
                    
           if($_POST['wish'] == $_POST['rang']) //СРАЗУ АКТИВИРУЕМ
           {
                //Обновления данных в БД псилайнеров (СТАТУС и РАНГ)
                $data = array('status'=>6,'rang'=>$_POST['rang']);
                $this->db->where(array('id'=>$_POST['user']));
                $this->db->update('psyliners',$data);
                
                //Если не пусто пояснение установить сообщение от администрации
                if(!empty($_POST['info']))
                {
                    
                    $data=array(
                        'from'=>0,
                        'to'=>$user->id,
                        'type'=>'admin',
                        'cr_time'=>time(),
                        'text'=>$_POST['info']
                    );
                    $this->db->insert('msgs',$data);
                }
                //ОТПРАВКА ПИСЬМА
               
                $this->load->library('email');
                            
                        $config['mailtype']='html';
                        $this->email->initialize($config);
                    
                        $this->email->from('info@psy-line.org', 'PSY-LINE.ORG');
                        $this->email->to($user->email); 
                        $this->email->subject('IPO|Ваш статус на сайте PSY-LINE.ORG изменился');
                        $this->email->message('
                        Добрый день, '.$user->username.'!<br /><br />
                        
                        Ваша анкета была рассмотрена аттестационной комиссией сайта PSY-LINE.ORG.<br /><br />
                        
                        Аттестационная комиссия утвердила для Вас статус <strong>'.$this->user_model->user_status($_POST['rang']).'</strong>.<br />
                        Теперь вы можете использовать весь потенциал портала PSY-LINE.ORG, в том числе и зарабатывать в качестве псилайнера!<br />
                        Не забудьте настроить свой псилайн-профиль в личном кабинете.
                        <br /><br />
                        
                        Ждем Вас на нашем портале!
                        <br /><br />
                        Это письмо написано роботом. Отвечать на него не надо.
                        ');	
                        $this->email->send();
                        
                        //Статус 
                        $ses = array(
                        'ses_msg' => 1,
                        'ses_msg_type' => 'ok',
                        'ses_msg_text' => '<strong>Псилайнер активирован.</strong><br /><br />
                        Письмо с информацией отправлено пользователю.'
                        );
                        $this->session->set_userdata($ses); 
                        redirect('admin/attestat'); 
           }
           else //ПРЕДЛОГАЕМ ДРУГОЙ СТАТУС
           {
                //Обновления данных в БД псилайнеров (СТАТУС и РАНГ)
                $data = array('status'=>5,'rang'=>$_POST['rang']);
                $this->db->where(array('id'=>$_POST['user']));
                $this->db->update('psyliners',$data);
                
                //Если не пусто пояснение установить сообщение от администрации
                if(!empty($_POST['info']))
                {
                    $this->load->model('user_model');
                    $user = $this->user_model->user_info($_POST['user']);   
                    $data=array(
                        'from'=>0,
                        'to'=>$user->id,
                        'type'=>'admin',
                        'cr_time'=>time(),
                        'text'=>$_POST['info']
                    );
                    $this->db->insert('msgs',$data);
                }
                //ОТПРАВКА ПИСЬМА
               
                $this->load->library('email');
                            
                        $config['mailtype']='html';
                        $this->email->initialize($config);
                    
                        $this->email->from('info@psy-line.org', 'PSY-LINE.ORG');
                        $this->email->to($user->email); 
                        $this->email->subject('IPO|Ваш статус на сайте PSY-LINE.ORG изменился');
                        $this->email->message('
                        Добрый день, '.$user->username.'!<br /><br />
                        
                        Ваша анкета была рассмотрена аттестационной комиссией сайта PSY-LINE.ORG.<br /><br />
                        
                        Аттестационная комиссия рекомендовала присвоить вам статус <strong>'.$this->user_model->user_status($_POST['rang']).'</strong>.<br />
                        Если Вы согласны с этим решением, то все что осталось для начала работы в качестве псилайнера, это 
                        подвердить свое согласие с этим решением в своем личном кабинете.
                        <br /><br />
                        
                        Ждем Вас на нашем портале!
                        <br /><br />
                        Это письмо написано роботом. Отвечать на него не надо.
                        ');	
                        $this->email->send();
                        
                        //Статус 
                        $ses = array(
                        'ses_msg' => 1,
                        'ses_msg_type' => 'ok',
                        'ses_msg_text' => '<strong>Статус псилайнер изменен.</strong><br /><br />
                        Письмо с информацией отправлено пользователю.'
                        );
                        $this->session->set_userdata($ses); 
                        redirect('admin/attestat'); 
            }     
       }
       else
       {
        redirect();
       }
    }
    
}
