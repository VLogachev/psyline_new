<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends CI_Controller {
    
	
    public function index($page = 1)
    {
        
        $query = $this->db->get_where('articles',array('status'=>1));
        $num = $query->num_rows();
        
        $this->load->library('pagination');

        $config['base_url'] = base_url().'articles/index/';
        $config['total_rows'] = $num;
        $config['per_page'] = 20; 
        $config['full_tag_open'] = '<p class="pagination">';
        $config['full_tag_close'] = '</p>';
        $config['first_link'] = 'В начало';
        $config['last_link'] = 'В конец';
        $config['next_link'] = 'Далее';
        $config['prev_link'] = 'Назад';
        $config['cur_tag_open'] = '<span>';
        $config['cur_tag_close'] = '</span>';
        
        $offset = $config['per_page']*($page-1);
        
        $this->pagination->initialize($config); 
        
        $data = array();
        //Категории
        $query = $this->db->get_where('articles_category',array('parent_id'=>0,'status'=>1));
        $categorys = $query->result();
        $data['categorys'] = $categorys;
        
        //СТАТЬИ
        
        $data['articles'] = $this->articles_model->get_articles($config['per_page'],$offset);
        
        
        $data['title'] = 'Публикации PSY-LINE.ORG';
        //$data['item'] = $article;
        $this->load->library('data');
        
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        
        $this->load->view('articles/items');
        
        $this->load->view('base/clear');
        $this->load->view('base/footer');
    }
    
    
    public function category($category, $page = 1)
    {
        
        $query = $this->db->get_where('articles',array('status'=>1,'category_id'=>$category));
        $num = $query->num_rows();
        
        $this->load->library('pagination');

        $config['base_url'] = base_url().'articles/category/'.$category;
        $config['total_rows'] = $num;
        $config['per_page'] = 20; 
        $config['full_tag_open'] = '<p class="pagination">';
        $config['full_tag_close'] = '</p>';
        $config['first_link'] = 'В начало';
        $config['last_link'] = 'В конец';
        $config['next_link'] = 'Далее';
        $config['prev_link'] = 'Назад';
        $config['cur_tag_open'] = '<span>';
        $config['cur_tag_close'] = '</span>';
        
        $offset = $config['per_page']*($page-1);
        
        $this->pagination->initialize($config); 
        
        $data = array();
        //Категории
        $query = $this->db->get_where('articles_category',array('parent_id'=>0,'status'=>1));
        $categorys = $query->result();
        $data['categorys'] = $categorys;
        
        //СТАТЬИ
        $data['articles'] = $this->articles_model->get_articles_category($config['per_page'],$offset,$category);
        $data['category_name'] = $this->articles_model->category_name($category);
        
        $data['title'] = 'Публикации PSY-LINE.ORG';
        //$data['item'] = $article;
        $this->load->library('data');
        
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        
        $this->load->view('articles/items_category');
        
        $this->load->view('base/clear');
        $this->load->view('base/footer');
    }
    
    
	public function item($id)
	{
        $data = array();
        
        $query = $this->db->get_where('articles',array('id'=>$id));
        $article = $query->row();
        
        $this->db->where('id',$article->id);
        $this->db->set('hits',$article->hits + 1);
        $this->db->update('articles');
        
        
        $data['title'] = $article->title;
        $data['item'] = $article;
        
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        
        $this->load->view('articles/item');
        
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
    
    public function add_comment(){
        
        $md5 = md5($_POST['a_id'].'boops'.$_POST['u_id']);
        if($_POST['md']!=$md5)
        {
            $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'er',
                'ses_msg_text' => '<strong>Ошибка!</strong><br />
                Обнаружена попытка подмены данных.'
                );
            $this->session->set_userdata($ses);
            redirect('articles/item/'.$_POST['a_id']);
        }
        
        if(iconv_strlen($_POST['msg'],'UTF-8')<=5)
        {
            $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'er',
                'ses_msg_text' => '<strong>Ошибка!</strong><br />
                Нельзя добавить пустой или слишком короткий комментарий.'
                );
            $this->session->set_userdata($ses);
            redirect('articles/item/'.$_POST['a_id']);
        }
        
        if(iconv_strlen($_POST['msg'],'UTF-8')>5 AND $_POST['md'] == $md5)
        {
            $data = array(
            'article_id'=>$_POST['a_id'],
            'user_id'=>$_POST['u_id'],
            'create'=>time(),
            'text'=>$_POST['msg']
            );
            $this->db->insert('articles_coments',$data);
            
            
            $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'ok',
                'ses_msg_text' => '<strong>Ok!</strong><br />
                Ваш комментарий успешно добавлен!'
                );
            $this->session->set_userdata($ses);
            redirect('articles/item/'.$_POST['a_id']);
        }
        
         
    }
    
    function add_article()
    {
        
        if($this->user_model->check_user()==true)//Проверяем пользователя
        {
            //Проверяем корректность категории
            if($this->articles_model->spec_category($_POST['cat_id']) == true)//Специальная категория
            {
                //Проверка на админа
                if($this->user_model->is_admin($this->session->userdata('user_id')) == true)//Админ
                {
                    $txt = preg_replace('~style="[^"]*"~i', '', $_POST['text']);
                    $txt = strip_tags($txt,'<i><u><b><p></strong><img><div><br>');
                    
                    $data = array(
                    'create'=>time(),
                    'user_id'=>$this->session->userdata('user_id'),
                    'title'=>$_POST['header'],
                    'category_id'=>$_POST['cat_id'],
                    'text'=>$txt,
                    'status'=>1,
                    'hits'=>0
                    );
                    
                    $this->db->insert('articles',$data);
                    
                    $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Статья успешно опубликована!</strong><br />'                 
                    );
                    $this->session->set_userdata($ses);
                    redirect('user/profile#tabs-4');
                }
                else
                {
                    redirect();
                }
            }
            else //Обычная категрия
            {
                    $txt = preg_replace('~style="[^"]*"~i', '', $_POST['text']);
                    $txt = strip_tags($txt,'<i><u><b><p></strong><img><div><br>');
                    
                    $data = array(
                    'create'=>time(),
                    'user_id'=>$this->session->userdata('user_id'),
                    'title'=>$_POST['header'],
                    'category_id'=>$_POST['cat_id'],
                    'text'=>$txt,
                    'status'=>1,
                    'hits'=>0
                    );
                    
                    $this->db->insert('articles',$data);
                    
                    $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Статья успешно опубликована!</strong><br />'                 
                    );
                    $this->session->set_userdata($ses);
                    redirect('user/profile#tabs-4');
            }
        }
        else //Какой-то косяк
        {
            redirect();
        }   
    }

}
