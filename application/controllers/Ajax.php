<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
    
 
    public function online(){
        
        //ставим оффлайм всем у кого последняя активность была более, чем 60 секунд онлайн
        $this->db->where('last_act <=', time()-(60));
        $this->db->update('users', array('online'=>0)); 
        
        if(isset($this->session->userdata['loged_in']) AND $this->session->userdata['loged_in']==1)
        {
        //устанавливаем метку времени для текущего пользователя и включаем онлайн
        
        $this->db->where('id', $this->session->userdata['user_id']);
        $this->db->update('users', array('online'=>1,'last_act'=>time())); 
        
        }
        //echo time();
    } 
    
    public function msg_read($msg_id)
    {
        echo $msg_id;
        $this->db->where('id',$msg_id);
        $data = array('new'=>0);
        $this->db->update('msgs',$data);
    }  
    
    public function cons($user_id,$znch,$tip)
    {
        if($znch == 0){$z = 1;}else{$z = 0;}
        
        $this->db->where('psyliner_id',$user_id);
        $data = array($tip=>$z);
        $this->db->update('psyliners_params',$data);
        
    }
    
    public function article_like($article_id,$user_id){
        
        
        $sql = $this->db->get_where('articles_like',array('user_id'=>$user_id,'article_id'=>$article_id));
        if($sql->num_rows()>0)
        {
            $this->db->delete('articles_like',array('article_id'=>$article_id,'user_id'=>$user_id));
            echo "Эта статья Вам больше не нравиться";
        }
        else
        {
            $data = array(
            'cr_time'=>time(),
            'article_id'=>$article_id,
            'user_id'=>$user_id
            );
            $this->db->insert('articles_like',$data);
            echo "Вам нравится эта статья";
        }
        
    }  
    
    public function cons_price($tip, $psyliner_id, $znch)
    {        
        $this->db->where('psyliner_id',$psyliner_id);
        $data = array($tip=>$znch);
        $this->db->update('psyliners_params',$data);
        
    }
    
    public function count_new_msg($user_id)
    {
        $this->db->where(array('to'=>$user_id,'new'=>1));
        $sql = $this->db->get('msgs');
        
        $n_new_msg = $sql->num_rows();
        echo $n_new_msg;
    }
    
    public function psyliner_phone($psyliner_id,$user_id=0)
    {
        $this->db->select('telefon');
        $this->db->where('id',$psyliner_id);
        $sql = $this->db->get('psyliners');
        $row = $sql->row();
        
        $params = $this->user_model->psyliner_params_by_psyliner_id($psyliner_id);
        
        //Вставка просмотра телефона
        $data = array(
        'phone_view_date' => time(),
        'psyliner_id' => $psyliner_id,
        'user_id' => $user_id
        );
        $this->db->insert('psyliners_views',$data);
        
        echo '<br /><strong>Телефон</strong>: <span style="font-size: 1.3em;">+'.$row->telefon.'</span><br />';
        echo '<strong>Адрес</strong>: '.$params->person_adres.' <a href="javascript:window.open(\'https://www.google.ru/maps/place/'.$params->person_adres.' '.$params->person_city.'\', \'maps\', \'width=800,height=600\')">См. на карте</a><br /><br />';
        echo '<strong>Условия личной встречи</strong>:<br /><em>'.$params->person_desc.'</em>';
    }
    
    public function cons_time($t,$user_id)
    {
        $days = array('Пнд','Втр','Срд','Чтв','Птн','<span style="color:#c00;">Сбт</span>','<span style="color:#c00;">Вск</span>');
        $mes_array = array('Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря');
        
        $denn = date('N',$t);//день недели
        $den = date('j',$t);
        $mes = date('n',$t);
        $god = date('Y',$t);
        
        echo '<br /><span style="font-size: 2em;">'.' '.$den.' '.$mes_array[$mes-1].' '.$god.' ('.$days[$denn-1].')</span><br />';
        
        echo '
        <h3>Выберите желаемое время для консультаций по московскому времени</h3>
        ';
        $ii = 0;
        for ($i = $t; $i <= $t+(86400-3600); $i=$i+(86400/24) ) {
            $ii++;
            
            $this->db->where(array(
            'psyliner_id'=>$this->user_model->psyliner_id($user_id),
            'time_from'=> $i
            ));
            
            $sql = $this->db->get('cons_time_for');
            $n = $sql->num_rows();
            if($n>0){$s = 'style="background-color: #D6FFD2;"';$st='st="1"';}else{$s = '';$st='st="0"';}
            
            echo '
            <div '.$s.' class="time_of_cons" t="'.$i.'" uid="'.$user_id.'" '.$st.'>
            '.date('H:i',$i).' - '.date('H:i',$i+(50*60)).'
            </div>
            ';
            
            if($ii%6==0){echo '<br />';}
            }
    }
    
    public function add_cons_time($t, $uid)
    {
       $this->load->model('user_model');
       $sql = $this->db->get_where('cons_time_for',array('time_from'=>$t,'psyliner_id'=>$this->user_model->psyliner_id($uid)));
       if($sql->num_rows()==0)
       {
            $data = array(
            'psyliner_id'=>$this->user_model->psyliner_id($uid),
            'time_from'=>$t
            );
            $this->db->insert('cons_time_for',$data);
            echo 'add';
       }
       else
       {
            $data = array(
            'psyliner_id'=>$this->user_model->psyliner_id($uid),
            'time_from'=>$t
            );
            $this->db->where($data);
            $this->db->delete('cons_time_for');
            echo 'dell';
       }       
    }
    
    public function show_cons_time($t, $uid, $tip)
    {
        $str = 'с '.date('H:i',$t).' до '.date('H:i',$t+(60*50)).' <span>'.date('j M Y',$t).'</span>';
        if($tip == 'dell')
        {
            echo '<div class="t_o_block" style="background-color: #FFDFDF;">Удалено время '.$str.'</div>';
        }
        if($tip == 'add')
        {
            echo '<div class="t_o_block" style="background-color: #D6FFD2;">Добавлено время '.$str.'</div>';
        }
        
    }
    public function ch_cons_now($pid,$val)
    {
        $this->db->select('cons_now');
        $this->db->where(array(
        'psyliner_id'=>$pid
        ));
        $sql = $this->db->get('psyliners_params');
        $res = $sql->row();
        
        if($val == 0)//сейчас выключено
        {
            $data = array('cons_now'=>1);
            $this->db->where(array('psyliner_id'=>$pid));
            $this->db->update('psyliners_params',$data);
        }
        else //сейчас включено
        {
            $data = array('cons_now'=>0);
            $this->db->where(array('psyliner_id'=>$pid));
            $this->db->update('psyliners_params',$data);
        }
        
    }
    
    public function ch_cons_free($pid,$val)
    {
        $this->db->select('free_cons');
        $this->db->where(array(
        'psyliner_id'=>$pid
        ));
        $sql = $this->db->get('psyliners_params');
        $res = $sql->row();
        
        if($val == 0)//сейчас выключено
        {
            $data = array('free_cons'=>1);
            $this->db->where(array('psyliner_id'=>$pid));
            $this->db->update('psyliners_params',$data);
        }
        else //сейчас включено
        {
            $data = array('free_cons'=>0);
            $this->db->where(array('psyliner_id'=>$pid));
            $this->db->update('psyliners_params',$data);
        }
    }
    
    function cons_time_for_user($t,$pid,$code)
    {
        $days = array('Пнд','Втр','Срд','Чтв','Птн','<span style="color:#c00;">Сбт</span>','<span style="color:#c00;">Вск</span>');
        $mes_array = array('Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря');
        
        $denn = date('N',$t);//день недели
        $den = date('j',$t);
        $mes = date('n',$t);
        $god = date('Y',$t);
        
        echo '<br /><h1>'.' '.$den.' '.$mes_array[$mes-1].' '.$god.' ('.$days[$denn-1].')</h1>';
        
         echo '
        <p style="text-indent: 0px;">Выберите желаемое время для консультаций.<br />
        <strong>Внимание!</strong> Время указано московское.</p>
        ';
        $ii = 0;
        for ($i = $t; $i <= $t+(86400-3600); $i=$i+(86400/24) ) {
            $ii++;
            
            $this->db->where(array(
            'psyliner_id'=>$pid,
            'time_from'=> $i
            ));
            
            $sql = $this->db->get('cons_time_for');
            $n = $sql->num_rows();
            if($n>0){$s = 'style="background-color: #D6FFD2;"';$st='st="1"';}else{$s = '';$st='st="0"';}
            
            echo '
            <div '.$s.' class="time_of_cons_for_user" t="'.$i.'" uid="'.$pid.'" '.$st.'>
            '.date('H:i',$i).' - '.date('H:i',$i+(50*60)).'
            </div>
            ';
            
            if($ii%6==0){echo '<br />';}
            }
    }

    /**
     * сохраняет blob видео и аудио
     */
    function upload_blob(){

        $conferenceFolder = APPPATH . "../videos/" . $this->input->post('conference-name') . "/" . $this->input->post('user-id');

        //если нет директории для конференции, то создаем
        if (!file_exists($conferenceFolder)) {
            mkdir($conferenceFolder, 0775, true);
        }

        $config['upload_path']          = $conferenceFolder;
        $config['allowed_types']        = '*';

        if($this->input->post('video-filename')){
            //сохраняем видео
            $config['file_name']  = $this->input->post('video-filename');
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('video-blob')) {
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
            }
        } else {
            //сохраняем аудио
            $config['file_name']  = $this->input->post('audio-filename');
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('audio-blob')) {
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
            }
        }

    }

    /**
     * Обновляет актуальное время начала и завершения сессии
     */
    function update_session_time(){

        $conferenceName = $this->input->post('conferenceName');
        $type = $this->input->post('type'); // open/close
        $userID = $this->input->post('userID');
        $consultationID = explode("-", $conferenceName)[1];

        //открыть сессию может только псилайнер
        if($type == "open"){
            //находим текущую консультацию
            $consSession = $this->db->get_where("cons_sessions",['cons_id' => $consultationID])->row();
            //если времени начала нет, то вставляем его
            if(empty($consSession->from_act)){
                $this->db->where('cons_id', $consSession->cons_id);
                $this->db->update('cons_sessions', ['from_act' => time()]);
            }
        }

        if($type == "close"){
            //находим роль пользователя, который нажал "закрыть"
            $role_id = $this->db->get_where("cons_users",['cons_id' => $consultationID, "user_id" => $userID])->row()->role_id;
            //если это псилайнер или ведущий, то обновляем время
            if($role_id == 1 || $role_id == 3){
                $consSession = $this->db->get_where("cons_sessions",['cons_id' => $consultationID])->row();
                $this->db->where('cons_id', $consSession->cons_id);
                $this->db->update('cons_sessions', ['to_act' => time()]);
            }
        }

    }

    /**
     * Сохраняет в БД лог событий консультации
     */
    function log_consultation(){

        $consultationID = $this->input->post('consultation_id');
        $message = base64_decode($this->input->post('message'));

        $oldConsultation = $this->db->get_where("cons_calendar",['id' => $consultationID])->row();
        $newLogMessage = $oldConsultation->log . $message;
        //обновляем лог консультации
        $this->db->where('id', $oldConsultation->id);
        $this->db->update('cons_calendar', ['log' => $newLogMessage]);

    }

}