<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

public function sms(){
    $body=file_get_contents("http://sms.ru/sms/send?api_id=a3c719e2-fe97-9a64-5525-f8bc598b335e&to=79064320205&text=".urlencode("Привет, Колбаська!!"));
    //echo $body;
}

public function index()
	{
        //проверка на уже вошел
        if( $this->session->userdata('loged_in') == 1)
        {
            $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'ok',
                'ses_msg_text' => '<strong>Вы уже вошли в аккаунт!</strong><br />
                Если у вас несколько учетных записей и вы хотите войти в другой аккаунт, то сначала 
                следует <a href="'.base_url().'user/logout">выйти</a> из текущего аккаунта.<br />
                '
                );
                $this->session->set_userdata($ses);
                redirect();
        }
        else
        {
            $this->load->helper('form');
    		
            $this->load->view('popup/header');
            $this->load->view('user/login');
            $this->load->view('popup/footer');
        }
	}
 
 
 public function upload($img){
    
    if( $this->session->userdata('loged_in') == 1)
        {
        
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        if($img == 'foto'){$foto = 'фотографию профиля';$folder = 'users_img/';}
        if($img == 'pasport'){$foto = 'изображение первой страницы паспорта';$folder = 'users_img/pasport/';}
        if($img == 'diplom'){$foto = 'скан диплома об образовании';$folder = 'users_img/diplom/';}
        if($img == 'dogovor'){$foto = 'скан подписанной копии договора';$folder = 'users_img/dogovor/';}
        if($img == 'dop_diplom1'){$foto = 'дополнительный документ 1';$folder = 'users_img/dop_diplom/';}
        if($img == 'dop_diplom2'){$foto = 'дополнительный документ 2';$folder = 'users_img/dop_diplom/';}
        if($img == 'dop_diplom3'){$foto = 'дополнительный документ 3';$folder = 'users_img/dop_diplom/';}
        
        //Удаление уже существующего фото
        $this->user_model->del_psyliner_foto($img,$folder,$this->session->userdata('user_id'));
        
        if($this->session->userdata('upload_er') != '') 
        {$er = $this->session->userdata('upload_er');}
        else
        {
            $er = '';
        }
        
        $data = array('foto'=>$foto,'img'=>$img,'error'=>$er);
        
        $this->load->view('popup/header',$data);
        $this->load->view('user/upload');    
        $this->load->view('popup/footer');
        
        }
        else
        {
            redirect();
        }
 }
 
 
 function do_upload()
	{
		
        $this->load->model('user_model');
        $info = $this->user_model->psyliner_info($this->session->userdata('user_id'));
        
        if($_POST['img']=='foto')
        {
        
        $config['upload_path'] = './users_img/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '5000';
		$config['max_width']  = '5000';
		$config['max_height']  = '5000';
        $config['encrypt_name'] = true;
        $config['overwrite'] = true;
		
		$this->load->library('upload', $config);
	
		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			$ses = array('upload_er' => $error);
            $this->session->set_userdata($ses);
            
            redirect('user/upload/foto');
			
		}	
		else
		{
			$img = $this->upload->data();
            $rand = rand(0,1000);
            $t = time();
            
            $img_full_name =  $img['file_path'].$t.$rand.'.'.$img['image_type'];
            $img_short_name = $t.$rand.'.'.$img['image_type'];
            
            rename($img['full_path'], $img_full_name);
			
            
            $data = array(
                        'foto' => $img_short_name
                );    
            $this->db->where('id',$info->id);
            $this->db->update('psyliners', $data);//Вносисм имя в БД
            
            //Ресайз
            $config['image_library'] = 'gd2'; // выбираем библиотеку
            $config['source_image']	= $img_full_name; 
            $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
            //$config['new_image'] = APPPATH.'../users_img/thumb/';
            $config['maintain_ratio'] = TRUE; // сохранять пропорции
            $config['width']	= 200; // и задаем размеры
            
            
            $this->load->library('image_lib', $config); // загружаем библиотеку 
            $this->image_lib->resize(); // и вызываем функцию
            
            
            //Водяные знаки
            $config['source_image']	= $img_full_name;
            $config['wm_text'] = 'www.psy-line.org';
            //$config['new_image'] = APPPATH.'../users_img/';
            $config['wm_type'] = 'text';
            $config['wm_font_path'] = './system/fonts/CGR.ttf';
            $config['wm_font_size']	= '10';
            $config['wm_font_color'] = 'ffffff';
            $config['wm_vrt_alignment'] = 'bottom';
            $config['wm_hor_alignment'] = 'right';
            $config['wm_padding'] = '-2';
            $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
            
            $this->image_lib->initialize($config); 
            $this->image_lib->watermark();
            
            redirect('user/profile');
            }
            }//Конец фото
            
            if($_POST['img']=='diplom')
            {
            
            $config['upload_path'] = './users_img/diplom/';
    		$config['allowed_types'] = 'gif|jpg|jpeg|png';
    		$config['max_size']	= '5000';
    		$config['max_width']  = '5000';
    		$config['max_height']  = '5000';
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = TRUE;
    		
    		$this->load->library('upload', $config);
    	
    		if ( ! $this->upload->do_upload())
    		{
    			$error = array('error' => $this->upload->display_errors());
    			$ses = array('upload_er' => $error);
                $this->session->set_userdata($ses);
                
                redirect('user/upload/diplom');
    			
    		}	
    		else
    		{
        		$img = $this->upload->data();
                $rand = rand(0,1000);
                $t = time().md5($this->session->userdata('user_id'));
                
                $img_full_name =  $img['file_path'].$t.$rand.'.'.$img['image_type'];
                $img_short_name = $t.$rand.'.'.$img['image_type'];
                
                rename($img['full_path'], $img_full_name);
                
                $data = array('foto_diplom' => $img_short_name); 
                $this->db->where('id',$info->id);
                $this->db->update('psyliners', $data);//Вносисм имя в БД            
                      
                
                
                //РЕСАЙЗ
                $config['image_library'] = 'gd2'; // выбираем библиотеку
                $config['source_image']	= $img_full_name; 
                $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
                $config['maintain_ratio'] = TRUE; // сохранять пропорции
                $config['width']	= 900; // и задаем размеры

                $this->load->library('image_lib', $config); // загружаем библиотеку 
                $this->image_lib->resize(); // и вызываем функцию
                 
                redirect('user/profile');
                }
            }//Конец диплома
            
            if($_POST['img']=='pasport')
            {
            
            $config['upload_path'] = './users_img/pasport/';
    		$config['allowed_types'] = 'gif|jpg|jpeg|png';
    		$config['max_size']	= '5000';
    		$config['max_width']  = '5000';
    		$config['max_height']  = '5000';
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = TRUE;
    		
    		$this->load->library('upload', $config);
    	
    		if ( ! $this->upload->do_upload())
    		{
    			$error = array('error' => $this->upload->display_errors());
    			$ses = array('upload_er' => $error);
                $this->session->set_userdata($ses);
                
                redirect('user/upload/pasport');
    			
    		}	
    		else
    		{
    			$img = $this->upload->data();
                $rand = rand(0,1000);
                $t = time().md5($this->session->userdata('user_id'));
                
                $img_full_name =  $img['file_path'].$t.$rand.'.'.$img['image_type'];
                $img_short_name = $t.$rand.'.'.$img['image_type'];
                
                rename($img['full_path'], $img_full_name);
                
                $data = array('foto_pasport' => $img_short_name); 
                $this->db->where('id',$info->id);
                $this->db->update('psyliners', $data);//Вносисм имя в БД            
                      
                
                
                //РЕСАЙЗ
                $config['image_library'] = 'gd2'; // выбираем библиотеку
                $config['source_image']	= $img_full_name;
                $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
                $config['maintain_ratio'] = TRUE; // сохранять пропорции
                $config['width']	= 900; // и задаем размеры

                $this->load->library('image_lib', $config); // загружаем библиотеку 
                $this->image_lib->resize(); // и вызываем функцию
                 
                redirect('user/profile');
                }
            }//Конец паспорта
            
            if($_POST['img']=='dogovor')
            {
            
            $config['upload_path'] = './users_img/dogovor/';
    		$config['allowed_types'] = 'gif|jpg|jpeg|png';
    		$config['max_size']	= '5000';
    		$config['max_width']  = '5000';
    		$config['max_height']  = '5000';
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = TRUE;
    		
    		$this->load->library('upload', $config);
    	
    		if ( ! $this->upload->do_upload())
    		{
    			$error = array('error' => $this->upload->display_errors());
    			$ses = array('upload_er' => $error);
                $this->session->set_userdata($ses);
                
                redirect('user/upload/dogovor');
    			
    		}	
    		else
    		{
    			$img = $this->upload->data();
                $rand = rand(0,1000);
                $t = time().md5($this->session->userdata('user_id'));
                
                $img_full_name =  $img['file_path'].$t.$rand.'.'.$img['image_type'];
                $img_short_name = $t.$rand.'.'.$img['image_type'];
                
                rename($img['full_path'], $img_full_name);
                
                $data = array('foto_dogovor' => $img_short_name);
                $this->db->where('id',$info->id); 
                $this->db->update('psyliners', $data);//Вносисм имя в БД            
                      
                
                
                //РЕСАЙЗ
                $config['image_library'] = 'gd2'; // выбираем библиотеку
                $config['source_image']	= $img_full_name; 
                $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
                $config['maintain_ratio'] = TRUE; // сохранять пропорции
                $config['width']	= 900; // и задаем размеры

                $this->load->library('image_lib', $config); // загружаем библиотеку 
                $this->image_lib->resize(); // и вызываем функцию
                 
                redirect('user/profile');
                }
            }//Конец договора
            
            if($_POST['img']=='dop_diplom1')
            {
            
            $config['upload_path'] = './users_img/dop_diplom/';
    		$config['allowed_types'] = 'gif|jpg|png';
    		$config['max_size']	= '5000';
    		$config['max_width']  = '5000';
    		$config['max_height']  = '5000';
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = TRUE;
    		
    		$this->load->library('upload', $config);
    	
    		if ( ! $this->upload->do_upload())
    		{
    			$error = array('error' => $this->upload->display_errors());
    			$ses = array('upload_er' => $error);
                $this->session->set_userdata($ses);
                
                redirect('user/upload/dop_diplom1');
    			
    		}	
    		else
    		{
    			$img = $this->upload->data();
                $rand = rand(0,1000);
                $t = time().md5($this->session->userdata('user_id'));
                
                $img_full_name =  $img['file_path'].$t.$rand.'.'.$img['image_type'];
                $img_short_name = $t.$rand.'.'.$img['image_type'];
                
                rename($img['full_path'], $img_full_name);
                 
                $data = array('foto_dop_diplom1' => $img_short_name);
                $this->db->where('id',$info->id); 
                $this->db->update('psyliners', $data);//Вносисм имя в БД            
                      
                
               
                //РЕСАЙЗ
                $config['image_library'] = 'gd2'; // выбираем библиотеку
                $config['source_image']	= $img_full_name; 
                $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
                $config['maintain_ratio'] = TRUE; // сохранять пропорции
                $config['width']	= 900; // и задаем размеры

                $this->load->library('image_lib', $config); // загружаем библиотеку 
                $this->image_lib->resize(); // и вызываем функцию
                 
                redirect('user/profile');
                
                }
            }//Конец доп1
            
            if($_POST['img']=='dop_diplom2')
            {
            
            $config['upload_path'] = './users_img/dop_diplom/';
    		$config['allowed_types'] = 'gif|jpg|jpeg|png';
    		$config['max_size']	= '5000';
    		$config['max_width']  = '5000';
    		$config['max_height']  = '5000';
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = TRUE;
    		
    		$this->load->library('upload', $config);
    	
    		if ( ! $this->upload->do_upload())
    		{
    			$error = array('error' => $this->upload->display_errors());
    			$ses = array('upload_er' => $error);
                $this->session->set_userdata($ses);
                
                redirect('user/upload/dop_diplom2');
    			
    		}	
    		else
    		{
    			$img = $this->upload->data();
                $rand = rand(0,1000);
                $t = time().md5($this->session->userdata('user_id'));
                
                $img_full_name =  $img['file_path'].$t.$rand.'.'.$img['image_type'];
                $img_short_name = $t.$rand.'.'.$img['image_type'];
                
                rename($img['full_path'], $img_full_name);
                
                $data = array('foto_dop_diplom2' => $img_short_name);
                $this->db->where('id',$info->id); 
                $this->db->update('psyliners', $data);//Вносисм имя в БД            
                      
                
                
                //РЕСАЙЗ
                $config['image_library'] = 'gd2'; // выбираем библиотеку
                $config['source_image']	= $img_full_name; 
                $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
                $config['maintain_ratio'] = TRUE; // сохранять пропорции
                $config['width']	= 900; // и задаем размеры

                $this->load->library('image_lib', $config); // загружаем библиотеку 
                $this->image_lib->resize(); // и вызываем функцию
                 
                redirect('user/profile');
                }
            }//Конец доп2
            
            if($_POST['img']=='dop_diplom3')
            {
            
            $config['upload_path'] = './users_img/dop_diplom/';
    		$config['allowed_types'] = 'gif|jpg|jpeg|png';
    		$config['max_size']	= '5000';
    		$config['max_width']  = '5000';
    		$config['max_height']  = '5000';
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = TRUE;
    		
    		$this->load->library('upload', $config);
    	
    		if ( ! $this->upload->do_upload())
    		{
    			$error = array('error' => $this->upload->display_errors());
    			$ses = array('upload_er' => $error);
                $this->session->set_userdata($ses);
                
                redirect('user/upload/dop_diplom3');
    			
    		}	
    		else
    		{
    			$img = $this->upload->data();
                $rand = rand(0,1000);
                $t = time().md5($this->session->userdata('user_id'));
                
                $img_full_name =  $img['file_path'].$t.$rand.'.'.$img['image_type'];
                $img_short_name = $t.$rand.'.'.$img['image_type'];
                
                rename($img['full_path'], $img_full_name);
                
                $data = array('foto_dop_diplom3' => $img_short_name);
                $this->db->where('id',$info->id); 
                $this->db->update('psyliners', $data);//Вносисм имя в БД            
                      
                
                
                //РЕСАЙЗ
                $config['image_library'] = 'gd2'; // выбираем библиотеку
                $config['source_image']	= $img_full_name; 
                $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
                $config['maintain_ratio'] = TRUE; // сохранять пропорции
                $config['width']	= 900; // и задаем размеры

                $this->load->library('image_lib', $config); // загружаем библиотеку 
                $this->image_lib->resize(); // и вызываем функцию
                 
                redirect('user/profile');
                }
            }//Конец доп3
            
		}	
        
        
        
    public function do_upload_user_avatar()
	{
		
        if( $this->session->userdata('loged_in') == 1)
        {
            $this->db->where('id',$this->session->userdata('user_id'));
            $sql = $this->db->get('users');
            $info = $sql->row();
            
            $config['upload_path'] = './users_img/';
    		$config['allowed_types'] = 'gif|jpg|png';
    		$config['max_size']	= '5000';
    		$config['max_width']  = '5000';
    		$config['max_height']  = '5000';
            $config['encrypt_name'] = true;
            $config['overwrite'] = true;
    		
    		$this->load->library('upload', $config);
            if ( ! $this->upload->do_upload())
    		{
    			$error = array('error' => $this->upload->display_errors());
    			$ses = array('upload_er' => $error);
                $this->session->set_userdata($ses);
                
                redirect('user/profile');
    			
    		}	
            else
    		{
    			$this->user_model->del_foto($this->session->userdata('user_id'));
                
                $img = $this->upload->data();
                $rand = rand(0,1000);
                $t = time();
                
                $img_full_name =  $img['file_path'].$t.$rand.'.'.$img['image_type'];
                $img_short_name = $t.$rand.'.'.$img['image_type'];
                
                rename($img['full_path'], $img_full_name);
    			
                
                $data = array(
                            'foto' => $img_short_name
                    );    
                $this->db->where('id',$info->id);
                $this->db->update('users', $data);//Вносисм имя в БД
                //Ресайз
                $config['image_library'] = 'gd2'; // выбираем библиотеку
                $config['source_image']	= $img_full_name; 
                $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
                //$config['new_image'] = APPPATH.'../users_img/thumb/';
                $config['maintain_ratio'] = TRUE; // сохранять пропорции
                $config['width']	= 200; // и задаем размеры
                
                
                $this->load->library('image_lib', $config); // загружаем библиотеку 
                $this->image_lib->resize(); // и вызываем функцию
                
                //Водяные знаки
                $config['source_image']	= $img_full_name;
                $config['wm_text'] = 'www.psy-line.org';
                //$config['new_image'] = APPPATH.'../users_img/';
                $config['wm_type'] = 'text';
                $config['wm_font_path'] = './system/fonts/CGR.ttf';
                $config['wm_font_size']	= '10';
                $config['wm_font_color'] = 'ffffff';
                $config['wm_vrt_alignment'] = 'bottom';
                $config['wm_hor_alignment'] = 'right';
                $config['wm_padding'] = '-2';
                $config['create_thumb'] = FALSE; // ставим флаг создания эскиза
                
                $this->image_lib->initialize($config); 
                $this->image_lib->watermark();
                
                redirect('user/profile');
            }
          }
          else
          {
            redirect();
          }
      }
        
        
 
 public function login(){
        $sql = $this->db->get_where('users', array('email' => $_POST['email'],'password' => md5($_POST['pass'])));
        if($sql->num_rows()==1) //Совпало
        {
            $row = $sql->row();
            
            $sql = $this->db->get_where('users_groups',array('user_id'=>$row->id));
            $row1 = $sql->result();
            
            $user_role = '';
            foreach($row1 as $role)
            {
                $user_role = $user_role.$role->group_id.'/';
            }
            
            $data = array(
                'last_login' => time(),
                'ip_address' => $_SERVER["REMOTE_ADDR"]
            );
            $this->db->where('id',$row->id);
            $this->db->update('users',$data);
            
            $ses = array(
                'loged_in' => 1,
                'user_id' => $row->id,
                'user_roles' => $user_role,
                'user_code' => md5('boops'.$row->id),
                'user_name' => $row->username
                );
                $this->session->set_userdata($ses);
                redirect();
        }
        else
        {
                $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'er',
                'ses_msg_text' => '<strong>Неверная пара логин/пароль!</strong><br />
                <span style="color: #3d3d3d;">Повторите попытку, <a href="'.base_url().'registration">зарегистрируйтесь</a> или 
                вернитесь на <a href="'.base_url().'">главную</a> страницу</span>.
                '
                );
                $this->session->set_userdata($ses);
                redirect('user');
        }
    }
 
 public function logout(){
        
            
            $ses = array(
                'loged_in' => 0,
                'user_id' => '',
                'user_roles' => ''
                );
                $this->session->set_userdata($ses);
                
                $ses = array(
                'ses_msg' => 1,
                'ses_msg_type' => 'ok',
                'ses_msg_text' => '<strong>Вы успешно вышли из аккаунта!</strong><br />
                '
                );
                $this->session->set_userdata($ses);
                redirect();
    }
    
    public function profile(){

        if( $this->session->userdata('loged_in') == 1)
        {
            if(strrpos($this->session->userdata['user_roles'],'3') !== false)//Пользователь
            {
                $this->db->where('id',$this->session->userdata('user_id'));
                $sql = $this->db->get('users');
                $info = $sql->row();

                //вынимаем все консультации для конкретного пользователя
                $consultations = $this->consultation_model->get_consultations_by_customer_id($this->session->userdata('user_id'));

                $this->load->helper('form');
                $this->load->library('form_validation');
                $this->load->library('Data');
                
                
                $data = array();
                $data['title'] = 'Личный кабинет пользователя';
                $data['info'] = $info;
                
                $this->load->view('base/header',$data);
                $this->load->view('base/top');
                
                $this->load->view('user/profile/user',['consultations' => $consultations]);
        
                $this->load->view('base/clear');
                $this->load->view('base/footer');
        
            }//Конец пользователя
            
            else//Псилайнер или админ или модератор
            {
                $info = $this->user_model->psyliner_info($this->session->userdata['user_id']);
                if($info->status == 2 AND !empty($info->foto) AND !empty($info->foto_diplom) AND !empty($info->foto_pasport)AND !empty($info->foto_dogovor))
                {
                $data = array('status'=>3);
                $this->db->where('id',$info->id);
                $this->db->update('psyliners',$data);
                $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Необходимые документы загружены!</strong><br />
                    <span style="color: #3d3d3d;">
                    На данный момент вы загрузили необходимый минимум документов.<br />
                    При желании вы можете загрузить еще до 3 документов, которые отражают ваш профеесиональный уровень.
                    Однако это вовсе не обязательно</span>.'
                );
                    
                    
                    $this->session->set_userdata($ses);
                    redirect('user/profile');
                }
                if($info->status == 3 AND (empty($info->foto) OR empty($info->foto_diplom) OR empty($info->foto_pasport) OR empty($info->foto_dogovor)))
                    {
                        $data = array('status'=>2);
                        $this->db->where('id',$info->id);
                        $this->db->update('psyliners',$data);
                        redirect('user/profile');
                    }

                $this->load->helper('form');
                $this->load->library('form_validation');
                $this->load->library('Data');
                
                
                $data = array();
                $data['title'] = 'Личный кабинет пользователя';
                
                
                $this->load->view('base/header',$data);
                $this->load->view('base/top');
        
            switch($info->status)
            {
                case 2: $this->load->view('user/profile/status2');break;
                case 3: $this->load->view('user/profile/status3');break;
                case 4: $this->load->view('user/profile/status4');break;
                case 5: $this->load->view('user/profile/status5');break;
                case 6:
                    //получаем все консультации для преподпвателя
                    $consultations = $this->consultation_model->get_consultations_by_psyliner_id($this->session->userdata['user_id']);
                    $this->load->view('user/profile/status6', ['consultations' => $consultations]);
                    break;
                
            }
        
            $this->load->view('base/clear');
            $this->load->view('base/footer');
            } //Конец псилайнера админа или модератора

        }//Конец залогиненого пользователя
        else
        {
            redirect();
        }
    }
    
    public function send_profile(){
        if( $this->session->userdata('loged_in') == 1)
        {
            $this->load->model('user_model');    
            $info = $this->user_model->psyliner_info($this->session->userdata['user_id']);
            
            $data = array(
                'wish_rang' => $_POST['status'],
                'status'=>4,
                'wish_rang_info' => $_POST['info']); 
            $this->db->where('id',$info->id);
            $this->db->update('psyliners',$data);
            
            
            $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Ваша анкета успешно отправлена администрации!</strong><br /><br />
                    <span style="color: #3d3d3d;">Разультаты аттестации будут высланы Вам на элетронную почту</span>.
                    '
                    );
            $this->session->set_userdata($ses);
            redirect('user/profile');
            
        }
        else
        {
            redirect();
        }
    }
 
 
    public function agree(){
        if( $this->session->userdata('loged_in') == 1)
        {
            $info = $this->user_model->psyliner_info($this->session->userdata['user_id']);
            $data = array(
                'status'=>6
                ); 
            $this->db->where('id',$info->id);
            $this->db->update('psyliners',$data);
            
            
            $data = array(
                'psyliner_id'=>$info->id
                ); 
            $this->db->insert('psyliners_params',$data);
            
            $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Ваша учетная запись успешно активирована!</strong><br />'
                    );
            $this->session->set_userdata($ses);
            redirect('user/profile');  
        }
        else
        {
            redirect();
        }
    }
    
    public function ch_pass(){
        //Проверяем старый пароль
        $user = $this->user_model->user_data($this->session->userdata('user_id'));
        if(md5($_POST['old'])== $user->password)
        {
            //Совпадение новых паролей
            if($_POST['new'] == $_POST['new1'])
            {
                //Кол-во символов в новом пароле
                if (strlen($_POST['new'])<6)
                {
                    $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'er',
                    'ses_msg_text' => '<strong>Пароль слишком короткий!</strong><br />'
                    );
                    $this->session->set_userdata($ses);
                    redirect('user/profile#tabs-2');  
                }
                else
                {
                    //Меняем пароль
                    $data = array('password'=>md5($_POST['new']));
                    $this->db->where('id',$user->id);
                    $this->db->update('users',$data);
                    //Отчет и редирект
                    $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Пароль успешно изменен!</strong><br />'
                    );
                $this->session->set_userdata($ses);
                redirect('user/profile#tabs-2');  
                }
                
            }
            else
            {
                $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'er',
                    'ses_msg_text' => '<strong>Пароли не совпадают!</strong><br />'
                    );
                $this->session->set_userdata($ses);
                redirect('user/profile#tabs-2');  
            }
        
        }
        else
        {
            $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'er',
                    'ses_msg_text' => '<strong>Введен неверный действующий пароль!</strong><br />'
                    );
                $this->session->set_userdata($ses);
                redirect('user/profile#tabs-2');  
        }
        
    }
    
    public function personal_msg(){
        $info = $this->user_model->psyliner_info($this->session->userdata['user_id']);
        $data = array('pers_msg'=>$_POST['msg']);
        $this->db->where('psyliner_id',$info->id);
        $this->db->update('psyliners_params',$data);

        $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Личное обращение успешно изменено!</strong><br />'
                    );
                $this->session->set_userdata($ses);
                redirect('user/profile#tabs-2');  
    }
    
    
    public function set_url(){
        $info = $this->user_model->psyliner_id($this->session->userdata['user_id']);
        $data = array('site_url'=>$_POST['site_url']);
        $this->db->where('psyliner_id',$info);
        $this->db->update('psyliners_params',$data);

        $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Ссылка на web-страниуцу успешно изменена!</strong><br />'
                    );
                $this->session->set_userdata($ses);
                redirect('user/profile#tabs-2');  
    }
    
    public function set_videourl(){
        $info = $this->user_model->psyliner_id($this->session->userdata['user_id']);
        $data = array('video_url'=>$_POST['video_url']);
        $this->db->where('psyliner_id',$info);
        $this->db->update('psyliners_params',$data);

        $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Ссылка на видео изменена!</strong><br />'
                    );
                $this->session->set_userdata($ses);
                redirect('user/profile#tabs-2');  
    }
    
    public function personal(){
        $info = $this->user_model->psyliner_id($this->session->userdata['user_id']);
        
        $data = array(
        'person'=>$_POST['person'],
        'person_city'=>$_POST['person_city'],
        'person_adres'=>$_POST['person_adres'],
        'person_desc'=>$_POST['description'],
        'person_price'=>$_POST['person_price']
        );
        $this->db->where('psyliner_id',$info);
        $this->db->update('psyliners_params',$data);

        $ses = array(
                    'ses_msg' => 1,
                    'ses_msg_type' => 'ok',
                    'ses_msg_text' => '<strong>Параметры личной встречи с клиентом успешно изменены!</strong><br />'
                    );
        $this->session->set_userdata($ses);
        redirect('user/profile#tabs-2');

    }
    
 }