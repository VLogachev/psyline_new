<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {

	public function _user_name($id)
    {
        $this->db->select('first_name, last_name');
        $this->db->from('users');
        $this->db->where('id',$id);
        $query = $this->db->get();
        
        $row = $query->row();
        $name = $row->first_name.' '.$row->last_name;
        return $name;
    }
    
	public function index()
	{
        $data = array();
        $data['title'] = "PSYLINE Форум";
 
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        $this->load->view('forum/items_list');
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
}
