<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Psyliners extends CI_Controller {
    
	public function index()
	{
        $this->load->library('cons');
        
        $data = array();
        $data['title'] = "Специалисты-псилайнеры";
 
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        $this->load->view('psyliner/list');
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
    
    public function profile($id)
	{
 
    $this->db->where("id", $id);
    $sql = $query = $this->db->get('psyliners');
    $psyliner = $sql->row();
    
    $this->user_model->user_info($id);
    
    $params = $this->user_model->psyliner_params_by_psyliner_id($id);
    
    if($psyliner->rang <=6)//Параметры нестатусных псилайнеров
    {
        if($params->person == 1)//Личная встреча
        {
            $per = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/customers.png"/></td>
                <td class="no_img">Личная встреча в <strong>г.'.$params->person_city.' '.number_format($params->person_price, 2, '.', ' ').' &#8381;</strong>.
                <div id="psyliner'.$psyliner->id.'_phone"></div>
                </td>
                <td class="img2_td" style="width: 145px;">
                <a class="btn_with_img view_phone" pid="'.$psyliner->id.'" uid="'.$this->session->userdata('user_id').'" href="#">
                <img style="height: 16px; top:14px;" title="Показать контакты" src="'.base_url().'icos/phone.png"/>
                Показать контакты</a>
                </td>
            </tr>
            </table>
            </div>
            ';
            
        }
        else
        {
            $per = '';
            
        }
        if($params->video == 1)//Видео
        {
            
            $v = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/video.png"/></td>
                <td class="nо_img"> Online Видео-консультация на 
                сайте Psy-Line.org: <strong>'.$this->user_model->price('video_price',$psyliner->rang).'.00 &#8381;
                </strong> за 50 мин</td>
                <td class="img2_td"><img title="Резервирование консультации" src="'.base_url().'icos/table.png"/></td>
            </tr>
            </table>
            </div>
            '; 
        }
        else
        {
            $v = '';
            
        }
        if($params->audio == 1)//Аудио
        {
            $a = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/audio.png"/></td>
                <td class="nо_img"> Online Аудио-консультация на 
                сайте Psy-Line.org: <strong>'.$this->user_model->price('audio_price',$psyliner->rang).'.00 &#8381;</strong> за 30 мин</td>
                <td class="img2_td"><img title="Резервирование консультации" src="'.base_url().'icos/table.png"/></td>
            </tr>
            </table>
            </div>
            '; 
            
        }
        else
        {
            $a = '';
            
        }
        if($params->text == 1)//Чат
        {
            $t = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/chat.png"/></td>
                <td class="nо_img"> Приватный чат на 
                сайте Psy-Line.org: <strong>'.$this->user_model->price('text_price',$psyliner->rang).'.00 &#8381;</strong> за 60 мин.</td>
                <td class="img2_td"><img title="Резервирование консультации" src="'.base_url().'icos/table.png"/></td>
            </tr>
            </table>
            </div>
            '; 
            
        }
        else
        {
            $t = '';
            
        }
    }
    if($psyliner->rang == 7 OR $psyliner->rang == 8) //Ранговитый псилайнер
    {
        if($params->person == 1)//Личная встреча
        { 
            $per = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/customers.png"/></td>
                <td class="no_img">Личная встреча в <strong>г.'.$params->person_city.' '.number_format($params->person_price, 2, '.', ' ').' &#8381;</strong>.
                <div id="psyliner'.$psyliner->id.'_phone"></div>
                </td>
                <td class="img2_td" style="width: 145px;">
                <a class="btn_with_img view_phone" pid="'.$psyliner->id.'" uid="'.$this->session->userdata('user_id').'" href="#">
                <img style="height: 16px; top:14px;" title="Показать контакты" src="'.base_url().'icos/phone.png"/>
                Показать контакты</a>
                </td>
            </tr>
            </table>
            </div>
            ';
            
        }
        else
        {
            $per = '';
            
        }
        if($params->video == 1)//Видео
        {
            
            $v = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/video.png"/></td>
                <td class="nо_img"> Online Видео-консультация на 
                сайте Psy-Line.org: <strong>'.number_format($params->video_price, 2, '.', ' ').' &#8381;
                </strong> за 50 мин</td>
                <td class="img2_td"><img title="Резервирование консультации" src="'.base_url().'icos/table.png"/></td>
            </tr>
            </table>
            </div>
            
            '; 
        }
        else
        {
            $v = '';
            
        }
        if($params->audio == 1)//Аудио
        {
            $a = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/audio.png"/></td>
                <td class="nо_img"> Online Аудио-консультация на 
                сайте Psy-Line.org: <strong>'.number_format($params->audio_price, 2, '.', ' ').' &#8381;</strong> за 30 мин</td>
                <td class="img2_td"><img title="Резервирование консультации" src="'.base_url().'icos/table.png"/></td>
            </tr>
            </table>
            </div>
            
            '; 
            
        }
        else
        {
            $a = '';
            
        }
        if($params->text == 1)//Чат
        {
            $t = 
            '
            <br />
            <div class="prcs">
            <table class="tbl_prcs">
            <tr>
                <td class="img_td"><img src="'.base_url().'icos/chat.png"/></td>
                <td class="nо_img"> Приватный чат на 
                сайте Psy-Line.org: <strong>'.number_format($params->text_price, 2, '.', ' ').' &#8381;</strong> за 60 мин.</td>
                <td class="img2_td"><img title="Резервирование консультации" src="'.base_url().'icos/table.png"/></td>
            </tr>
            </table>
            </div>
            
            '; 
            
        }
        else
        {
            $t = '';
            
        }
    }
    
    
        $data = array();
        $data['title'] = $psyliner->name;
        $data['per'] = $per;
        $data['v'] = $v;
        $data['a'] = $a;
        $data['t'] = $t;
        $data['user'] = $this->user_model->user_info($id);
        $data['psyliner'] = $psyliner;
        $data['params'] = $this->user_model->psyliner_params_by_psyliner_id($id);
        
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        $this->load->view('psyliner/one');
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
    
    public function calendar()
	{
        $data = array();
        $data['title'] = "Специалисты-псилайнеры";
 
        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        $this->load->view('psyliner/calendar');
        $this->load->view('base/clear');
        $this->load->view('base/footer');
	}
}