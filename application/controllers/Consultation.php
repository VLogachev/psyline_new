<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultation extends CI_Controller {

    public function order($tip,$pid,$cod)
    {
        
        if(md5('!'.$pid.$tip)!=$cod) //ПОДМЕНА ДАННЫХ
        {
            $ses = array(
            'ses_msg' => 1,
            'ses_msg_type' => 'er',
            'ses_msg_text' => '<strong>Произошла ошибка:</strong><br />
            Обнаружена попытка подмены данных!'
            );
            $this->session->set_userdata($ses);
            redirect('psyliners');
            //break;
        }
        
        $pid_uid = $this->user_model->user_id_by_psyliner_id($pid); //ID консультанта
        $client_uid = $this->session->userdata('user_id'); //ID клиента
        
        if(!empty($client_uid) AND $client_uid==$pid_uid)//САМ У СЕБЯ
        {
            $ses = array(
            'ses_msg' => 1,
            'ses_msg_type' => 'er',
            'ses_msg_text' => '<strong>Произошла ошибка:</strong><br />
            Вы не можете зарезервировать консультацию у себя!'
            );
            $this->session->set_userdata($ses);
            redirect('psyliners');
            //break;
        }
        
        
        $data = array();
        
        $data['title'] = "Резервирование консультации";
        $data['pid'] = $pid;
        $data['pid_uid'] = $pid_uid;
        if($tip == 'v'){$data['tip']='видео-';}
        if($tip == 'a'){$data['tip']='аудио-';}
        if($tip == 't'){$data['tip']='текстовой ';}

        $this->load->view('base/header',$data);
        $this->load->view('base/top');
        $this->load->view('right/right-b');
        $this->load->view('right/module1');
        $this->load->view('right/right-e');
        
        $this->load->view('consultation/calendar');
        
        $this->load->view('base/clear');
        $this->load->view('base/footer');
    }

    /**
     * Склеивает все аудио и видео фрагменты сессий в 1 файл, запускать по cron'у
     */
    public function merge_media(){
        $consultationID = $this->input->get('consultation_id');
        $videosFolder = APPPATH . "../videos";
        if(empty($consultationID)){
            $this->consultation_model->merge_media($videosFolder);
        } else {
            $this->consultation_model->merge_media($videosFolder, $consultationID);
        }
    }

}