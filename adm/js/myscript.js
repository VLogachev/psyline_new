$(document).ready(function () {
    $(".more_info").toggle(
    function(){
        var num = $(this).attr('n').toString();
        $('.more-info-tr-'+num).show(500);
        $(this).text('Скрыть');
    },
    function(){
        var num = $(this).attr('n').toString();
        $('.more-info-tr-'+num).hide(500);
        $(this).html('Подробно &nabla;');
    }
    );
    
    $("#site_msg.er #close").click(function(){
            $("#overlay_msg").hide();
            $("#site_msg").hide();
        });
        $("#site_msg.ok #close").click(function(){
            $("#overlay_msg").hide();
            $("#site_msg").hide();
    });
});     