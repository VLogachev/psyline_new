$(document).ready(function () {
        
        $(".iframe").colorbox({iframe:true, width:"640px", height:"480px"});
        
        $("#agree").bind("change",function(){ 
        if($(this).is(":checked")){ 
        $("#ifagree").show(300);// делаем что-то, когда чекбокс включен 
        }else{ 
        $("#ifagree").hide(300);// делаем что-то другое, когда чекбокс выключен 
        }});
        
        
        $(".datepicker").datepicker({changeMonth:true, changeYear:true});
        
        $("input.dr").mask("99.99.9999",{placeholder:"дд.мм.гггг"});
        $("input.god").mask("9999",{placeholder:"гггг"});
        $("input.m_g").mask("99.9999",{placeholder:"мм.гггг"});
        $("input.tel2").mask("(999) 999-99-99",{placeholder:"_"});
        
        $("#site_msg.er #close").click(function(){
            $("#overlay_msg").hide();
            $("#site_msg").hide();
        });
        $("#site_msg.ok #close").click(function(){
            $("#overlay_msg").hide();
            $("#site_msg").hide();
        });
        
        
        setInterval(function(){
        $.ajax({
              type: 'POST',
              url: '/ajax/online/'/*,
              success: function(data){
                $('.results').html(data);
              }*/
            });
        },30000);
        
        
        $(".iframe").colorbox({iframe:true, width:"750px", height:"500px"});
        
        
        $("#myonoffswitch2").click(function(){
            var id = $(this).attr("userid");
            var znch = $(this).attr("znch");
            var tip = $(this).attr("tip");
            
            $.ajax({
              type: 'POST',
              url: '/ajax/cons/'+id+'/'+znch+'/'+tip ,
              /*data: "name=John&location=Boston",*/
              success: function(data){
                $('.results').html(data);
              }
            });
        });
        
        $("#myonoffswitch1").click(function(){
            var id = $(this).attr("userid");
            var znch = $(this).attr("znch");
            var tip = $(this).attr("tip");
            
            $.ajax({
              type: 'POST',
              url: '/ajax/cons/'+id+'/'+znch+'/'+tip ,
              /*data: "name=John&location=Boston",*/
              success: function(data){
                $('.results').html(data);
              }
            });
        });
        $("#myonoffswitch").click(function(){
            var id = $(this).attr("userid");
            var znch = $(this).attr("znch");
            var tip = $(this).attr("tip");
            
            $.ajax({
              type: 'POST',
              url: '/ajax/cons/'+id+'/'+znch+'/'+tip
            });
        });
        
        $("#video_price").click(function(){
            var zn = $("#v_price_zn").val();
            var id = $("#v_price_zn").attr('ps_id');
            $.ajax({
              type: 'POST',
              url: '/ajax/cons_price/video_price/'+id+'/'+zn
            });  
        });
        
        $("#audio_price").click(function(){
            var zn = $("#a_price_zn").val();
            var id = $("#a_price_zn").attr('ps_id');
            $.ajax({
              type: 'POST',
              url: '/ajax/cons_price/audio_price/'+id+'/'+zn
            });  
        });
        
        $("#text_price").click(function(){
            var zn = $("#t_price_zn").val();
            var id = $("#t_price_zn").attr('ps_id');
            $.ajax({
              type: 'POST',
              url: '/ajax/cons_price/text_price/'+id+'/'+zn
            });  
        });
        
        
        $("#article_like").click(function(){
            
            var n = $("#who_like").val();
            
            var userid = $(this).attr('uid');
            var artid = $(this).attr('aid');
            var autorid = $(this).attr('autid');
            if(userid == autorid){
                alert("Вы не можете оценивать сами себя!");
            }
            else
            {
                $.ajax({
                  type: 'POST',
                  url: '/ajax/article_like/'+artid+'/'+userid,
                  success: function(data){$('#like_info').html(data);}
                });
                
                //alert ('/ajax/article_like/'+artid+'/'+userid); 
                var newn = n+1;
                $("#who_like").text(newn);      
            } 
        });
        
        
        $(".view_phone").click(function(event){
            
            event.preventDefault();
            var pid = $(this).attr("pid");
            var uid = $(this).attr("uid");
            //alert (pid);
            
            $.ajax({
              type: 'POST',
              url: '/ajax/psyliner_phone/'+pid+'/'+uid ,
              /*data: "name=John&location=Boston",*/
              success: function(data){
                $('#psyliner'+pid+'_phone').html(data);
              }
            });
            $(this).hide();
        });
        
        
        $(".cal_day").click(function(){
            
            var t = $(this).attr("t");
            var uid = $(this).attr("uid");
            
            $.ajax({
              type: 'POST',
              url: '/ajax/cons_time/'+t+'/'+uid ,
              /*data: "name=John&location=Boston",*/
              success: function(data){
                $('#cal_time').html(data);
              }
            });
        });
        
        
        $(".cal_day_act").click(function(){
            
            var t = $(this).attr("t");
            var pid = $(this).attr("pid");
            var code = $(this).attr("code");
            
            
            $.ajax({
              type: 'POST',
              url: '/ajax/cons_time_for_user/'+t+'/'+pid+'/'+code ,
              success: function(data){
                $('#hours').html(data);
              }
            });
        });
        
        
        $("body").on('click', '.time_of_cons', function(){
            
            var t = $(this).attr("t");
            var uid = $(this).attr("uid");
            var st = $(this).attr("st");
            $("#cal_cons_block").css('display','block');
            $.ajax({
              type: 'POST',
              url: '/ajax/add_cons_time/'+t+'/'+uid,
              success: function(data){
                  
                  if(data == 'add')
                  {
                    
                    $.ajax({
                    type: 'POST',
                    url: '/ajax/show_cons_time/'+t+'/'+uid+'/add' ,
                    success: function(data1){$('#cal_cons').append(data1);}
                    });
                  }
                  if(data == 'dell')
                  {
                    
                    $.ajax({
                    type: 'POST',
                    url: '/ajax/show_cons_time/'+t+'/'+uid+'/dell' ,
                    success: function(data1){$('#cal_cons').append(data1);}
                    });
                  }
              
              }
            });
            if(st==1)
            {
                $(this).css('background-color', '#FFF');
                $(this).attr('st','0');          
            }
            else
            {
                $(this).css('background-color', '#D6FFD2');
                $(this).attr('st','1');
            }
            
        });

        
        
        $("#tabs").tabs();
        
        $(".box").colorbox({rel:'group2', transition:"fade"});
        
        $(".doc_link").colorbox({rel:'group2', transition:"fade"});
        
        $(".u_p_close").click(function(event){
            event.preventDefault();
            $("#top_msgs").slideUp(300);
        });
        
        $("#user_p .dn1").click(function(){
            $("#top_msgs").slideDown(300);
        });
        
        
        $("#who_like").click(function(){
            $(".arrow_box").show(300);
        });
        $("#close_arrow").click(function(){
            $(".arrow_box").hide(300);
        });
        
        
        
        
        $(".hide_block_head div").click(
        function()
        {
            var ind = $(this).parent().attr('par');
            var act = $(this).parent().attr('act');
            
            if(act == "sd")
            {
                $(".hide_block"+ind).slideDown(300);
                $(this).parent().attr('act','su');
                $(this).html("Скрыть &#9650;");
            }
            if(act == "su")
            {
                $(".hide_block"+ind).slideUp(300);
                $(this).parent().attr('act','sd');
                $(this).html("Показать &#9660;");
            }
        });
        
        $("#topmenu ul li .strelka").click(function(){
            $(this).next("ul").show();
        });
        
        $("#topmenu ul li .strelkaup").click(function(){
            $("#topmenu ul li ul").hide();
        });
        
        
        $("td.set_links a").click(function(event){
            event.preventDefault();
            var a = $(this).attr("rel");
            var div = "#"+a;
            $(".hide_form").hide();
            $(div).slideDown(300);
        });
        
        $("#msg_block .show_msg").click(function(){
            var ind = $(this).attr('msgid');
            
            $(this).prev().slideUp(300);
            $(this).next().slideDown(300);
            $("tr #msg"+ind).removeClass('new_msg');
            
            $.ajax({
              type: 'POST',
              url: '/ajax/msg_read/'+ind
              });
        });
        
        $("#msg_block .msg_hide").click(function(){
            var ind = $(this).attr('msgid');
            var clashide = ".msg"+ind+"text";
            var clasopen = ".msg"+ind+"pretext";
            $(clashide).slideUp(300);
            $(clasopen).slideDown(300);
            
            
        });
        

        $("#new_f_t").click(function(){
            $("#form_new_f_t").slideDown(300);
        });
        
        $("#form_new_f_t span").click(function(){
            $("#form_new_f_t").slideUp(300);
        });
        

        
        $(".profile_set_buts div").click(function(){
            var a = $(this).attr('rel');
            $(".hide_form_1").slideUp(300);
            $("#"+a).slideDown(300);
        });


        
        $(".comment_msg").mouseup(
            function selectedText(pos)
            {
              if(window.getSelection)
              txt = window.getSelection().toString();
              else if(document.getSelection)
              txt = document.getSelection();                
              else if(document.selection)
              txt = document.selection.createRange().text;
             
              var nl = txt.length;//Сколько букв
              if(nl>5)
              {
                var uid = $(this).attr('uname'); //Юзер
                
                var px = pos.pageX-200+'px';
                var py = pos.pageY-30+'px';
                $(".quot").css('display','none');
                
                $(this).children(".quot").css({'display' : 'block', 'left' : px, 'top' : py});
                $(this).children(".quot").attr({'uname' : uid, 'txt' : txt});
              }
              if(nl<=5)
              {
                $(".quot").css('display','none');
              }
            }
        );
        
        $(".quot").click(function(){
            var uname = $(this).attr('uname');
            var txt = $(this).attr('txt');
            
            var ins = '<div class="citata"><span>'+uname+' писал(а):</span><br /><blockquote>'+txt+'</blockquote></div><br /><br />';
            $('.nicEdit-main').append(ins);
        });



        $("#rel_l").click(function(){
            path = "http://psy.loc/user/profile#tabs-6";
            window.location.href = path;
            window.location.reload(true); 
        });


        $(".onoffswitch_now").click(function(){
            var id = $(this).attr("uid");
            var znch = $(this).attr("znch");
            
            $.ajax({
              type: 'POST',
              url: '/ajax/ch_cons_now/'+id+'/'+znch,
            });
        });
        
        
        $(".onoffswitch_free").click(function(){
            var id = $(this).attr("uid");
            var znch = $(this).attr("znch");
            
            $.ajax({
              type: 'POST',
              url: '/ajax/ch_cons_free/'+id+'/'+znch,
            });
        });













});