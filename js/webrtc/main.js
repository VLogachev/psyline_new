//инициализируем модальное окно webrtc
$( "#webRTCDialog" ).dialog({
    autoOpen: false,
    modal: true,
    title: 'сессия',
    width: 1000,
    height: 600,
    dialogClass: 'z-index-121',
    close: function(event, ui){
        console.log("dialog closed");

        //отправляем лог
        sendLog(currConferenceName.split("-")[1] ,userId, "Close connection", Math.round(Date.now()/1000));

        //если юзер принимал участие в конференции
        if(currCategoryByStream === 5){
            connection.leave();
        } else {
            connection.close();
        }
        //connection.refresh();
        //connection.leave();
        updateSessionActTime("close");
        //location.reload();
        refreshConnections();
    }
});

/*
* Настраиваем работу webrtc
*/
//текущая категория стрима, которая открыта в модальном окне
var currCategoryByStream;
//текущее имя конференции, открытой в модальном окне
var currConferenceName = "conference-not-found";

//id текущего юзера
var userId = $("#webRTCDialog").data('userId');

var badConnection = false;

var connection = new RTCMultiConnection();
connection.firebase = "https://brilliant-inferno-6553.firebaseio.com";
connection.privileges.canMuteRemoteStream = true;

//при ошибке пытаемся подключиться еще раз
connection.autoReDialOnFailure = true;

connection.extra = {
    userID:userId
};

//connection.resources.firebaseio = 'https://popping-torch-6566.firebaseio.com/';
connection.resources.firebaseio = 'https://brilliant-inferno-6553.firebaseio.com/';

connection.onstreamid = function(e){
    //console.log("On STREAM ID");
};

connection.onleave = function(e){
    console.log("ON LEAVE");

    //отправляем лог
    sendLog(currConferenceName.split("-")[1] ,userId, "Another peer left", Math.round(Date.now()/1000));

    //если это псилайнер на индивидуальной консультации, то открываем сессию заново
    if(connection.isInitiator && (currCategoryByStream === 1 || currCategoryByStream === 2 || currCategoryByStream === 3)){
        //badConnection = true;
        //$("#webRTCDialog").dialog("close");
        //$("#webRTCDialog").dialog("open");
        //connection.open(currConferenceName);

        setInterval(function(){
            console.log("initiator reconnect timer");
            if(Object.keys(connection.peers).length == 1){
                console.log("initiator closes and opens again");
                $("#webRTCDialog").dialog("close");
                $("#webRTCDialog").dialog("open");
                connection.open(currConferenceName);
            }
        }, 15000);

    }

    //если это клиент на индивидуальной консультации
    if(!connection.isInitiator && (currCategoryByStream === 1 || currCategoryByStream === 2 || currCategoryByStream === 3)){
        $("#webRTCDialog").dialog("close");
    }
};

connection.onstream = function(e) {
    //если подключили локальный стрим, то начинаем его записывать
    /*if(e.type === "local"){
        var multiStreamRecorder = new MultiStreamRecorder(e.stream);
        multiStreamRecorder.ondataavailable = function(blobs) {
            sendBlob(blobs, currConferenceName, userId);
        };
        multiStreamRecorder.start(3000);
    }*/

    e.mediaElement.width = 400;
    videosContainer.insertBefore(e.mediaElement, videosContainer.firstChild);
    scaleMedia();
};

//когда получам сообщение от другого пира
connection.onCustomMessage = function(message){

    var remoteVideo = document.querySelector("video");

    switch(message){
        //отключаем remote video
        case "DISABLE_REMOTE_VIDEO":
            connection.streams.mute({video: true, type: 'remote'});
            remoteVideo.style.visibility = "hidden";
            break;
        //включаем remote video
        case "ENABLE_REMOTE_VIDEO":
            setTimeout(function(){
                connection.streams.unmute({video: true, type: 'remote'});
                remoteVideo.style.visibility = "visible";
            }, 1000);
            break;
        //отключаем remote audio
        case "DISABLE_REMOTE_AUDIO":
            connection.streams.mute({audio: true, type: 'remote'});
            remoteVideo.muted = true;
            break;
        //включаем remote audio
        case "ENABLE_REMOTE_AUDIO":
            connection.streams.unmute({audio: true, type: 'remote'});
            remoteVideo.muted = false;
            break;
        default: console.error("command not found");
    }
};

//при разрыве соединения
connection.ondisconnected = function(event){
    console.log("ON DISCONNECTED");

    if (event.isSocketsDisconnected) {
        //connection.connect(); // reconnect to same channel

        // or not only connect to same channel,
        // but also open a "session" in that channel
        //connection.open(currConferenceName);

        // or not only connect to same channel,
        // but also join a "session" in that channel
        //connection.join(currConferenceName);

        $( "#webRTCDialog" ).dialog( "open" );
        //connection.open(currConferenceName);
        //alert("Disconnected. Trying to reconnect...");
        videosContainer.textContent = "Trying to reconnect...";

        setInterval(function(){
            console.log("participant reconnect timer");
            if(Object.keys(connection.peers).length == 1){
                console.log("participant is trying to join");
                connection.join(currConferenceName);
            }
        }, 5000);

    }
};

connection.onclose = function(){
    console.log("ON CLOSE");
};

connection.onopen = function(e){
    console.log("ON OPEN");
};

connection.onerror = function(e){
    console.log("ON ERROR");
};

connection.ondrop = function(e){
    console.log("ON DROP");
};

connection.onSessionClosed = function(e){
    console.log("ON SESSION CLOSED");
};

connection.onfailed = function(e){
    console.log("ON FAILED");
};

connection.onstatechange = function(e){
    console.log("ON STATE CHANGE");
};

connection.onstreamended = function(e) {
    console.log("ON STREAM ENDED");
    e.mediaElement.style.opacity = 0;
    if (e.mediaElement.parentNode) {
        e.mediaElement.parentNode.removeChild(e.mediaElement);
    }
    checkForNoVideos();
    scaleMedia();
};

var sessions = {};


//при доступной новой сессии
connection.onNewSession = function(session) {
    console.log('onNewSession');

    //если находим кнопку с подключением к сессии(в случае если создатель сессии хочет к ней подключиться, после отключения)
    if($("button[data-conference-name="+session.sessionid+"]").length){
        var button = $("button[data-conference-name="+session.sessionid+"]");
        var catByStr = button.data("categoryByStream");
        //если это конференция
        if(catByStr === 5){
            button.removeClass('open-session');
            button.addClass('connect-to-session');
            button.text('присоединиться');
        }
    }

    //меняем надпись оффлайн на онлайн
    $("span[data-conference-name="+session.sessionid+"]").text("online");
    //активируем кнопку подключения к сессии
    $("button[data-conference-name="+session.sessionid+"]").removeAttr("disabled");
    //при нажатии на кнопку подключения к сессии
    $( ".connect-to-session" ).on('click', function() {

        //устанавливаем тип соединения аудио или видео+аудио
        setSessionType($(this).data("categoryByStream"));
        currCategoryByStream = $(this).data("categoryByStream");
        currConferenceName = $(this).data("conferenceName");

        //отправляем лог
        sendLog(currConferenceName.split("-")[1] ,userId, "Session connect", Math.round(Date.now()/1000));

        //открываем модальное окно
        startConsultationTimer($(this).data("toTime"));
        $( "#webRTCDialog" ).dialog( "open" );

        //открываем webrtc соединение
        connection.join($(this).data("conferenceName"));
    });
};

var videosContainer = document.getElementById('webRTCVideoContainer');

//при нажатии на кнопку начать сессию
$( ".open-session" ).on('click', function() {

    //устанавливаем тип соединения аудио или видео+аудио
    setSessionType($(this).data("categoryByStream"));
    currCategoryByStream = $(this).data("categoryByStream");
    currConferenceName = $(this).data("conferenceName");

    //отправляем лог
    sendLog(currConferenceName.split("-")[1] ,userId, "Session open", Math.round(Date.now()/1000));

    //открываем модальное окно
    startConsultationTimer($(this).data("toTime"));
    $( "#webRTCDialog" ).dialog( "open" );

    //открываем webrtc соединение
    connection.open(currConferenceName);

    updateSessionActTime("open");
});

/**
 * устанвливаем параметы сессии, аудио или видео
 *
 * @param $type
 */
function setSessionType($type){

    var video, audio, oneway;

    if($type === 4){ //вебинар
        video = true;
        audio = true;
        oneway = true;
    } else {
        //иначе демо(1), супервизия(2), инд консультация(3), обсуждение(5)
        video = true;
        audio = true;
        oneway = false;
    }

    connection.session = {
        video: video,
        audio: audio,
        oneway: oneway
    };
}

//если на странице больше нет видео, то закрываем модальное окно
function checkForNoVideos(){
    if(document.querySelectorAll('video').length === 0){
        $("#webRTCDialog").dialog("close");
    }
}

/**
 * устанавливает заголоок модального окна
 * @param toTime время окончания открытого сеанса
 */
function startConsultationTimer(toTime){

    //каждую секунду обновляем время окончания сеанса
    setInterval(function(){
        var title = "";
        switch(currCategoryByStream){
            case 1:title = "Демо-консультация.";break;
            case 2:title = "Супервизия.";break;
            case 3:title = "Индивидуальная консультация.";break;
            case 4:title = "Вэбинар.";break;
            case 5:title = "Обсуждение.";break;
        }

        //устанавливаем заголовок модального окна
        $("#webRTCDialog").dialog("option", "title", title);

        //если время сеанса закончилось, то закрываем диалоговое окно
        //if(toTime * 1000 - Date.now() < 0) $("#webRTCDialog").dialog("close");

        var secondsLeft = Math.round((toTime * 1000 - Date.now()) / 1000);
        var minutesLeft = Math.floor(secondsLeft / 60);
        secondsLeft -= minutesLeft * 60;
        minutesLeft = "0" + minutesLeft;
        secondsLeft = "0" + secondsLeft;

        var strTimeLeft = minutesLeft.substr(-2) + ":" + secondsLeft.substr(-2);
        //показываем время окончания консультации над чатом
        $("#consultationTimeLeft").html("Осталось " + strTimeLeft);
    }, 1000);

}

// начинаем поиск ice candidates
connection.connect();

//каждые 5 сек обновляем соединение для поиска новых коннектов
/*setInterval(function(){
    //если мы на стороне клиента и в соединении только 1 peer, то обновляем
    if(!connection.isInitiator && Object.keys(connection.peers).length < 2){
        connection.refresh();
    }
}, 5000);*/

function scaleMedia() {

    var videos = document.querySelectorAll('video');
    var length = videos.length;
    var video;

    //set custom controls for videos
    setCustomVideoControls();

    //если 1 видео, то растягиваем его до 700px
    if(length === 1){
        videos[0].width = 700;
        $("video:first").removeAttr("style");
        return;
    }

    //если 2 видео
    if(length === 2){
        $("video").each(function(index){
            $(this).removeAttr("style");
            if(index === 0){
                $(this).css("position", "absolute");
                $(this).css("width", "700px");
            }
            if(index === 1){
                $(this).css("position", "absolute");
                $(this).css("width", "250px");
                $(this).css("left", "430px");
                $(this).css("top", "20px");
            }
        });
        return;
    } else {
        //3 и более
        $("video").each(function(index){
            $(this).removeAttr("style");
            $(this).css("width", "300px");
            $(this).css("padding", "20px 20px");
        });

    }

}

//creates custom video controls for all video tags on the page
function setCustomVideoControls(){

    var videos = $("video");
    videos.each(function(index){
        $(this).removeAttr("controls");
    });

    //если видео консультация и есть подключенный стрим, то добавляем 2 кнопки (fullsize и disable remote stream)
    if((currCategoryByStream === 1 || currCategoryByStream === 2 || currCategoryByStream === 3) && videos.length == 2){
        //добавляем иконки видео, аудио и fullscreen
        $("#webRTCVideoContainer").append("<a id='btnDisableRemoteVideo' href='#' style='position: absolute; top: 0; left: 0;'><img src='/images/video-icon.png' style='width: 48px;'></a>");
        $("#webRTCVideoContainer").append("<a id='btnDisableRemoteAudio' href='#' style='position: absolute; top: 48px; left: 0;'><img src='/images/sound-icon.png' style='width: 48px;'></a>");
        $("#webRTCVideoContainer").append("<a id='btnFullscreen' href='#' style='position: absolute; top: 96px; left: 0;'><img src='/images/fullscreen-icon.png' style='width: 48px;'></a>");
        $("#webRTCVideoContainer").append("<a id='btnEndCall' href='#' style='position: absolute; top: 144px; left: 0;'><img src='/images/end-call-icon.png' style='width: 48px;'></a>");

        var remoteVideo = document.querySelector("video");

        //добавляем обработчики кнопок
        //fullscreen
        $("#btnFullscreen").on("click", function(){
            requestFullScreen(remoteVideo);
        });
        //при нажатии на кнопку отключения видео на противоположной стороне
        $("#btnDisableRemoteVideo").on("click", function(){
            //меяем иконку и отправляем сообщение, чтобы remote peer выключил наше видео
            var img = $(this).find("img");
            if(img.attr("src") === "/images/video-icon.png"){
                img.attr("src","/images/video-icon-crossed.png");
                connection.sendCustomMessage("DISABLE_REMOTE_VIDEO");
            } else {
                img.attr("src","/images/video-icon.png");
                connection.sendCustomMessage("ENABLE_REMOTE_VIDEO");
            }
            return false;
        });
        //при нажатии на кнопку отключения аудио на противоположной стороне
        $("#btnDisableRemoteAudio").on("click", function(){
            //меяем иконку и отправляем сообщение, чтобы remote peer выключил наше аудио
            var img = $(this).find("img");
            if(img.attr("src") === "/images/sound-icon.png"){
                img.attr("src","/images/sound-icon-crossed.png");
                connection.sendCustomMessage("DISABLE_REMOTE_AUDIO");
            } else {
                img.attr("src","/images/sound-icon.png");
                connection.sendCustomMessage("ENABLE_REMOTE_AUDIO");
            }
            return false;
        });
        //при нажатии на кнопку завершить звонок
        $("#btnEndCall").on("click", function(){
            $("#webRTCDialog").dialog("close");
            return false;
        });
    }

}

/**
 * отправляет blob файлы на сервер
 *
 * @param blobs
 * @param conferenceName
 * @param userID
 */
function sendBlob(blobs ,conferenceName, userID){

    var audioBlob = blobs.audio;
    var videoBlob = blobs.video;

    //отправляем видео
    var fileType = 'video'; // or "audio"
    var fileName = 'video.webm';  // or "wav"

    var formData = new FormData();
    formData.append(fileType + '-filename', fileName);
    formData.append(fileType + '-blob', videoBlob);
    formData.append('conference-name', conferenceName);
    formData.append('user-id', userID);

    xhr('/ajax/upload_blob', formData, function (resp) {
        console.log(resp);
        //window.open(location.href + fName);
    });

    //отправляем аудио
    fileType = 'audio';
    fileName = 'audio.wav';

    formData = new FormData();
    formData.append(fileType + '-filename', fileName);
    formData.append(fileType + '-blob', audioBlob);
    formData.append('conference-name', conferenceName);
    formData.append('user-id', userID);

    xhr('/ajax/upload_blob', formData, function (resp) {
        //console.log(resp);
        //window.open(location.href + fName);
    });

    function xhr(url, data, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                //callback(request.responseText);
            }
        };
        request.open('POST', url);
        request.send(data);
    }

}

/**
 * ajax
 * обновляет время создания и закрытия сессии на сервере
 * @type string open/close
 */
function updateSessionActTime(type){
    $.ajax({
        url: '/ajax/update_session_time',
        type: 'POST',
        data: 'conferenceName='+currConferenceName+"&userID="+userId+"&type="+type,
        success: function(data) {
            //called when successful
            //console.log(data);
        },
        error: function(e) {
            //called when there is an error
            console.log(e.message);
        }
    });
}

/**
 * Разворачивает элемент в fullscreen мод
 *
 * @param el
 */
function requestFullScreen(el) {
    var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen;
    requestMethod.call(el);
}

/**
 * Приводит все кнопки и статусы в изначальное состояние(оффлайн) и начинает заново поиск соединения
 */
function refreshConnections(){
    $(".connect-to-session").each(function(index, el){
        //console.log(index, el);
        //выключаем кнопку
        $(this).attr("disabled", true);
        //меняем статус на оффлайн
        var localConfName = $(this).data("conferenceName");
        $("span[data-conference-name="+localConfName+"]").text("offline");
    });
    //удаляем все видео теги
    videosContainer.innerHTML = "";
}

/**
 * Отправляет ajax запрос на сервер с текстом лога консультации
 *
 * @param consultation_id
 * @param user_id
 * @param event
 * @param time
 */
function sendLog(consultation_id ,user_id, event, timestamp){

    var message = "USER_ID:" + user_id + ",EVENT:" + event + ",TIMESTAMP:" + timestamp + ";";

    $.ajax({
        url: '/ajax/log_consultation',
        type: 'POST',
        data: 'consultation_id=' + consultation_id + '&message='+btoa(message),
        success: function(data) {
            console.log(data);
        },
        error: function(e) {
            console.log(e.message);
        }
    });
}
