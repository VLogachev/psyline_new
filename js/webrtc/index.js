/*
* INITIAL VARIABLES
*/

var connection = new RTCMultiConnection();

connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';
connection.socketMessageEvent = 'video-conference-demo';

connection.session = {
    audio: true,
    video: true
};

connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true
};

var videosContainer = document.getElementById('webRTCVideoContainer');

//init modal window for webrtc
$( "#webRTCDialog" ).dialog({
    autoOpen: false,
    modal: true,
    title: 'сессия',
    width: 1000,
    height: 600,
    dialogClass: 'z-index-121',
    close: function(event, ui){
        clearVideos();
        connection.close();
        window.location.reload();
    }
});

/*
* UI HANDLERS
*/

//when user clicks the open session button
$( ".open-session" ).on('click', function() {
    //opens a new room
    connection.open($(this).data("conferenceName"));
    $( "#webRTCDialog" ).dialog( "open" );
});

//when user clicks to join session
$( ".connect-to-session" ).on('click', function() {
    connection.join($(this).data("conferenceName"));
    $( "#webRTCDialog" ).dialog( "open" );
});

//when user clicks the disable local video button
$('.controls__disable-video').on('click', function(){
    $(this).hide();
    $('.controls__enable-video').show();
    connection.streamEvents.selectFirst().stream.mute('video');
});

//when user clicks the enable local video button
$('.controls__enable-video').on('click', function(){
    $(this).hide();
    $('.controls__disable-video').show();
    connection.streamEvents.selectFirst().stream.unmute('video');
});

//when user clicks the disable local audio button
$('.controls__disable-audio').on('click', function(){
    $(this).hide();
    $('.controls__enable-audio').show();
    connection.streamEvents.selectFirst().stream.mute('audio');
});

//when user clicks the enable local audio button
$('.controls__enable-audio').on('click', function(){
    $(this).hide();
    $('.controls__disable-audio').show();
    connection.streamEvents.selectFirst().stream.unmute('audio');
});

//when user clicks the fullscreen button
$('.controls__fullscreen').on('click', function(){
    requestFullscreenForLastVideo();
});

//when user clicks the end call button
$('.controls__end-call').on('click', function(){
    connection.streamEvents.selectFirst().stream.stop();
    connection.getAllParticipants().forEach(function(participantId) {
        connection.disconnectWith(participantId);
    });
    $( "#webRTCDialog" ).dialog( "close" );
});

/*
*  WebRTC HANDLERS
 */

connection.onstream = function(event) {
    //fixing echo
    if(event.mediaElement) {
        event.mediaElement.muted = true;
        delete event.mediaElement;
    }

    var video = document.createElement('video');
    if(event.type === 'local') {
        video.muted = true;
    }
    video.src = URL.createObjectURL(event.stream);
    videosContainer.appendChild(video);
    setTimeout(function() {
        video.play();
    }, 5000);

    scaleVideos();
};

connection.onmute = function(){};

connection.onunmute = function(){};

/*
*  MISC FUNCTIONS
 */

function clearVideos(){
    videosContainer.innerHTML = "";
}

function scaleVideos(){
    if($('video').length === 1){
        $('video').css('width', '100%');
    }
    if($('video').length === 2){
        $('video').css('width', '49%').css('padding-left', '1%');
    }
}

function requestFullscreenForLastVideo(){
    var elem = document.querySelector("video:last-child");
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
    } else if (elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
    }
}

//every 5 seconds check for available room
setInterval(function(){
    $(".connect-to-session").each(function(index, el){
        var connectBtn = $(this);
        connection.checkPresence($(this).data("conferenceName"), function(isRoomEists, roomid) {
            if(isRoomEists) {
                //выключаем кнопку
                connectBtn.attr("disabled", false);
                //меняем статус на оффлайн
                var localConfName = connectBtn.data("conferenceName");
                $("span[data-conference-name="+localConfName+"]").text("online");
            }
            else {
                //выключаем кнопку
                connectBtn.attr("disabled", true);
                //меняем статус на оффлайн
                var localConfName = connectBtn.data("conferenceName");
                $("span[data-conference-name="+localConfName+"]").text("offline");
            }
        });

    });
}, 5000);